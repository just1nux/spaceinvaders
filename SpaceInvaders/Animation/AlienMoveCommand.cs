﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class AlienMoveCommand : Command
    {
        private GameObject gameObject;
        private int soundNum;
        private bool repeating;

        public AlienMoveCommand(GameObject gameObject)
        {
            setGameObject(gameObject);
            soundNum = 3;
            repeating = true;
        }

        public void setRepeating(bool repeating)
        {
            this.repeating = repeating;
        }

        public void setGameObject(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public GameObject getGameObject()
        {
            return this.gameObject;
        }

        public override void execute(float deltaTime)
        {
            Grid grid = (Grid)GameObjectFactory.NewGameObject(GameObject.Name.Grid);
            
            float speed = grid.countAliens() / 55.0f;
            deltaTime = 0.6f * speed - (GameManager.GetLevel() * 0.05f);
            if(deltaTime < 0.05f){
                deltaTime = 0.05f;
            }
            gameObject.Move(0,0);

            //Sound
            switch(soundNum){
                case 0:
                    SoundManager.Play(Soundk.Name.fastinvader1);
                    break;
                case 1:
                    SoundManager.Play(Soundk.Name.fastinvader2);
                    break;
                case 2:
                    SoundManager.Play(Soundk.Name.fastinvader3);
                    break;
                case 3:
                    SoundManager.Play(Soundk.Name.fastinvader4);
                    break;
                default:
                    break;
            }


            soundNum++;
            
            if(soundNum == 4)
                soundNum = 0;
            if (repeating)
               TimerManager.Add(this, deltaTime);
        }

        public override void print()
        {

        }

        public override void initialize()
        {

        }
    }
}
