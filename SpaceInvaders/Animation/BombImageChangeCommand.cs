﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class BombImageChangeCommand: Command
    {
        private Sprite sprite;
        private SLink currImage;
        private SLink firstImage;

        public BombImageChangeCommand(Sprite.Name name)
        {
            setSprite(name);
            this.sprite = SpriteManager.Instance().findActiveByName(name);
            Debug.Assert(this.sprite != null);
        }

        public void attachImage(Image.Name imageName)
        {
            Image image = ImageManager.Instance().findActiveByName(imageName);
            ImageHolder imageHolder = new ImageHolder(image);
            addNode(imageHolder, ref firstImage);
            setCurrImage(imageHolder);
        }

        private void addNode(SLink node, ref SLink head)
        {
            if (head == null)
            {
                head = node;
                node.setNext(null);
            }
            else
            {
                node.setNext(head);
                head = node;
            }
        }

        public void setSprite(Sprite sprite){
            this.sprite = sprite;
            Debug.Assert(this.sprite != null);
        }

        public void setSprite(Sprite.Name name)
        {
            setSprite(SpriteManager.Instance().findActiveByName(name));
            Debug.Assert(this.sprite != null);
        }

        public Sprite getSprite()
        {
            return this.sprite;
        }

        public void setCurrImage(SLink currImage)
        {
            this.currImage = currImage;
        }

        public SLink getCurrImage()
        {
            return this.currImage;
        }

        public void setFirstImage(SLink firstImage)
        {
            this.firstImage = firstImage;
        }

        public SLink getFirstImage()
        {
            return this.firstImage;
        }
        
        public override void execute(float deltaTime)
        {
            Debug.Assert(currImage != null);
            ImageHolder imageHolder = (ImageHolder)this.currImage.getNext();

            if (imageHolder == null)
            {
                imageHolder = (ImageHolder)firstImage;
            }

            this.currImage = imageHolder;
            getSprite().setImage(imageHolder.getImage());

            //Grid grid = (Grid)GameObjectManager.FindGameObject(GameObject.Name.Grid);
            //float speed = grid.countAliens() / 55.0f;
            //deltaTime = 0.5f * speed;

            TimerManager.Add(this, deltaTime);
        }

        public override void print()
        {

        }

        public override void initialize()
        {

        }
    }
}
