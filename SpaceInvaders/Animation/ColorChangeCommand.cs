﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ColorChangeCommand: Command
    {
        private SpriteProxy sprite;
        private Color color;

        public ColorChangeCommand(SpriteProxy sprite, Color.Name colorName)
        {
            setSprite(sprite);
            Debug.Assert(this.sprite != null);
            Color color = ColorManager.Add(colorName);
            setColor(color);
            Debug.Assert(this.color != null);
        }

        public void setColor(Color color)
        {
            this.color = color;
        }

        public void setSprite(SpriteProxy sprite){
            this.sprite = sprite;
            Debug.Assert(this.sprite != null);
        }

        public SpriteProxy getSprite()
        {
            return this.sprite;
        }

        public override void execute(float deltaTime)
        {
            Debug.Assert(this.sprite != null);
            Debug.Assert(this.color != null);
            this.sprite.setColor(this.color);
        }

        public override void print()
        {

        }

        public override void initialize()
        {

        }
    }
}
