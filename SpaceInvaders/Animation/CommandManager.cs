﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class CommandManager : ContainerManager
    {
        private static volatile CommandManager instance;

        private CommandManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static CommandManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new CommandManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static CommandNode Add(CommandNode.Name name, Command command, int index=0)
        {
                CommandManager manager = CommandManager.Instance();
                CommandNode node = (CommandNode)manager.addLink();
                node.setCommand(command);
                node.setName(name);
                node.setIndex(index);
            
            return node;
        }
        

        public static CommandNode Add(CommandNode.Name commandNodeName, GameObject.Name gameObjectName, int nodeIndex=0, int gameObjectIndex=0, GameObjectMoveCommand.Direction direction = GameObjectMoveCommand.Direction.EAST, float x=0.0f, float y=0.0f, bool repeating=false)
        {
                CommandManager manager = CommandManager.Instance();
                CommandNode node = (CommandNode)manager.addLink();
                GameObjectMoveCommand command = new GameObjectMoveCommand(gameObjectName, direction, x, y, repeating, gameObjectIndex);
            
                node.setCommand(command);
                node.setName(commandNodeName);
                node.setIndex(nodeIndex);
            
            return node;
        }



        public static CommandNode FindCommandNode(CommandNode.Name name, int index=0)
        {
            CommandManager manager = CommandManager.Instance();
            CommandNode node = manager.findActiveByName(name);
            return node;
        }

        protected override Container create()
        {
            Container link = new CommandNode();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            CommandNode nodeA = (CommandNode)linkA;
            CommandNode nodeB = (CommandNode)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }

        public CommandNode findActiveByName(CommandNode.Name name, int index=0)
        {
            CommandNode foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                CommandNode node = (CommandNode)pullFromReserve();
                node.setName(name);
                node.setIndex(index);
                foundNode = (CommandNode)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            CommandManager manager = CommandManager.Instance();
            Debug.WriteLine("----Command Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            CommandManager manager = CommandManager.Instance();
            manager.baseDestroy();
        }
    }
}
