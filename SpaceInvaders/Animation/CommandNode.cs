﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class CommandNode : Container
    {
        private Name name;
        private int index;
        private Command command;

        public CommandNode()
        {
            initialize();
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public Name getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public override void print()
        {
            Debug.WriteLine("Tree name:{0}", getName());
        }

        public void setCommand(Command command)
        {
            this.command = command;
        }

        public Command getCommand()
        {
            return this.command;
        }

        public enum Name
        {
            Uninitialized,
            MissileMove,
            GridMove,
            AlienVisibility,
            MissileVisibility,
            ShipMoveLeft,
            ShipMoveRight,
            DelayedCommand,
            BombMove,
        }

    }
}
