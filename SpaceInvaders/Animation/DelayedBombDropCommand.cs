﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class DelayedBombDropCommand : Command
    {
        GameObject alien;
        bool val = false;

        public DelayedBombDropCommand(GameObject alien)
        {
            this.alien = alien;
        }

        public DelayedBombDropCommand(bool val)
        {
            this.alien = GameObjectFactory.NewGameObject(GameObject.Name.UFO);
            this.val = val;
        }

        public override void execute(float deltaTime)
        {
            GameObjectFactory.SetTree(TreeNode.Name.BombTree);
            GameObjectFactory.SetParent(GameObjectFactory.NewGameObject(GameObject.Name.BombRoot));
            Bomb bomb = null;
           
            if (this.alien != null)
            {
                if (!val)
                {
                    bomb = (Bomb)BombManager.getBomb();
                    Debug.Assert(bomb != null);
                }
                else
                {
                    RectManager.Add(Rect.Name.Bomb, 0, -1000, -1000, 9.0f, 21.0f);
                    bomb = (Bomb)GameObjectFactory.NewGameObject(GameObject.Name.BombUFO, Rect.Name.Bomb, 0, 0);
                }

                bomb.setX(this.alien.getX());
                bomb.setY(this.alien.getY() - 15.0f);

                //Start Bomb moving
                bomb.moveCommand = new GameObjectMoveCommand(bomb, GameObjectMoveCommand.Direction.SOUTH, 500.0f, 10.0f, true);
                TimerManager.Add(bomb.moveCommand, 0.04f);
            }

            if (!val)
            {
                this.alien = BombManager.getAlien();
                Random random = new Random();
                float randomTime = random.Next(125, 250) / 100.0f;
                TimerManager.Add(new DelayedBombDropCommand(alien), randomTime);
            }
        }

        public override void print()
        {
            Debug.WriteLine("");
        }

        public override void initialize()
        {

        }

        private void relocateAndMoveBomb()
        {

        }


        private GameObject.Name getBombType(int num)
        {
            GameObject.Name name = 0;

            switch (num)
            {
                case 0:
                    name = GameObject.Name.BombLightning;
                    break;
                case 1:
                    name = GameObject.Name.BombTwister;
                    break;
                case 2:
                    name = GameObject.Name.BombDagger;
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }

            return name;
        }


    }
}
