﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class DelayedCommand : Command
    {
        Command command;
        float delay;

        public DelayedCommand (Command command, int index, float delay)
        {
            this.command = command;
            this.delay = delay;        
        }

        public override void execute(float deltaTime)
        {
            TimerManager.Add(command, delay);
        }

        public override void print()
        {
            Debug.WriteLine("");
        }

        public override void initialize()
        {

        }
    }
}
