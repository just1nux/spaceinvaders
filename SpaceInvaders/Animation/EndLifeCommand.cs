﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class EndLifeCommand: Command
    {

        public EndLifeCommand()
        {
        }

        public override void execute(float deltaTime)
        {

            Player currentPlayer = GameManager.GetCurrentPlayer();
            Player nextPlayer;


            if (currentPlayer == GameManager.GetPlayerOne())
            {
                nextPlayer = GameManager.GetPlayerTwo();
            }
            else
            {
                nextPlayer = GameManager.GetPlayerOne();
            }


            if (GameManager.GetNumPlayers() == 2 && nextPlayer.getLives() > 0)
            {                // Swap Player
                GameManager.SetCurrentPlayer(nextPlayer);
            }


            if (GameManager.GetCurrentPlayer().getLives() <= 0)
            {
                GamePlayingState.Instance().Stop();
                GameManager.SetState(GameOverState.Instance());
                GameOverState.Instance().Start();
            }
            else
            {
                GamePlayingState.Instance().Stop();
                GameManager.SetState(PlayerStartState.Instance(GameManager.GetNumPlayers()));
                PlayerStartState.Instance(GameManager.GetNumPlayers()).Start();
            }

 
        }

        public override void print()
        {

        }

        public override void initialize()
        {

        }
    }
}
