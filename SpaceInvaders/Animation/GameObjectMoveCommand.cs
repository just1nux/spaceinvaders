﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class GameObjectMoveCommand : Command
    {
        public enum Direction
        {
            NORTH,
            SOUTH,
            EAST,
            WEST,
        }

        private float maxMoveDistance;
        private Direction direction;
        private float moveBy;
        private GameObject gameObject;
        private float moveDistance;
        private bool repeating;

        public GameObjectMoveCommand(GameObject.Name gameObjectName, Direction direction, float maxMoveDistance=100.0f, float moveBy=1.0f, bool repeating = true, int index=0)
        {
            GameObject gameObject = GameObjectManager.FindGameObject(gameObjectName, index);
            setGameObject(gameObject);

            setMaxMoveDistance(maxMoveDistance);
            setMoveBy(moveBy);
            setDirection(direction);
            setRepeating(repeating);
        }

        public GameObjectMoveCommand(GameObject gameObject, Direction direction, float maxMoveDistance = 100.0f, float moveBy = 1.0f, bool repeating = true)
        {
            setGameObject(gameObject);
            setMaxMoveDistance(maxMoveDistance);
            setMoveBy(moveBy);
            setDirection(direction);
            setRepeating(repeating);
        }



        public void setRepeating(bool repeating)
        {
            this.repeating = repeating;
        }

        public bool isRepeating()
        {
            return this.repeating;
        }

        public void setGameObject(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public GameObject getGameObject()
        {
            return this.gameObject;
        }

        public void setMaxMoveDistance(float maxMoveDistance)
        {
            this.maxMoveDistance = maxMoveDistance;
            this.moveDistance = maxMoveDistance;
        }

        public float getMaxMoveDistance()
        {
            return this.maxMoveDistance;
        }

        public void setDirection(Direction direction)
        {
            this.direction = direction;
        }

        public Direction getDirection()
        {
            return this.direction;
        }

        public void setMoveBy(float moveBy)
        {
            this.moveBy = moveBy;
        }

        public float getMoveBy()
        {
            return this.moveBy;
        }

        public override void execute(float deltaTime)
        {

         if (moveDistance > 0)
            {
                switch (direction)
                {
                    case Direction.NORTH:
                        moveNorth(deltaTime);
                        break;
                    case Direction.SOUTH:
                        moveSouth(deltaTime);
                        break;
                    case Direction.EAST:
                        moveEast(deltaTime);
                        break;
                    case Direction.WEST:
                        moveWest(deltaTime);
                        break;
                }

                if (repeating)
                {
                    TimerManager.Add(this, deltaTime);
                }
            }
        }

        private void moveEast(float deltaTime)
        {
            float moveY = 0;
            moveDistance = maxMoveDistance - moveBy;
            gameObject.Move(moveBy, moveY);
        }

        private void moveWest(float deltaTime)
        {
            float moveY = 0;
            moveDistance = moveDistance - moveBy;
            gameObject.Move(-1 * moveBy, moveY);
        }

        private void moveNorth(float deltaTime)
        {
            float moveX = 0;
            moveDistance = moveDistance - moveBy;
             gameObject.Move(moveX, moveBy);
        }

        private void moveSouth(float deltaTime)
        {
            float moveX = 0;
            moveDistance = moveDistance - moveBy;
            gameObject.Move(moveX, -1 * moveBy);
        }

        public override void print()
        {
            Debug.WriteLine("maxMoveDistance:{0} direction:{1} moveBy:{2} gameObject:{3} moveDistance:{4} repeating:{5}", maxMoveDistance, direction, moveBy, gameObject.getName(), moveDistance, repeating);
        }

        public override void initialize()
        {

        }
    }
}
