﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class GameObjectVisibilityCommand : Command
    {
        GameObject gameObject;
        bool visible;

        public GameObjectVisibilityCommand (GameObject.Name name, int index, bool visible)
        {
            this.gameObject = GameObjectManager.FindGameObject(name, index);
            this.visible = visible;    
        }

        public override void execute(float deltaTime)
        {
            if (gameObject != null)
            {
                Debug.Assert(gameObject.getSprite() != null);

                SpriteProxy sprite = gameObject.getSprite();
                sprite.setVisible(this.visible);
            }
        }

        public override void print()
        {
            Debug.WriteLine("");
        }

        public override void initialize()
        {

        }
    }
}
