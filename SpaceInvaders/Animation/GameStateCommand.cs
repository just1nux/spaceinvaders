﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class GameStateCommand : Command
    {
        public enum State
        {
            START,
            STOP,
            PAUSE,
        }

        private State state;
        private GameState gameState;

        public GameStateCommand (GameState gameState, State state)
        {
            this.state = state;
            this.gameState = gameState;
        }

        public override void execute(float deltaTime)
        {
            switch(this.state){
                case State.START:
                    this.gameState.Start();
                    break;
                case State.STOP:
                    this.gameState.Stop();
                    break;
            }
        }

        public override void print()
        {
        }

        public override void initialize()
        {

        }
    }
}
