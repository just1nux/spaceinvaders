﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ShipStateCommand : Command
    {
        ShipState state;
        Ship ship;

        public ShipStateCommand (GameObject.Name name, ShipState state, int index=0)
        {
            this.ship = (Ship) GameObjectManager.FindGameObject(name, index);
            this.state = state;
        }

        public override void execute(float deltaTime)
        {
            this.ship.setState(state);
        }

        public override void print()
        {
            Debug.WriteLine("");
        }

        public override void initialize()
        {

        }
    }
}
