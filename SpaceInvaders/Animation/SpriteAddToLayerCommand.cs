﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteAddToLayerCommand : Command
    {
        SpriteBase sprite;
        SpriteLayer spriteLayer;

        public SpriteAddToLayerCommand (SpriteBase sprite, SpriteLayer spriteLayer)
        {
            this.sprite = sprite;
            this.spriteLayer = spriteLayer;
        }

        public override void execute(float deltaTime)
        {
            spriteLayer.add(sprite);
            sprite = null;
            spriteLayer = null;
        }

        public override void print()
        {
        }

        public override void initialize()
        {
        }
    }
}
