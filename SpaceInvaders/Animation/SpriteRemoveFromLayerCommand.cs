﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteRemoveFromLayerCommand : Command
    {
        SpriteBase sprite;
        SpriteLayer spriteLayer;

        public SpriteRemoveFromLayerCommand (SpriteBase sprite, SpriteLayer spriteLayer)
        {
            this.sprite = sprite;
            this.spriteLayer = spriteLayer;
        }

        public override void execute(float deltaTime)
        {
            spriteLayer.removeSprite(sprite);
            sprite = null;
            spriteLayer = null;
        }

        public override void print()
        {
        }

        public override void initialize()
        {
        }
    }
}
