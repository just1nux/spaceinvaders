﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class StateChangeCommand : Command
    {
        GameState nextState;
        GameState currentState;

        public StateChangeCommand(GameState nextState, GameState currentState)
        {
            this.nextState = nextState;
            this.currentState = currentState;
        }

        public override void execute(float deltaTime)
        {
             if (GameManager.GetState() != nextState)
             {
                currentState.Stop();
                GameManager.SetState(nextState);
                nextState.Start();
             }
        }

        public override void print()
        {
            Debug.WriteLine("");
        }

        public override void initialize()
        {

        }

    }
}
