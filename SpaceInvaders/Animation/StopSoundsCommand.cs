﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class StopSoundsCommand : Command
    {

        public StopSoundsCommand ()
        {
            
        }

        public override void execute(float deltaTime)
        {
            SoundManager.Stop();
        }

        public override void print()
        {
        }

        public override void initialize()
        {

        }
    }
}
