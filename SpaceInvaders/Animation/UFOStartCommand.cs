﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class UFOStartCommand : Command
    {
        UFO ufo;

        public UFOStartCommand ()
        {
            
        }

        public override void execute(float deltaTime)
        {

            Random random = new Random();

            GameObjectFactory.SetTree(TreeNode.Name.UFOTree);
            UFORoot ufoRoot = (UFORoot)GameObjectFactory.NewGameObject(GameObject.Name.UFORoot);
            GameObjectFactory.SetParent(ufoRoot);
            RectManager.Add(Rect.Name.UFO, 0, 230, 595, 44.0f, 20.0f);
            this.ufo = (UFO)GameObjectFactory.NewGameObject(GameObject.Name.UFO, Rect.Name.UFO, 0, 0);
            ((Sprite)this.ufo.getSprite().getSprite()).setColor(Color.Name.Red);

            int direction = random.Next(0, 2);

            if (direction > 0){
               this.ufo.setStrategy(new UFORightStrategy());
            }

            this.ufo.getSprite().setVisible(true);
            this.ufo.Start();
            SoundManager.Play(Soundk.Name.ufohighpitch);

            deltaTime = random.Next(75, 200)/5.0f;

            TimerManager.Add(this, deltaTime);
        }

        public override void print()
        {
        }

        public override void initialize()
        {

        }
    }
}
