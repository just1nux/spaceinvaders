﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class AttractMode
    {

        public AttractMode()
        {

        }

        public static void CreatePlayerChoose()
        {
            FontManager.Add(Font.Name.PUSH, SpriteLayer.Name.PlayerChoose, "PUSH", 290, 450);
            FontManager.Add(Font.Name.ONEORTWO, SpriteLayer.Name.PlayerChoose, "1 OR 2PLAYERS BUTTON", 100, 375);

        }

        public static void ActivatePlayerChoose()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.PlayerChoose).setVisible(true);
            Command command = new StateChangeCommand(AttractState.Instance(), GameManager.GetState());
            TimerManager.Add(command, 2.5f);
        }

        public static void DeactivatePlayerChoose()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.PlayerChoose).setVisible(false);
        }

        public static void CreatePlayerChosen()
        {
            FontManager.Add(Font.Name.PLAYER, SpriteLayer.Name.PlayerChosen, "", 120, 375);
        }

        public static void ActivatePlayerChosen()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.PlayerChosen).setVisible(true);
            Command command = new StateChangeCommand(GamePlayingState.Instance(), GameManager.GetState());
            TimerManager.Add(command, 2.0f);

            Font message = FontManager.FindByName(Font.Name.PLAYER);
            int playerNum = GameManager.GetCurrentPlayer().getNum();
            int levelNum = GameManager.GetLevel() + 1;
            message.UpdateMessage("PLAYER <" + playerNum + "> LEVEL <" + levelNum + ">");
        }

        public static void DeactivatePlayerChosen()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.PlayerChosen).setVisible(false);
            Font message = FontManager.FindByName(Font.Name.PLAYER);
            if (message != null)
            message.UpdateMessage("");
        }

        public static void CreateScoreKey(){
        
            FontManager.Add(Font.Name.PLAY, SpriteLayer.Name.ScoreKey, "PLAY", 290, 535);
            FontManager.Add(Font.Name.SPACE, SpriteLayer.Name.ScoreKey, "SPACE", 170, 470);
            FontManager.Add(Font.Name.INVADERS, SpriteLayer.Name.ScoreKey, "INVADERS", 343, 470);
            FontManager.Add(Font.Name.TABLEHEAD, SpriteLayer.Name.ScoreKey, "*SCORE ADVANCE TABLE*", 97, 387);
            FontManager.Add(Font.Name.MYSTERY, SpriteLayer.Name.ScoreKey, "=? MYSTERY", 247, 345);
            FontManager.Add(Font.Name.ALIEN30, SpriteLayer.Name.ScoreKey, "=30", 247, 303);
            FontManager.Add(Font.Name.POINTS, SpriteLayer.Name.ScoreKey, "POINTS", 340, 303);
            FontManager.Add(Font.Name.ALIEN20, SpriteLayer.Name.ScoreKey, "=20", 247, 261);
            FontManager.Add(Font.Name.POINTS, SpriteLayer.Name.ScoreKey, "POINTS", 340, 261);

            Font equalsTenFont = FontManager.Add(Font.Name.ALIEN10, SpriteLayer.Name.ScoreKey, "=10", 249, 219);
            Font pointsFont = FontManager.Add(Font.Name.POINTS, SpriteLayer.Name.ScoreKey, "POINTS", 340, 219);

            equalsTenFont.getSpriteFont().setColor(Color.Name.Green);
            pointsFont.getSpriteFont().setColor(Color.Name.Green);

            ImageManager.Add(Image.Name.Squid1, Texture.Name.Aliens, 60.0f, 540.0f, 120.0f, 120.0f);
            Sprite sSprite = SpriteManager.Add(Sprite.Name.Squid, Image.Name.Squid1, 215.0f, 305.0f, 25.0f, 22.0f);
            SpriteProxy sSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, sSprite, sSprite.getX(), sSprite.getY());
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ScoreKey, sSpriteProxy);

            ImageManager.Add(Image.Name.Crab2, Texture.Name.Aliens, 276.0f, 300.0f, 168.0f, 120.0f);
            Sprite cSprite = SpriteManager.Add(Sprite.Name.Crab, Image.Name.Crab2, 215.0f, 263.0f, 32.25f, 22.0f);
            SpriteProxy cSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, cSprite, cSprite.getX(), cSprite.getY());
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ScoreKey, cSpriteProxy);
            
            ImageManager.Add(Image.Name.Octopus1, Texture.Name.Aliens, 32.0f, 60.0f, 184.0f, 120.0f);
            Sprite oSprite = SpriteManager.Add(Sprite.Name.Octopus, Image.Name.Octopus1, 215.0f, 222.0f, 40.0f, 22.0f);
            SpriteProxy oSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, oSprite, oSprite.getX(), oSprite.getY());
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ScoreKey, oSpriteProxy);
            
            ImageManager.Add(Image.Name.UFO1, Texture.Name.Aliens, 120.0f, 788.0f, 240.0f, 105.0f);
            Sprite ufoSprite = SpriteManager.Add(Sprite.Name.UFO, Image.Name.UFO1, 215.0f, 345.0f, 44.0f, 20.0f);
            SpriteProxy ufoSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, ufoSprite, ufoSprite.getX(), ufoSprite.getY());
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ScoreKey, ufoSpriteProxy);
         
        }

        public static void ActivateScoreKey()
        {
            Command command = new StateChangeCommand(PlayerChooseState.Instance(), GameManager.GetState());
            TimerManager.Add(command, 3.0f);
            
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ScoreKey).setVisible(true);
            SpriteManager.Add(Sprite.Name.Octopus).setColor(Color.Name.Green);
            SpriteManager.Add(Sprite.Name.UFO).setColor(Color.Name.White);
        }

        public static void DeactivateScoreKey()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ScoreKey).setVisible(false);
            SpriteManager.Add(Sprite.Name.Octopus).setColor(Color.Name.White); 
        }

        public static void CreateGameOver()
        {
            FontManager.Add(Font.Name.GAMEOVER, SpriteLayer.Name.GameOver, "GAME OVER", 220, 375);
        }

        public static void ActivateGameOver()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.GameOver).setVisible(true);
            Command command = new StateChangeCommand(AttractState.Instance(), GameManager.GetState());
            TimerManager.Add(command, 4.0f);
        }

        public static void DeactivateGameOver()
        {
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.GameOver).setVisible(false);
        }

        public static void ActivateAttractOverlay()
        {
            SpriteLayer overlayScoreLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayScores);
            overlayScoreLayer.setVisible(true);
            SpriteLayer overlayCreditsLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayCredits);
            overlayCreditsLayer.setVisible(true);

        }

        public static void DeactivateAttractOverlay()
        {
            SpriteLayer overlayScoreLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayScores);
            overlayScoreLayer.setVisible(false);
            SpriteLayer overlayCreditsLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayCredits);
            overlayCreditsLayer.setVisible(false);
          
        }

    }
}
