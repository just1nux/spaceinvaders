﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    abstract class CollisionLink
    {
        private CollisionLink colNext;
        private CollisionLink colPrev;

        protected CollisionLink()
        {
            this.initialize();
        }

        protected void initialize()
        {
            this.colNext = null;
            this.colPrev = null;
        }

        public CollisionLink getNext(){
            return this.colNext;
        }

        public void setNext(CollisionLink colNext){
            this.colNext = colNext;
        }

        public CollisionLink getPrev(){
            return this.colPrev;
        }

        public void setPrev(CollisionLink colPrev){
            this.colPrev = colPrev;
        }
    }
}
