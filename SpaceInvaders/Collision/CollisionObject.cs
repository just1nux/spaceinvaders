﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class CollisionObject
    {
        public SpriteBox collisionSprite;
        public CollisionRect collisionRect;
        private bool drawCollisionSprite;

        public CollisionObject(SpriteProxy spriteProxy, bool drawCollisionSprite=true)
        {
            setDrawCollisionSprite(drawCollisionSprite);
            Debug.Assert(spriteProxy != null);

            SpriteBase sprite = spriteProxy.getSprite();
            Debug.Assert(sprite != null);

            this.collisionRect = new CollisionRect(sprite.getAzulRect());


            Debug.Assert(this.collisionRect != null);

            this.collisionSprite = SpriteBoxManager.Add(SpriteBox.Name.Borderbox, Color.Name.Red, this.collisionRect.getX(), this.collisionRect.getY(), this.collisionRect.getW(), this.collisionRect.getH());
             
            //Collision Sprite

            if (isDrawCollisionSprite())
            {
                SpriteLayerManager.AddSprite(SpriteLayer.Name.CollisionLayer, collisionSprite);
            }
        }

        public void update(float x, float y, float w, float h)
        {
            if (collisionRect != null)
            {


                this.collisionRect.setX(x);
                this.collisionRect.setY(y);
                this.collisionRect.setW(w);
                this.collisionRect.setH(h);

                if (collisionSprite != null)
                {
                    this.collisionSprite.setX(this.collisionRect.getX());
                    this.collisionSprite.setY(this.collisionRect.getY());
                    this.collisionSprite.setW(this.collisionRect.getW());
                    this.collisionSprite.setH(this.collisionRect.getH());
                }
            }
        }

        public void setDrawCollisionSprite(bool drawCollisionSprite)
        {
            this.drawCollisionSprite = drawCollisionSprite;
        }

        public bool isDrawCollisionSprite()
        {
            return drawCollisionSprite;
        }
    }
}

