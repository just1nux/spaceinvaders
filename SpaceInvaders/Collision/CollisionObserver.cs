﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    abstract class CollisionObserver : CollisionLink
    {
        public abstract void notify();
        public CollisionSubject subject;

        public virtual void Execute()
        {
            // default
        }

        public void setSubject(CollisionSubject subject){
            this.subject = subject;
        }

        public CollisionSubject getSubject(){
            return this.subject;
        }
    }
}