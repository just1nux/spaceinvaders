﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class CollisionPair : Container
    {
        public CollisionPair.Name name;
        public int index;
        public GameObject gameObjectA;
        public GameObject gameObjectB;
        public CollisionSubject subject;


        public enum Name
        {
            Uninitialized,
            NullObject,
            AlienMissile,  
            AlienWall,
            AlienShield,
            ShipWall,
            MissileWall,
            MissileShield,
            MissileUFO,
            BombWall,
            BombShield,
            BombShip,
            BombMissile,
            UFOWall,
        }

        public CollisionPair()
            : base()
        {
        }

        public override void initialize()
        {
            this.gameObjectA = null;
            this.gameObjectB = null;
            this.name = CollisionPair.Name.Uninitialized;
            this.index = 0;
            setSubject(null);
        }


        ~CollisionPair()
        {
            setSubject(null);
        }

        public void Set(CollisionPair.Name colpairName, int index, GameObject treeRootA, GameObject treeRootB)
        {
            Debug.Assert(treeRootA != null);
            Debug.Assert(treeRootB != null);

            setSubject(new CollisionSubject());

            this.gameObjectA = treeRootA;
            this.gameObjectB = treeRootB;
            this.name = colpairName;
            this.index = index;


        }

        public void attach(CollisionObserver observer)
        {
            Debug.Assert(this.getSubject() != null);
            this.getSubject().attach(observer);
        }

        public void notifyListeners()
        {
            Debug.Assert(this.getSubject() != null);
            this.getSubject().notify();
        }

        public void setName(CollisionPair.Name name)
        {
            this.name = name;
        }

        public CollisionPair.Name getName(){
            return this.name;
        }


        public void setSubject(CollisionSubject subject)
        {
            this.subject = subject;
        }

        public CollisionSubject getSubject()
        {
            return this.subject;
        }


        public int getIndex()
        {
            return index;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public void Process()
        {
            Collide(this.gameObjectA, this.gameObjectB);
        }


        static public void Collide(GameObject pSafeTreeA, GameObject pSafeTreeB)
        {
            GameObject pNodeA = pSafeTreeA;
            GameObject pNodeB = pSafeTreeB;

            while (pNodeA != null)
            {
                pNodeB = pSafeTreeB;

                while (pNodeB != null)
                {
                  //  Debug.WriteLine("ColPair: collide:  {0}, {1}", pNodeA.getName(), pNodeB.getName());

                    CollisionRect collisionRectA = pNodeA.collisionObject.collisionRect;
                    CollisionRect collisionRectB = pNodeB.collisionObject.collisionRect;

                    if (CollisionRect.Intersect(collisionRectA, collisionRectB))
                    {
                        pNodeA.Accept(pNodeB);
                        break;
                    }

                    pNodeB = (GameObject)pNodeB.getSibling();
                }
                pNodeA = (GameObject)pNodeA.getSibling();
            }
        }

        public void setCollision(GameObject gameObjectA, GameObject gameObjectB)
        {
            Debug.Assert(gameObjectA != null);
            Debug.Assert(gameObjectB != null);

            this.getSubject().setGameObjectA(gameObjectA);
            this.getSubject().setGameObjectB(gameObjectB);
        }

        public override void print()
        {
            if (gameObjectA != null && gameObjectB != null){
           Debug.WriteLine("Collision Pair: {0} visits {1}", gameObjectA.getName().ToString(), gameObjectB.getName().ToString());
            }
        }
    }
}
