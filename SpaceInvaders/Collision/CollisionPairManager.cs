﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class CollisionPairManager : ContainerManager
    {
        private static volatile CollisionPairManager instance;
        private CollisionPair activeCollisionPair;

        private CollisionPairManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
            
        }

        public static CollisionPairManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new CollisionPairManager(reserveNum, reserveGrow);
                setActiveCollisionPair(null);
            }
            return instance;
        }

        ~CollisionPairManager()
        {
            CollisionPairManager.instance = null;
        }

        public static CollisionPair Add(CollisionPair.Name name, GameObject.Name gameObjectNameA, GameObject.Name gameObjectNameB, int gameObjectIndexA=0, int gameObjectIndexB=0)
        {
            GameObject gameObjectA = GameObjectFactory.NewGameObject(gameObjectNameA, gameObjectIndexA);
            GameObject gameObjectB = GameObjectFactory.NewGameObject(gameObjectNameB, gameObjectIndexB);
            
            return CollisionPairManager.Add(name, 0, gameObjectA, gameObjectB);
        }


        public static CollisionPair Add(CollisionPair.Name name, GameObject gameObjectA, GameObject gameObjectB)
        {
            return CollisionPairManager.Add(name, 0, gameObjectA, gameObjectB);
        }

        public static CollisionPair Add(CollisionPair.Name name, int index, GameObject gameObjectA, GameObject gameObjectB)
        {
            CollisionPairManager manager = CollisionPairManager.Instance();

            CollisionPair pair = (CollisionPair)manager.addLink();
            pair.Set(name, index, gameObjectA, gameObjectB);

            return pair;
        }

        public static void Remove(Container pair)
        {
            CollisionPairManager manager = CollisionPairManager.Instance();

            Debug.Assert(pair != null);
            manager.remove(ref pair);
        }

        protected override Container create()
        {
            Container link = new CollisionPair();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            CollisionPair nodeA = (CollisionPair)linkA;
            CollisionPair nodeB = (CollisionPair)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }
        
        public static CollisionPair Find(CollisionPair.Name name, int index=0)
        {
            CollisionPairManager manager = CollisionPairManager.Instance();

            CollisionPair foundNode = null;
            // if there are active links to search
            if (manager.getpActive() != null)
            {
                //pull a reserve node for use
                CollisionPair node = (CollisionPair)manager.pullFromReserve();
                node.setName(name);
                node.setIndex(index);

                foundNode = (CollisionPair)manager.searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                manager.addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Process()
        {
            CollisionPairManager manager = CollisionPairManager.Instance();

            CollisionPair pair = (CollisionPair)manager.getpActive();

            while (pair != null)
            {
                setActiveCollisionPair(pair);
                pair.Process();
                pair = (CollisionPair)pair.getNext();
            }
        }

        static public CollisionPair getActiveCollisionPair()
        {
            CollisionPairManager manager = CollisionPairManager.Instance();
            return manager.activeCollisionPair;
        }

        static public void setActiveCollisionPair(CollisionPair pair)
        {
            CollisionPairManager manager = CollisionPairManager.Instance();
            manager.activeCollisionPair = pair;
        }

        public static void Print()
        {
            CollisionPairManager manager = CollisionPairManager.Instance();
            Debug.WriteLine("----Collision Pair Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            CollisionPairManager manager = CollisionPairManager.Instance();
            manager.baseDestroy();
        }

        public static void CreateCollisionPairs(){

            //******** UFO *********
            //Ufo Hit Wall
            CollisionPair UFOWallCollisionPair = CollisionPairManager.Add(CollisionPair.Name.UFOWall, GameObject.Name.UFORoot, GameObject.Name.WallRoot);
            UFOWallCollisionPair.attach(new ObserverRemoveUFO());
            UFOWallCollisionPair.attach(new ObserverStopSounds());

            //******** GRID *********
            //Grid Hits Wall Right
            CollisionPair gridWallRCollisionPair = CollisionPairManager.Add(CollisionPair.Name.AlienWall, GameObject.Name.Grid, GameObject.Name.WallRight);
            gridWallRCollisionPair.attach(new ObserverGrid());

            //Grid Hits Wall Left
            CollisionPair gridWallLCollisionPair = CollisionPairManager.Add(CollisionPair.Name.AlienWall, GameObject.Name.Grid, GameObject.Name.WallLeft);
            gridWallLCollisionPair.attach(new ObserverGrid());

            //******** SHIP **********
            //Ship hits Left or Right Wall
            CollisionPair shipWallCollisionPair = CollisionPairManager.Add(CollisionPair.Name.ShipWall, GameObject.Name.WallShipRoot, GameObject.Name.ShipRoot);
            shipWallCollisionPair.attach(new ObserverShip());

            //Grid Hit Sheild
            CollisionPair gridShieldCollisionPair = CollisionPairManager.Add(CollisionPair.Name.AlienShield, GameObject.Name.Grid, GameObject.Name.ShieldRoot);
            gridShieldCollisionPair.attach(new ObserverEndLife());
            gridShieldCollisionPair.attach(new ObserverPlayerLoseLife());
            gridShieldCollisionPair.attach(new ObserverResetGrid());
           // gridShieldCollisionPair.attach(new ObserverExplodeShipVisual());

            //********* MISSILE **********

            //Missile Hits Alien
            CollisionPair alienCollisionPair = CollisionPairManager.Add(CollisionPair.Name.AlienMissile, GameObject.Name.MissileRoot, GameObject.Name.Grid);
            alienCollisionPair.attach(new ObserverRemoveAlien());
            alienCollisionPair.attach(new ObserverRemoveMissile());
            alienCollisionPair.attach(new ObserverIncreaseScore());
            alienCollisionPair.attach(new ObserverExplodeAlienSound());
            alienCollisionPair.attach(new ObserverExplodeAlienVisual());

            //Missile Hits UFO
            CollisionPair ufoCollisionPair = CollisionPairManager.Add(CollisionPair.Name.MissileUFO, GameObject.Name.MissileRoot, GameObject.Name.UFORoot);
            ufoCollisionPair.attach(new ObserverUFOExplosion());
            ufoCollisionPair.attach(new ObserverPlaySounds(Soundk.Name.ufolowpitch));
            ufoCollisionPair.attach(new ObserverRemoveMissile());
            ufoCollisionPair.attach(new ObserverRemoveUFO());
            ufoCollisionPair.attach(new ObserverStopSounds());

            //Missile hit top wall
            CollisionPair missileWallCollisionPair = CollisionPairManager.Add(CollisionPair.Name.MissileWall, GameObject.Name.MissileRoot, GameObject.Name.WallTop);
            missileWallCollisionPair.attach(new ObserverRemoveMissile());
            missileWallCollisionPair.attach(new ObserverExplodeWallMissileVisual());

            //Missile hit Shield
            CollisionPair missileShieldCollisionPair = CollisionPairManager.Add(CollisionPair.Name.MissileShield, GameObject.Name.MissileRoot, GameObject.Name.ShieldRoot);
            missileShieldCollisionPair.attach(new ObserverExplodeShieldMissileVisual());
            missileShieldCollisionPair.attach(new ObserverRemoveShieldCube());
            missileShieldCollisionPair.attach(new ObserverRemoveMissile());

            //Missile Hit Bomb
            CollisionPair bombMissileCollisionPair = CollisionPairManager.Add(CollisionPair.Name.BombMissile, GameObject.Name.BombRoot, GameObject.Name.MissileRoot);
            bombMissileCollisionPair.attach(new ObserverRemoveBomb());
            bombMissileCollisionPair.attach(new ObserverRemoveMissile());
            bombMissileCollisionPair.attach(new ObserverMissileBombExplosion());

            //********* BOMB **********

            //Bomb hit Bottom Wall
            CollisionPair bombBottomWallCollisionPair = CollisionPairManager.Add(CollisionPair.Name.BombWall, GameObject.Name.BombRoot, GameObject.Name.WallBottom);
            bombBottomWallCollisionPair.attach(new ObserverRemoveBomb());
            bombBottomWallCollisionPair.attach(new ObserverExplodeWallBombVisual());

            //Bomb hit Shield Root
            CollisionPair bombShieldCollisionPair = CollisionPairManager.Add(CollisionPair.Name.BombShield, GameObject.Name.BombRoot, GameObject.Name.ShieldRoot);
            bombShieldCollisionPair.attach(new ObserverExplodeShieldBombVisual());
            bombShieldCollisionPair.attach(new ObserverRemoveShieldCube());
            bombShieldCollisionPair.attach(new ObserverRemoveBomb());

            //Bomb hit Ship
            CollisionPair bombShipCollisionPair = CollisionPairManager.Add(CollisionPair.Name.BombShip, GameObject.Name.BombRoot, GameObject.Name.ShipRoot);
            bombShipCollisionPair.attach(new ObserverPlayerLoseLife());
            bombShipCollisionPair.attach(new ObserverExplodeShipVisual());
            bombShipCollisionPair.attach(new ObserverEndLife());
            bombShipCollisionPair.attach(new ObserverRemoveBomb());
            bombShipCollisionPair.attach(new ObserverRemoveShip());
            
        }
    }
}

