﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class CollisionRect : Rect
    {
        public CollisionRect(float x, float y, float w, float h): base(x, y, w, h)
        {

        }

        public CollisionRect(Azul.Rect rect): base(rect)
        {

        }
    }
}

