﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class CollisionSubject
    {
        private CollisionObserver observers;
        private GameObject gameObjectA;
        private GameObject gameObjectB;

        public CollisionSubject()
        {
            this.setGameObjectA(null);
            this.setGameObjectB(null);
            this.setObservers(null);
        }

        ~CollisionSubject()
        {
            this.setGameObjectA(null);
            this.setGameObjectB(null);
            this.setObservers(null);
            //TODO: List nodes not removed
        }

        public void setGameObjectA(GameObject gameObjectA)
        {
            this.gameObjectA = gameObjectA;
        }

        public GameObject getGameObjectA()
        {
            return this.gameObjectA;
        }

        public void setGameObjectB(GameObject gameObjectB)
        {
            this.gameObjectB = gameObjectB;
        }

        public GameObject getGameObjectB()
        {
            return this.gameObjectB;
        }

        public void setObservers(CollisionObserver observers)
        {
            this.observers = observers;
        }

        public CollisionObserver getObservers()
        {
            return this.observers;
        }

        public void attach(CollisionObserver observer)
        {
            Debug.Assert(observer != null);

            observer.setSubject(this);

            if (observers == null)
            {
                setObservers(observer);
                observer.setNext(null);
                observer.setPrev(null);
            }
            else
            {
                observer.setNext(observers);
                observers.setPrev(observer);
                setObservers(observer);
            }

        }

        public void notify()
        {
            CollisionObserver observer = this.observers;

            //Loop through listeners and notify them
            while (observer != null)
            {
                observer.notify();

                observer = (CollisionObserver)observer.getNext();
            }

        }
    }
}
