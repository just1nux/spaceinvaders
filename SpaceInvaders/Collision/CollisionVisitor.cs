﻿using System;
using System.Diagnostics;


namespace SpaceInvaders
{

    public abstract class CollisionVisitor : PCSNode
    {
        abstract public void Accept(CollisionVisitor other);

        public virtual void VisitGrid(Grid a)
        {
            throw new NotImplementedException("Visit from Grid unimplemented");
        }

        public virtual void VisitColumn(Column a)
        {
            throw new NotImplementedException("Visit from Column unimplemented");
        }

        public virtual void VisitCrab(Crab a)
        {
            throw new NotImplementedException("Visit from Crab unimplemented");
        }

        public virtual void VisitSquid(Squid a)
        {
            throw new NotImplementedException("Visit from Squid unimplemented");
        }

        public virtual void VisitOctopus(Octopus a)
        {
            throw new NotImplementedException("Visit from Octopus unimplemented");
        }

        public virtual void VisitShip(Ship m)
        {
            throw new NotImplementedException("Visit from Ship unimplemented"); 
        }

        public virtual void VisitShipRoot(ShipRoot m)
        {
            throw new NotImplementedException("Visit from ShipRoot unimplemented");
        }

        public virtual void VisitMissile(Missile m)
        {
            throw new NotImplementedException("Visit from Missile unimplemented");
        }

        public virtual void VisitMissileRoot(MissileRoot r)
        {
            throw new NotImplementedException("Visit from MissileRoot unimplemented");
        }

        public virtual void VisitNullGameObject(GameObjectNull n)
        {
            throw new NotImplementedException("Visit from GameObjectNull unimplemented");
        }

        public virtual void VisitWallRoot(WallRoot w)
        {
            throw new NotImplementedException("Visit from WallRoot unimplemented");
        }

        public virtual void VisitWallShipRoot(WallShipRoot w)
        {
            throw new NotImplementedException("Visit from WallShipRoot unimplemented");
        }


        public virtual void VisitWall(Wall w)
        {
            throw new NotImplementedException("Visit from Wall unimplemented");
        }

        public virtual void VisitShieldRoot(ShieldRoot s)
        {
            throw new NotImplementedException("Visit from Shield Root unimplemented");
        }

        public virtual void VisitShield(Shield s)
        {
            throw new NotImplementedException("Visit from Shield unimplemented");
        }
        
        public virtual void VisitShieldColumn(ShieldColumn s)
        {
            throw new NotImplementedException("Visit from Shield Column unimplemented");
        }
         
        public virtual void VisitShieldCube(ShieldCube s)
        {
            throw new NotImplementedException("Visit from Shield Cube unimplemented");
        }

        public virtual void VisitBomb(Bomb b)
        {
            throw new NotImplementedException("Visit from Bomb unimplemented.");
        }

        public virtual void VisitBombRoot(BombRoot b)
        {
            throw new NotImplementedException("Visit from Bomb Root unimplemented.");
        }

        public virtual void VisitUFORoot(UFORoot u)
        {
            throw new NotImplementedException("Visit from UFO Root unimplemented.");
        }

        public virtual void VisitUFO(UFO u)
        {
            throw new NotImplementedException("Visit from UFO unimplemented.");
        }
    }

}