﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Color : Container
    {
        private Name name;
        private float red;
        private float green;
        private float blue;
        private float alpha;

        public Color()
        {
            initialize();
        }

        public void set(float red, float green, float blue, float alpha)
        {
            setRed(red);
            setGreen(green);
            setBlue(blue);
            setAlpha(alpha);
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public Azul.Color getAzulColor()
        {
           Azul.Color azulColor = new Azul.Color(getRed(), getGreen(), getBlue(), getAlpha());
           //Debug.WriteLine("{0} {1}", this.getName(), azulColor.alpha); 
           return azulColor;
        }

        public void setRed(float red){
            this.red = red;
        }

        public float getRed(){
            return this.red;
        }

        public void setGreen(float green)
        {
            this.green = green;
        }

        public float getGreen()
        {
            return this.green;
        }

        public void setBlue(float blue)
        {
            this.blue = blue;
        }

        public float getBlue()
        {
            return this.blue;
        }

        public void setAlpha(float alpha)
        {
            this.alpha = alpha;
        }

        public float getAlpha()
        {
            return this.alpha;
        }

        public enum Name
        {
            Uninitialized,
            NullColor,
            Red,
            Green,
            Blue,
            Black,
            White,
            Dark,
        }

    }
}
