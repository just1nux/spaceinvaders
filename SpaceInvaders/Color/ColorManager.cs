﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class ColorManager : ContainerManager
    {
        private static volatile ColorManager instance;

        private ColorManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {

        }

        public static ColorManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new ColorManager(reserveNum, reserveGrow);
                ColorManager.Add(Color.Name.Green, 0.0f, 1.0f, 0.0f, 1.0f);
                ColorManager.Add(Color.Name.Red, 1.0f, 0.0f, 0.0f, 1.0f);
                ColorManager.Add(Color.Name.Blue, 0.0f, 0.0f, 1.0f, 1.0f);
                ColorManager.Add(Color.Name.Black, 0.0f, 0.0f, 0.0f, 1.0f);
                ColorManager.Add(Color.Name.Dark, 0.0f, 0.0f, 0.50f, 1.0f);
                ColorManager.Add(Color.Name.White, 1.0f, 1.0f, 1.0f, 1.0f);
            }
            return instance;
        }

        public static Color Add(Color.Name name)
        {
            ColorManager manager = ColorManager.Instance();
            Color node = (Color)manager.addLink();
            node.setName(name);
            return node;
        }

        public static Color Add(Color.Name name, float r, float g, float b, float a)
        {
            ColorManager manager = ColorManager.Instance();
            Color color = manager.findActiveByName(name);
            if (color == null){
                color = Add(name);
                color.set(r, g, b, a);
            }

            return color;
        }

        protected override Container create()
        {
            Container link = new Color();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Color nodeA = (Color)linkA;
            Color nodeB = (Color)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public Color findActiveByName(Color.Name name)
        {
            Color foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Color node = (Color)pullFromReserve();
                node.setName(name);
                foundNode = (Color)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            ColorManager manager = ColorManager.Instance();
            Debug.WriteLine("----Color Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            ColorManager manager = ColorManager.Instance();
            manager.baseDestroy();
        }
    }
}
