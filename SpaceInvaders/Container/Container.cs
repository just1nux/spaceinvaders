﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{


    abstract public class Container
    {
        public enum Status
        {
            UNINITIALIZED,
            INITIALIZED,
            ACTIVE,
            RESERVE,
        }

        protected Container next;
        protected Container prev;
        protected Status containerStatus;

        abstract public void print();
        abstract public void initialize();

        public Container()
        {
            clear();
            setStatus(Container.Status.UNINITIALIZED);
            initialize();
        }

        ~Container()
        {
            initialize();
        }

        public Status getStatus()
        {
            return this.containerStatus;
        }

        public void setStatus(Status containerStatus)
        {
            this.containerStatus = containerStatus;
        }

        public bool isUninitialized()
        {
            if (containerStatus == Status.UNINITIALIZED)
                return true;
            else
                return false;
        }

        public bool isActive()
        {
            if (containerStatus == Status.ACTIVE)
                return true;
            else
                return false;
        }

        public bool isReserve()
        {
            if (containerStatus == Status.RESERVE)
                return true;
            else
                return false;
        }

        public void setPrev(Container prev)
        {
            this.prev = prev;
        }

        public void setNext(Container next)
        {
            this.next = next;
        }

        public Container getPrev()
        {
            return this.prev;
        }

        public Container getNext()
        {
            return this.next;
        }

        public void clear()
        {
            setNext(null);
            setPrev(null);
        }

        public void printBase()
        {
            print();
        }

    }
}
