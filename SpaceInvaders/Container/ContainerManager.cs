﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class ContainerManager
    {
        protected int mNumActive;
        protected int mNumReserve;
        protected int mDeltaGrow;
        protected int mInitialNumReserved;
        protected int mTotalNumLinks;

        public Container pReserve;
        public Container pActive;

        public int getmNumActive() { return mNumActive; }
        public int getmNumReserve() { return mNumReserve; }
        public int getmTotalNumLinks() { return mTotalNumLinks; }
        public int getmDeltaGrow() { return mDeltaGrow; }
        public int getmInitialNumReserved() { return mInitialNumReserved; }
        public Container getpReserve() { return pReserve; }
        public Container getpActive() { return pActive; }

        public void setmNumActive(int num) { mNumActive = num; }
        public void setmNumReserve(int num) { mNumReserve = num; }
        public void setmTotalNumLinks(int num) { mTotalNumLinks = num; }
        public void setmDeltaGrow(int num) { mDeltaGrow = num; }
        public void setmInitialNumReserved(int num) { mInitialNumReserved = num; }
        public void setpReserve(Container link) { pReserve = link; }
        public void setpActive(Container link) { pActive = link; }

        public void incrementmNumActive() { mNumActive++; }
        public void incrementmNumReserve() { mNumReserve++; }
        public void incrementmTotalNumLinks() { mTotalNumLinks++; }
        public void decrementmNumActive() { mNumActive--; }
        public void decrementmNumReserve() { mNumReserve--; }
        public void decrementmTotalNumLinks() { mTotalNumLinks--; }

        abstract protected Boolean compare(Container linkA, Container linkB);
        abstract protected Container create();

        protected ContainerManager(int reserveStart = 3, int reserveIncrease = 1)
        {
            Debug.Assert(reserveStart > 0);
            Debug.Assert(reserveIncrease > 0);

            setmNumActive(0);
            setmNumReserve(0);
            setmTotalNumLinks(0);
            setpReserve(null);
            setpActive(null);

            setmDeltaGrow(2);
            setmInitialNumReserved(5);

            increaseReserve(getmInitialNumReserved());
        }

        //PUBLIC METHODS
   
        public void remove(ref Container link)
        {
            removeFromActive(link);
            addToReserve(ref link);
        }

        protected void printTotals()
        {
            printLinks("ACTIVE", getpActive(), getmNumActive());
            printLinks("RESERVE", getpReserve(), getmNumReserve());
            Debug.WriteLine("TOTAL: {0} links", getmTotalNumLinks());
            Debug.WriteLine("\n");
        }

        protected void printLinks(String nName, Container container, int num)
        {
            Debug.WriteLine("{0}: {1} Links", nName, num);

            while (container != null)
            {
                container.printBase();
                container = (Container) container.getNext();
            }
        }

        //RESERVE MANAGEMENT

        private void checkReserve()
        {
            //reserve more links if reserve is empty
            if (pReserve == null)
            {
                increaseReserve(getmDeltaGrow());
            }
        }

        protected void increaseReserve(int num)
        {
            //add reserve links regardless of number available
            for (int y = 0; y < num; y++)
            {
                Container container = create();
                addToReserve(ref container);
                incrementmTotalNumLinks();
            }
        }

        //ADD SUPPORT
        protected Container addLink()
        {
            Container link = pullFromFront(ref pReserve);
            addToActive(ref link);
            decrementmNumReserve();
            return link;
        }

        protected void addToReserve(ref Container link)
        {
            Debug.Assert(link != null);

            if (link.isUninitialized())
            {
                addToFront(ref link, ref pReserve);
                link.setStatus(Container.Status.RESERVE);
                link.initialize();
                incrementmNumReserve();
            }
        }

        protected void addToActive(ref Container link)
        {
            Debug.Assert(link != null);

            if (link.isUninitialized())
            {
                addToFront(ref link, ref pActive);
                link.setStatus(Container.Status.ACTIVE);
                incrementmNumActive();
            }
        }

        protected void addToActiveSorted(ref Container link)
        {
            Debug.Assert(link != null);

            link.setStatus(Container.Status.ACTIVE);
            addToCenter(ref link, ref pActive);
            incrementmNumActive();         
        }


        private void addToFront(ref Container link, ref Container front)
        {
            if (front != null)
            {
                link.setNext(front);
                front.setPrev(link);
            }
            front = link;
        }



        private void addToCenter(ref Container link, ref Container front)
        {
           
            Container current;

            if (front == null || !compare(front, link))
            {
                link.setNext(front);
                front = link;
            } else {
                current = front;
                while (current.getNext() != null && compare(current.getNext(), link))
                {
                    current = current.getNext();
                }
            
            link.setNext(current.getNext());
            current.setNext(link);
            }
        }

        //PULL SUPPORT

        protected Container pullFromActive()
        {
            Container link = pullFromFront(ref pActive);
            decrementmNumActive();
            return link;
        }

        protected Container pullFromReserve()
        {
            Container link = pullFromFront(ref pReserve);
            decrementmNumReserve();
            return link;
        }

        protected Container pullFromFront(ref Container pLink)
        {
            checkReserve();

            Container link = pLink;

            if (pLink.getNext() != null)
            {
                pLink = pLink.getNext();
                pLink.setPrev(null);
            }
            else
            {
                pLink = null;
            }

            link.clear();
            link.setStatus(Container.Status.UNINITIALIZED);
            return link;
        }

        //REMOVE SUPPORT

        protected void removeFromActive(Container link)
        {
            if (link.isActive())
            {
                removeLink(ref pActive, link);
                decrementmNumActive();
            }
        }

        protected void removeFromReserve(Container link)
        {
            if (link.isReserve())
            {
                removeLink(ref pReserve, link);
                decrementmNumReserve();
            }
        }

        private void removeLink(ref Container head, Container link)
        {
            Debug.Assert(link != null);

            if (link.getPrev() != null)
            {	// middle or last
                link.getPrev().setNext(link.getNext());
            }
            else
            {  // first
                head = link.getNext();
            }

            if (link.getNext() != null)
            {	//middle
                link.getNext().setPrev(link.getPrev());
            }

            link.clear();
            link.setStatus(Container.Status.UNINITIALIZED);
        }

        //FIND SUPPORT

        protected Container searchActiveForLink(Container link)
        {
            Container pLink = this.pActive;
            Container foundLink = null;

            while (pLink != null && foundLink == null)
            {
                if (compare(link, pLink))
                    foundLink = pLink;
                pLink = pLink.getNext();
            }

            return foundLink;
        }

        //DESTROY

        protected void baseDestroy()
        {
            //Remove from Active List

            Container node = this.pActive;
            Container tmpNode;

            while (node != null)
            {
                tmpNode = node.getNext();
                removeFromActive(node);
                node = tmpNode;
                decrementmTotalNumLinks();
                tmpNode = null;
            }

            // Remove All from Reserve
            node = this.pReserve;
            
            while (node != null)
            {
                tmpNode = node.getNext();
                removeFromReserve(node);
                node = tmpNode;
                decrementmTotalNumLinks();
                tmpNode = null;
            }
        }

    }
}
