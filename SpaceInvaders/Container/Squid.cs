﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Squid : Alien
    {
        private Name name;
        public Squid(): base()
        {
            
        }

        public override void initialize()
        {
            /*name = Name.Uninitialized;

            imageManager.add(Image.Name.Squid1, Texture.Name.Aliens, 570.0f, 0.0f, 120.0f, 120.0f);
            imageManager.add(Image.Name.Squid2, Texture.Name.Aliens, 700.0f, 350.0f, 82.5f, 60.0f);
            Sprite sprite = spriteManager.add(Sprite.Name.Squid, Image.Name.Squid1, 330.0f, 500.0f, 60.0f, 60.0f);
            SpriteLayer squidBatch = spriteLayerManager.add(SpriteLayer.Name.SquidLayer);
            squidBatch.add(Sprite.Name.Squid);
            setSprite(sprite);
             */
        }

        public Name getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public enum Name
        {
            Uninitialized,
            Squid1,
            Squid2,
            Squid3,
            Squid4,
            Squid5,
        }
    }
}
