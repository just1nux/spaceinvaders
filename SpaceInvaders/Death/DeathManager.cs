﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class DeathManager : ContainerManager
    {
        private static DeathManager instance = null;
        private DeathNode deathNode;

        private DeathManager(int reserveNum = 3, int reserveGrow = 1)
            : base(reserveNum, reserveGrow)
        {
            this.deathNode = (DeathNode)this.create();
        }

        ~DeathManager()
        {
            this.deathNode = null;
            DeathManager.instance = null;
        }

        public static DeathManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new DeathManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        protected override Container create()
        {
            Container link = new DeathNode();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            return false;
        }

    
        public static void Destroy()
        {
            DeathManager manager = DeathManager.Instance();
            manager.baseDestroy();
        }
        
        
        public static DeathNode Add(object obj)
        {
            DeathManager manager = DeathManager.Instance();

            DeathNode node = (DeathNode)manager.addLink();
            Debug.Assert(node != null && obj != null);
            node.set(obj);
            return node;
        }

        public static void Print()
        {
            DeathManager manager = DeathManager.Instance();
            Debug.WriteLine("----Death Manager----");
            manager.printTotals();
        }

}
}
