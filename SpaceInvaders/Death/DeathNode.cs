﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class DeathNode : Container
    {
        public object gameObject;

        public DeathNode()
            : base()
        {
            this.gameObject = null;
        }

        public override void initialize()
        {

        }

        ~DeathNode()
        {
            this.gameObject = null;
        }

        public void set(object gameObject)
        {
            this.gameObject = gameObject;
        }

        public override void print()
        {
            Debug.WriteLine("\t\tname: {0} ", this.GetHashCode());
        }
    }
}
