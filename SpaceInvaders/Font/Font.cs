﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Font:Container
    {
        private Name name;
        public SpriteFont spriteFont;

        public enum Name
        {
            NULLOBJECT,
            UNINITIALIZED,
            CREDITS,
            NUMCREDITS,
            SCORE,
            NUMSCORE1,
            NUMSCORE2,
            NUMLIVES,
            NUMHIGHSCORE,
            PLAY,
            SPACE,
            INVADERS,
            TABLEHEAD,
            MYSTERY,
            ALIEN30,
            ALIEN20,
            ALIEN10,
            POINTS,
            PUSH,
            ONEORTWO,
            UFOPOINTS,
            GAMEOVER,
            PLAYER,
        }

        public Font()
        {
           this.spriteFont = new SpriteFont();
        }

        public SpriteFont getSpriteFont()
        {
            return this.spriteFont;
        }

        public override void initialize()
        {
            this.name = Name.UNINITIALIZED;
        }

        public void setName(Name name)
        {
            this.name = name;
        }

        public Name getName()
        {
            return this.name;
        }

        public void UpdateMessage(String message)
        {
            Debug.Assert(message != null);
            Debug.Assert(this.spriteFont != null);
            this.spriteFont.UpdateMessage(message);
        }

        public void Set(Font.Name name, String message, Glyph.Name glyphName, float xStart, float yStart)
        {
            Debug.Assert(message != null);

            this.name = name;
            this.spriteFont.Set(name, message, glyphName, xStart, yStart);
        }

        public override void print()
        {
            Debug.WriteLine("Font name:{0}", getName());
        }
    }
}
