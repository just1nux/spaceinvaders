﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class FontManager:ContainerManager
    {
       //private static volatile FontManager instance;

        public FontManager(int reserveNum = 3, int reserveGrow = 1)
            : base(reserveNum, reserveGrow)
        {
            Texture texture = TextureManager.Add(Texture.Name.SpaceInvaders24pt, "sifontsprite24v1.tga");
            GlyphManager.AddXml("sifontsprite24v1.xml", Glyph.Name.SpaceInvaders24pt, Texture.Name.SpaceInvaders24pt);
        }

        public static FontManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            FontManager instance = GameManager.GetCurrentPlayer().fontManagerInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().fontManagerInstance = new FontManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().fontManagerInstance;
            }
            return instance;
        }


        /*
        public static FontManager Instance()
        {

            if (instance == null)
            {
                instance = new FontManager();
            }
            return instance;
        }*/

        public static Font Add (Font.Name fontName)
        {
            FontManager manager = FontManager.Instance();
            Font node = manager.findActiveByName(fontName);

            if (node == null)
            {
                node = (Font) manager.addLink();
                Debug.Assert(node != null);
                node.setName(fontName);
            }

            Debug.Assert(node != null);
            return node;
        }

        public static Font Add(Font.Name name, SpriteLayer.Name layerName, String message, float xStart, float yStart, Glyph.Name glyphName = Glyph.Name.SpaceInvaders24pt)
        {
            FontManager manager = FontManager.Instance();

            Font node = (Font)manager.addLink();
            Debug.Assert(node != null);

            node.Set(name, message, glyphName, xStart, yStart);

            // Add to sprite layer
            SpriteLayerManager.AddSprite(layerName, node.getSpriteFont());
           
            return node;
        }

        public static void AddXml(Glyph.Name glyphName, String assetName, Texture.Name textureName)
        {
            GlyphManager.AddXml(assetName, glyphName, textureName);      
        }
     
        protected override Container create()
        {
            Container link = new Font();
            return link;
        }

        public static Font FindByName(Font.Name name, int index = 0)
        {
            FontManager manager = FontManager.Instance();
            Font font = (Font) manager.findActiveByName(name);
            return font;
        }

        public Font findActiveByName(Font.Name name)
        {
            Font foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Font node = (Font)pullFromReserve();
                node.setName(name);
                foundNode = (Font)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Font nodeA = (Font)linkA;
            Font nodeB = (Font)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public static void Destroy()
        {
            FontManager manager = FontManager.Instance();
            manager.baseDestroy();
        }

        public static void Print()
        {
            FontManager manager = FontManager.Instance();
            Debug.WriteLine("----Font Manager----");
            manager.printTotals();
        }
    }
}    

