﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Glyph:Container
    {
        private String character;
        private Texture texture;
        private Rect rect;
        private Name name;
        private int index;

        public enum Name
        {
            NullObject,
            Uninitialized,
            SpaceInvaders24pt,
        }

        public Glyph()
        {

        }

        public override void initialize()
        {
            this.name = Name.Uninitialized;
            this.rect = null;
            this.texture = null;
            this.character = null;
            this.index = 0;
        }

        public void setCharacter(String character)
        {
            this.character = character;
        }

        public String getCharacter()
        {
            return this.character;
        }

        public void setTexture(Texture texture)
        {
            this.texture = texture;
        }

        public Texture getTexture()
        {
            return this.texture;
        }

        public void setRect(Rect rect)
        {
            this.rect = rect;
        }

        public Rect getRect()
        {
            return this.rect;
        }

        public void setName(Name name)
        {
            this.name = name;
        }

        public Name getName()
        {
            return this.name;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public int getIndex()
        {
            return this.index;
        }

        public override void print()
        {
            Debug.WriteLine("Glyph name:{0} index:{1}", getName(), getIndex());
        }
    }
}
