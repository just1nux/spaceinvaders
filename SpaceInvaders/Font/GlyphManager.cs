﻿using System;
using System.Diagnostics;
using System.Xml;

namespace SpaceInvaders
{
    class GlyphManager:ContainerManager
    {
        private static volatile GlyphManager instance;


        public GlyphManager(int reserveNum = 3, int reserveGrow = 1)
            : base(reserveNum, reserveGrow)
        {
        }

        public static GlyphManager Instance()
        {
            if (instance == null)
            {
                instance = new GlyphManager();
            }
            return instance;
        }

        public static Glyph Add (Glyph.Name glyphName, int glyphIndex, Rect.Name rectName, int rectIndex,  Texture.Name textureName)
        {
            GlyphManager manager = GlyphManager.Instance();
            Glyph node = manager.findActiveByName(glyphName, glyphIndex);
            if (node == null)
            {
                node = (Glyph) manager.addLink();
                Debug.Assert(node != null);
                node.setName(glyphName);
                node.setIndex(glyphIndex);                
                node.setTexture(TextureManager.FindByName(textureName));
                node.setRect(RectManager.FindByName(rectName, rectIndex));
            }

            Debug.Assert(node != null);
            return node;
        }

        protected override Container create()
        {
            Container link = new Glyph();
            return link;
        }

        public static Glyph FindByName(Glyph.Name name, int index = 0)
        {
            GlyphManager manager = GlyphManager.Instance();
            Glyph glyph = (Glyph) manager.findActiveByName(name, index);
            return glyph;
        }

        public Glyph findActiveByName(Glyph.Name name, int index)
        {
            Glyph foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Glyph node = (Glyph)pullFromReserve();
                node.setName(name);
                node.setIndex(index);
                foundNode = (Glyph)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Glyph nodeA = (Glyph)linkA;
            Glyph nodeB = (Glyph)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }

        public static void Destroy()
        {
            GlyphManager manager =GlyphManager.Instance();
            manager.baseDestroy();
        }

        public static void AddXml(String assetName, Glyph.Name glyphName, Texture.Name textureName)
        {
            int key = -1;
            int x = -1;
            int y = -1;
            int w = -1;
            int h = -1;

               using (XmlReader reader = XmlReader.Create(assetName))
            {
                reader.Read();
                while (reader.Read())
                {
                    if (reader.IsStartElement() && reader.LocalName == "character")
                    {
                        reader.MoveToAttribute("key");
                        key = reader.ReadContentAsInt();
                        reader.MoveToContent();
                        reader.ReadToDescendant("x");
                        x = Convert.ToInt32(reader.ReadInnerXml());
                        reader.ReadToNextSibling("y");
                        y = Convert.ToInt32(reader.ReadInnerXml());
                        reader.ReadToNextSibling("width");
                        w = Convert.ToInt32(reader.ReadInnerXml());
                        reader.ReadToNextSibling("height");
                        h = Convert.ToInt32(reader.ReadInnerXml());

                       // Debug.WriteLine("key:{0} x:{1} y:{2} w:{3} h:{4}", key, x, y, w, h);
                        RectManager.Add(Rect.Name.SpaceInvaders24pt, key, x, y, w, h);
                        Add(glyphName, key, Rect.Name.SpaceInvaders24pt, key, textureName);
                    }
                    
                }
            }

        }

        public static void Print()
        {
            GlyphManager manager = GlyphManager.Instance();
            Debug.WriteLine("----Glyph Manager----");
            manager.printTotals();
        }
    }
}