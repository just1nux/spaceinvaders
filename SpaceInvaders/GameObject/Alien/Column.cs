﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Column : GameObjectNull
    {

        public Column(GameObject.Name name, int index)
            : base()
        {
            setName(GameObject.Name.Column);
            setIndex(index);
            getCollisionObject().collisionSprite.setLineColor(Color.Name.Green);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitColumn(this);
        }

        public override void VisitMissileRoot(MissileRoot m)
        {
            CollisionPair.Collide((GameObject) m.getChild(), (GameObject)this.getChild());
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair.Collide(m, (GameObject)this.getChild());
        }

    }
}
