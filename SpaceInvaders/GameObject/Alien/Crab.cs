﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Crab : GameObject
    {
        public Crab(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.Crab, Image.Name.Crab2, SpriteLayer.Name.CrabLayer)
        {
        }

        protected override void loadImages()
        {
            ImageManager.Add(Image.Name.Crab1, Texture.Name.Aliens, 36.0f, 300.0f, 168.0f, 120.0f);
            ImageManager.Add(Image.Name.Crab2, Texture.Name.Aliens, 276.0f, 300.0f, 168.0f, 120.0f);
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
           otherGameObject.VisitCrab(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(m, this);
            pair.notifyListeners();
        }


        public override void VisitMissileRoot(MissileRoot m)
        {
            CollisionPair.Collide((GameObject)m.getChild(), this);
        }
    }
}
