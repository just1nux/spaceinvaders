﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Grid : GameObjectNull
    {
        private float deltaX;
        private float deltaY;

        public Grid()
            : base()
        {
            GameObjectFactory.SetTree(TreeNode.Name.AlienTree);
            setName(GameObject.Name.Grid);
            setDeltaX(6.0f);
            setDeltaY(0.0f);
            GameObjectManager.Add(this);
        }

        public void alienBeamIn()
        {
            float timing = 1.0f;
            float interval = timing/55.0f;

            for (int i = 10; i >= 0; i--)
            {
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Squid, i, true), timing);
                timing = timing - interval;
            }

            for (int i = 10; i >= 0; i--)
            {
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Crab, i, true), timing);
                timing = timing - interval;
            }

            for (int i = 21; i >= 11; i--)
            {
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Crab, i, true), timing);
                timing = timing - interval;
            }

            for (int i = 10; i >= 0; i--)
            {
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Octopus, i, true), timing);
                timing = timing - interval;
            }

            for (int i = 21; i >= 11; i--)
            {
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Octopus, i, true), timing);
                timing = timing - interval;
            }
        }


        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Move(float xDelta, float yDelta)
        {
            PCSTreeIterator iterator = new PCSTreeIterator(this);

            GameObject node = (GameObject)iterator.getFirst();

            while (iterator.hasNext())
            {
                node.setX(node.getX() + this.deltaX);
                node.setY(node.getY() + this.deltaY);
                node.update();
                node = (GameObject)iterator.getNext();
            }
            this.deltaY = 0.0f;
        }

        public int countAliens(){
            int aliens = 0;

            PCSTreeIterator iterator = new PCSTreeIterator(this);

            GameObject node = (GameObject)iterator.getFirst();

            while (iterator.hasNext())
            {
                aliens++;
                node = (GameObject)iterator.getNext();
            }
            aliens = aliens - 12; // dont count columns or grid;
            return aliens;
        }

        public void setDeltaX(float delta)
        {
            this.deltaX = delta;
        }

        public float getDeltaX()
        {
            return this.deltaX;
        }

        public void setDeltaY(float delta)
        {
            this.deltaY = delta;
        }

        public float getDeltaY()
        {
            return this.deltaY;
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitGrid(this);
        }

        public override void VisitMissileRoot(MissileRoot m)
        {
            CollisionPair.Collide((GameObject)m.getChild(), (GameObject)this.getChild());
        }

        public override void VisitWallRoot(WallRoot w)
        {
            CollisionPair.Collide(this, (GameObject)w.getChild());
        }

        public override void VisitShieldRoot(ShieldRoot s)
        {
           
        }

    }
}
