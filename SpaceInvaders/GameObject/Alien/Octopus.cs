﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Octopus : GameObject
    {
        public Octopus(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.Octopus, Image.Name.Octopus2, SpriteLayer.Name.OctopusLayer)
        {
        }

        protected override void loadImages()
        {
            ImageManager.Add(Image.Name.Octopus1, Texture.Name.Aliens, 32.0f, 60.0f, 184.0f, 120.0f);
            ImageManager.Add(Image.Name.Octopus2, Texture.Name.Aliens, 272.0f, 60.0f, 184.0f, 120.0f);
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitOctopus(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(m, this);
            pair.notifyListeners();
        }

        public override void VisitMissileRoot(MissileRoot m)
        {
            CollisionPair.Collide((GameObject)m.getChild(), this);
        }
    }
}