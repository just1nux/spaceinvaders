﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Squid : GameObject
    {
        public Squid(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.Squid, Image.Name.Squid2, SpriteLayer.Name.SquidLayer)
        {
            //Add Commands
        }

        protected override void loadImages()
        {
            ImageManager.Add(Image.Name.Squid1, Texture.Name.Aliens, 60.0f, 540.0f, 120.0f, 120.0f);
            ImageManager.Add(Image.Name.Squid2, Texture.Name.Aliens, 300.0f, 540.0f, 120.0f, 120.0f);
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitSquid(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(m, this);
            pair.notifyListeners();
        }

        public override void VisitMissileRoot(MissileRoot m)
        {
            CollisionPair.Collide((GameObject)m.getChild(), this);
        }

    }
}