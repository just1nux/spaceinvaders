﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Bomb:GameObject
    {
        public GameObjectMoveCommand moveCommand = null;
        private bool markForDeath;

        public Bomb(GameObject.Name name, Rect.Name rect, int index, int rectIndex, Sprite.Name sprite, Image.Name image, SpriteLayer.Name layer)
            : base(name, rect, index, rectIndex, sprite, image, layer)
        {
        }

        public void setMarkForDeath(bool markForDeath)
        {
            this.markForDeath = markForDeath;
        }

        public bool isMarkForDeath()
        {
            return markForDeath;
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitBomb(this);
        }

        protected override void loadImages(){
        }

        public override void VisitWall(Wall w)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, w);
            pair.notifyListeners();
        }

        public override void VisitShieldRoot(ShieldRoot s)
        {
            CollisionPair.Collide(this, (GameObject)s.getChild());
        }

        public override void VisitShip(Ship m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, m);
            pair.notifyListeners();
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, m);
            pair.notifyListeners();
        }

    }
}
