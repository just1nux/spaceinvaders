﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombDagger:Bomb
    {
        public BombDagger(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.BombDagger, Image.Name.BombDagger1, SpriteLayer.Name.BombLayer)
        {

        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.BombDagger1, Texture.Name.Aliens, 97.0f, 1508.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombDagger2, Texture.Name.Aliens, 337.0f, 1508.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombDagger3, Texture.Name.Aliens, 577.0f, 1508.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombDagger4, Texture.Name.Aliens, 817.0f, 1508.0f, 45.0f, 105.0f);
        }

    }
}
