﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombLightning:Bomb
    {
        public BombLightning(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.BombLightning, Image.Name.BombLightning1, SpriteLayer.Name.BombLayer)
        {

        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.BombLightning1, Texture.Name.Aliens, 97.0f, 1748.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombLightning2, Texture.Name.Aliens, 337.0f, 1748.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombLightning3, Texture.Name.Aliens, 577.0f, 1748.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombLightning4, Texture.Name.Aliens, 817.0f, 1748.0f, 45.0f, 105.0f);
        }

    }
}
