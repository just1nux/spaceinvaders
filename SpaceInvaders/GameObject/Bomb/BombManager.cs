﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombManager
    {
        private static volatile BombManager instance;
        private Random random;
        private int bomb1n;
        private int bomb2n;
        private int bomb3n;
        private static int setNum;

        public BombManager()
        {
            random = new Random();

            bomb1n = -1;
            bomb2n = -1;
            bomb3n = -1;
            setNum = 0;
        }

        public static BombManager Instance()
        {
            if (instance == null)
            {
                instance = new BombManager();
            }
            return instance;
        }

        public static GameObject getBomb()
        {
            BombManager manager = BombManager.Instance();
            GameObject gameObject = null;
            RectManager.Add(Rect.Name.Bomb, setNum, -1000, -1000, 9.0f, 21.0f);

            PCSNode bombRoot = GameObjectManager.FindGameObject(GameObject.Name.BombRoot);
            GameObjectFactory.SetParent(bombRoot);

            if (manager.bomb1n != -1){
                gameObject = GameObjectFactory.NewGameObject(getBombType(manager.bomb1n), Rect.Name.Bomb, setNum, setNum);
                manager.bomb1n = -1;
            } else if (manager.bomb2n != -1) {
                gameObject = GameObjectFactory.NewGameObject(getBombType(manager.bomb2n), Rect.Name.Bomb, setNum, setNum);
                manager.bomb2n = -1;
            } else if (manager.bomb3n != -1) {
                gameObject = GameObjectFactory.NewGameObject(getBombType(manager.bomb3n), Rect.Name.Bomb, setNum, setNum);
                manager.bomb3n = -1;
            } else {
                MakeBombs();
                gameObject = GameObjectFactory.NewGameObject(getBombType(manager.bomb1n), Rect.Name.Bomb, setNum, setNum);
                manager.bomb1n = -1;

                setNum++;
                if (setNum >= 4)
                    setNum = 0;
            }
            
            return gameObject;
       }



        public static void MakeBombs()
        {
            BombManager manager = BombManager.Instance();

            int bombSetNum = manager.random.Next(0, 6);
                              
            switch(bombSetNum){
                case 0:
                    manager.bomb1n = 0;
                    manager.bomb2n = 1;
                    manager.bomb2n = 2;
                    break;
                case 1:
                    manager.bomb1n = 0;
                    manager.bomb2n = 2;
                    manager.bomb2n = 1;
                    break;
                case 2:
                    manager.bomb1n = 1;
                    manager.bomb2n = 0;
                    manager.bomb2n = 2;
                    break;
                case 3:
                    manager.bomb1n = 1;
                    manager.bomb2n = 2;
                    manager.bomb2n = 0;
                    break;
                case 4:
                    manager.bomb1n = 2;
                    manager.bomb2n = 0;
                    manager.bomb2n = 1;
                    break;
                case 5:
                    manager.bomb1n = 2;
                    manager.bomb2n = 1;
                    manager.bomb2n = 0;
                    break;
                default:
                    break;
            }
        }

        public static GameObject getAlien()
        {
           BombManager manager = BombManager.Instance();
           
           GameObject alien = null;
           GameObject column = null;

           Grid grid = (Grid) GameObjectFactory.NewGameObject(GameObject.Name.Grid);

            if (grid.getChild() != null){
            while(column == null){
                int randomColumn = manager.random.Next(0, 11);
                column = GameObjectManager.FindGameObject(GameObject.Name.Column, randomColumn);
            }

            alien = (GameObject)column.getChild();
            }
            return alien;
        }

        public static void MakeBombRoot()
        {
            BombManager manager = BombManager.Instance();
            GameObject bombRoot = GameObjectFactory.NewGameObject(GameObject.Name.BombRoot);
        }

        private static GameObject.Name getBombType(int num)
        {
            BombManager manager = BombManager.Instance();

            GameObject.Name name = 0;

            switch (num)
            {
                case 0:
                    name = GameObject.Name.BombLightning;
                    break;
                case 1:
                    name = GameObject.Name.BombTwister;
                    break;
                case 2:
                    name = GameObject.Name.BombDagger;
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }

            return name;
        }

    }
}
