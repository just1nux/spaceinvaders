﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombRoot : GameObjectNull
    {
        public BombRoot()
        {
            GameObjectFactory.SetTree(TreeNode.Name.BombTree);
            setName(GameObject.Name.BombRoot);
            GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitBombRoot(this);
        }

        public override void VisitShieldRoot(ShieldRoot s)
        {
            CollisionPair.Collide((GameObject)this.getChild(), (GameObject)s.getChild());
        }

    }
}

