﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombTwister:Bomb
    {
        public BombTwister(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.BombTwister, Image.Name.BombTwister1, SpriteLayer.Name.BombLayer)
        {
        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.BombTwister1, Texture.Name.Aliens, 97.0f, 2228.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombTwister2, Texture.Name.Aliens, 337.0f, 2228.0f, 45.0f, 105.0f);
            ImageManager.Add(Image.Name.BombTwister3, Texture.Name.Aliens, 577.0f, 2228.0f, 45.0f, 105.0f);
        }

    }
}
