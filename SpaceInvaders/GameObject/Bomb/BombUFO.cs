﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class BombUFO:Bomb
    {
        public BombUFO(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.BombUFO, Image.Name.BombUFO1, SpriteLayer.Name.BombLayer)
        {

        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.BombUFO1, Texture.Name.Aliens, 97.0f, 1964.0f, 46.0f, 151.0f);
        }

    }
}
