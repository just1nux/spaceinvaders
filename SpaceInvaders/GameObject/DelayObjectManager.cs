﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class DelayedObjectMan
    {
        private CollisionObserver head;
        private static DelayedObjectMan instance = null;

        static public void Attach(CollisionObserver observer)
        {
            
            // protection
            Debug.Assert(observer != null);

            DelayedObjectMan pDelayMan = DelayedObjectMan.privGetInstance();

            // add to front
            if (pDelayMan.head == null)
            {
                pDelayMan.head = observer;
                observer.setNext(null);
                observer.setPrev(null);
            }
            else
            {
                observer.setNext(pDelayMan.head);
                observer.setPrev(null);
                pDelayMan.head.setPrev(observer);
                pDelayMan.head = observer;
            }
        }
        private void privDetach(CollisionObserver node, ref CollisionObserver head)
        {
            // protection
            Debug.Assert(node != null);

            if (node.getPrev() != null)
            {	// middle or last node
                node.getPrev().setNext(node.getNext());
            }
            else
            {  // first
                head = (CollisionObserver)node.getNext();
            }

            if (node.getNext() != null)
            {	// middle node
                node.getNext().setPrev(node.getPrev());
            }
        }
        static public void Process()
        {
            DelayedObjectMan pDelayMan = DelayedObjectMan.privGetInstance();

            CollisionObserver pNode = pDelayMan.head;

            while (pNode != null)
            {
                // Fire off listener
                pNode.Execute();
                pNode = (CollisionObserver)pNode.getNext();
            }


            // remove
            pNode = pDelayMan.head;
            CollisionObserver pTmp = null;

            while (pNode != null)
            {
                pTmp = pNode;
                pNode = (CollisionObserver)pNode.getNext();

                // remove
                pDelayMan.privDetach(pTmp, ref pDelayMan.head);
            }
        }
        private DelayedObjectMan()
        {
            this.head = null;
        }
        private static DelayedObjectMan privGetInstance()
        {
            // Do the initialization
            if (instance == null)
            {
                instance = new DelayedObjectMan();
            }

            // Safety - this forces users to call create first
            Debug.Assert(instance != null);

            return instance;
        }

    }
}