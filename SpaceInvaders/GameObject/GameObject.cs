﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class GameObject : CollisionVisitor
    {
        public enum Name
        {
            Uninitialized,
            NullObject,
            Root,
            Grid,
            Column,
            Squid,
            Crab,
            Octopus,
            ShipRoot,
            Ship,
            MissileRoot,
            Missile,
            WallRoot,
            WallLeft,
            WallRight,
            WallTop,
            WallBottom,
            WallShipLeft,
            WallShipRight,
            ShieldRoot,
            Shield,
            ShieldColumn,
            ShieldCube,
            BombRoot,
            Bomb,
            BombLightning,
            BombDagger,
            BombTwister,
            UFO,
            BombUFO,
            UFORoot,
            WallShipRoot,
        }

        private SpriteProxy spriteBase;
        private float angle;
        private float x;
        private float y;
        private float sx;
        private float sy;
        private float w;
        private float h;
        private int index;
        public CollisionObject collisionObject;
        bool drawCollisionSprite;


        protected abstract void loadImages();

        public GameObject(GameObject.Name name = GameObject.Name.NullObject, Rect.Name rectName = Rect.Name.NullRect, int index = 0, int rectIndex=0, Sprite.Name spriteName = Sprite.Name.NullObject, Image.Name imageName = Image.Name.NullObject, SpriteLayer.Name spriteLayerName = SpriteLayer.Name.NullObject, bool drawCollisionSprite=false)
            : base()
        {
            setName(name);
            setIndex(index);
            
            setDrawCollisionSprite(drawCollisionSprite);
            setDrawCollisionSprite(true);
           
            Rect rect = RectManager.Add(rectName, rectIndex);
            set(rect);
            loadImages();
            Sprite sprite = SpriteManager.Add(spriteName, imageName, x, y, w, h);
            SpriteProxy spriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, sprite, x, y);
            this.setSprite(spriteProxy);
            SpriteLayerManager.AddSprite(spriteLayerName, spriteProxy);

            CollisionObject collisionObject = new CollisionObject(spriteBase, isDrawCollisionSprite());
            Debug.Assert(collisionObject != null);
            setCollisionObject(collisionObject);
        }

        public void setCollisionObject(CollisionObject collisionObject)
        {
            this.collisionObject = collisionObject;
        }

        public CollisionObject getCollisionObject()
        {
            return this.collisionObject;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setDrawCollisionSprite(bool drawCollisionSprite){
            this.drawCollisionSprite = drawCollisionSprite;
        }

        public bool isDrawCollisionSprite()
        {
            return drawCollisionSprite;
        }

        public void setSprite(SpriteProxy sprite)
        {
            this.spriteBase = sprite;
        }

        public SpriteProxy getSprite()
        {
            return this.spriteBase;
        }

        public void set(Rect rect)
        {
            set(rect.getX(), rect.getY(), rect.getW(), rect.getH(), rect.getA());
        }

        public void set(float x, float y, float w, float h, float a)
        {
            setX(x);
            setY(y);
            setW(w);
            setH(h);
            setAngle(a);
        }

        public void setX(float x)
        {
            this.x = x;
        }

        public float getX()
        {
            return this.x;
        }

        public void setY(float y)
        {
            this.y = y;
        }

        public float getY()
        {
            return this.y;
        }

        public void setSx(float sx)
        {
            this.sx = sx;
        }

        public float getSx()
        {
            return this.sx;
        }

        public void setSy(float sy)
        {
            this.sy = sy;
        }

        public float getSy()
        {
            return this.sy;
        }

        public void setH(float h)
        {
            this.h = h;
        }

        public float getH()
        {
            return this.h;
        }

        public void setW(float w)
        {
            this.w = w;
        }

        public float getW()
        {
            return this.w;
        }

        public void setAngle(float angle)
        {
            this.angle = angle;
        }

        public float getAngle()
        {
            return this.angle;
        }

        public virtual void Move(float xDelta, float yDelta)
        {
            this.setX(this.getX() + xDelta);
            this.setY(this.getY() + yDelta);
            this.update();
        }

        public virtual void MovePosition(float x, float y)
        {
            this.setX(x);
            this.setY(y);
            this.update();          
        }



        public virtual void update()
        {

            if (spriteBase != null)
            {
                spriteBase.setX(x);
                spriteBase.setY(y);
                spriteBase.update();
            }

            if (collisionObject != null)
            {
                collisionObject.update(x, y, w, h);
            }

        }


        protected void baseUpdateBoundingBox()
        {
            // Go to first child
            PCSNode pNode = (PCSNode)this;
            pNode = pNode.getChild();

            // Set ColTotal to first child
            if (pNode != null)
            {
                GameObject pGameObj = (GameObject)pNode;

                CollisionRect ColTotal = this.getCollisionObject().collisionRect;
                ColTotal.Set(pGameObj.getCollisionObject().collisionRect);

                // loop through sliblings
                while (pNode != null)
                {
                    pGameObj = (GameObject)pNode;
                    ColTotal.union(pGameObj.getCollisionObject().collisionRect);

                    // go to next sibling
                    pNode = pNode.getSibling();
                }

                this.setX(this.collisionObject.collisionRect.getX());
                this.setY(this.collisionObject.collisionRect.getY());
                this.setW(this.collisionObject.collisionRect.getW());
                this.setH(this.collisionObject.collisionRect.getH());
            }
        }

        public void print()
        {
            Debug.WriteLine("NAME:{0} X:{1} Y:{2} INDEX:{3}", getName(), getX(), getY(), getIndex());
        }
    }
}
