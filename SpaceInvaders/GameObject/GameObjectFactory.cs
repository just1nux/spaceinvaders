﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class GameObjectFactory
    {
        private PCSTree tree;
        private PCSNode parent;
        private static volatile GameObjectFactory instance;

        public static GameObjectFactory Instance()
        {
            if (instance == null)
            {
                instance = new GameObjectFactory();
            }
            return instance;
        }

        public GameObjectFactory()
        {

        }

        public static GameObject NewGameObject(GameObject.Name name, int index = 0, int rectIndex = 0, bool treeAdd = true)
        {
            GameObject gameObject = NewGameObject(name, Rect.Name.NullRect, index, rectIndex, treeAdd);
            return gameObject;
        }


        public static GameObject NewGameObject(GameObject.Name name, Rect.Name rectName, int index = 0, int rectIndex = 0, bool treeAdd = true)
        {
            GameObject gameObject = null;
            GameObjectFactory gameObjectFactory = GameObjectFactory.Instance();

            gameObject = GameObjectManager.FindGameObject(name, index);

            if (gameObject == null)
            {
                gameObject = GameObjectManager.RestoreGameObject(name, index);
            }

            if (gameObject == null)
            {
                switch (name)
                {
                    //Groups of Elements
                    case GameObject.Name.Grid:
                        gameObject = new Grid();
                        break;
                    case GameObject.Name.Column:
                        gameObject = new Column(name, index);
                        break;
                    case GameObject.Name.ShipRoot:
                        gameObject = new ShipRoot();
                        break;
                    case GameObject.Name.MissileRoot:
                        gameObject = new MissileRoot();
                        break;
                    case GameObject.Name.WallRoot:
                        gameObject = new WallRoot();
                        break;
                    case GameObject.Name.WallShipRoot:
                        gameObject = new WallShipRoot();
                        break;
                    case GameObject.Name.ShieldRoot:
                        gameObject = new ShieldRoot();
                        break;
                    case GameObject.Name.Shield:
                        gameObject = new Shield(name, index);
                        break;
                    case GameObject.Name.ShieldColumn:
                        gameObject = new ShieldColumn(name, index);
                        break;
                    case GameObject.Name.BombRoot:
                        gameObject = new BombRoot();
                        break;
                    case GameObject.Name.UFORoot:
                        gameObject = new UFORoot();
                        break;


                    //Game Elements
                    case GameObject.Name.Squid:
                        gameObject = new Squid(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.Crab:
                        gameObject = new Crab(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.Octopus:
                        gameObject = new Octopus(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.Ship:
                        gameObject = new Ship(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.Missile:
                        gameObject = new Missile(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.WallLeft:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallLeft);
                        break;
                    case GameObject.Name.WallRight:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallRight);
                        break;
                    case GameObject.Name.WallShipLeft:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallShipLeft);
                        break;
                    case GameObject.Name.WallShipRight:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallShipRight);
                        break;
                    case GameObject.Name.WallTop:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallTop);
                        break;
                    case GameObject.Name.WallBottom:
                        gameObject = new Wall(name, rectName, index, rectIndex, Sprite.Name.WallBottom);
                        break;
                    case GameObject.Name.ShieldCube:
                        gameObject = new ShieldCube(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.BombLightning:
                        gameObject = new BombLightning(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.BombDagger:
                        gameObject = new BombDagger(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.BombTwister:
                        gameObject = new BombTwister(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.BombUFO:
                        gameObject = new BombUFO(name, rectName, index, rectIndex);
                        break;
                    case GameObject.Name.UFO:
                        gameObject = new UFO(name, rectName, index, rectIndex, new UFOLeftStrategy());
                        break;
                    default:
                        Debug.Assert(false);
                        break;
                }

                if (treeAdd)
                    GetTree().Insert(gameObject, GetParent());
            }

            return gameObject;
        }

        public static void SetTree(PCSTree tree)
        {
            GameObjectFactory gameObjectFactory = GameObjectFactory.Instance();
            SetParent(tree.getRoot());
            gameObjectFactory.tree = tree;
        }

        public static void SetTree(TreeNode.Name name)
        {
            TreeNode node = TreeManager.Add(name);
            SetTree(node.getTree());
        }

        public static PCSTree GetTree()
        {
            GameObjectFactory gameObjectFactory = GameObjectFactory.Instance();
            return gameObjectFactory.tree;
        }

        public static void SetParent(PCSNode parent)
        {
            GameObjectFactory gameObjectFactory = GameObjectFactory.Instance();
            gameObjectFactory.parent = parent;
        }

        public static PCSNode GetParent()
        {
            GameObjectFactory gameObjectFactory = GameObjectFactory.Instance();
            return gameObjectFactory.parent;
        }

        public static void CreateAlienGrid()
        {
            Grid grid = (Grid)GameObjectFactory.NewGameObject(GameObject.Name.Grid);

            int aliensPerRow = 11;
            float gridX = 95.0f;
            float gridY = 537.0f - (GameManager.GetLevel()*42.0f);
            float columnX = 0.0f;
            float columnY = 0.0f;
            float rowSpacing = 42.0f;
            float columnSpacing = 48.0f;

            //Create Grid
            GameObjectFactory.SetTree(TreeManager.Add(TreeNode.Name.AlienTree).getTree());
            GameObjectFactory.SetParent(grid);

            //Create Aliens
            for (int i = 0; i < aliensPerRow; i++)
            {
                columnX = gridX + i * columnSpacing;
                columnY = gridY;

                GameObject column = GameObjectFactory.NewGameObject(GameObject.Name.Column, i);

                GameObjectFactory.SetParent(column);

                RectManager.Add(Rect.Name.Squid, i, columnX, columnY, 25.0f, 22.0f);
                GameObject squid = GameObjectFactory.NewGameObject(GameObject.Name.Squid, Rect.Name.Squid, i, i);
                
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Squid, i, false), 0.0f);
                squid.setX(columnX);
                squid.setY(columnY);
                columnY -= rowSpacing;

                RectManager.Add(Rect.Name.Crab, i, columnX, columnY, 32.25f, 22.0f);
                GameObject crab = GameObjectFactory.NewGameObject(GameObject.Name.Crab, Rect.Name.Crab, i, i);

                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Crab, i, false), 0.0f);                
                crab.setX(columnX);
                crab.setY(columnY);
                columnY -= rowSpacing;

                
                RectManager.Add(Rect.Name.Crab, i + aliensPerRow, columnX, columnY, 32.25f, 22.0f);
                crab = GameObjectFactory.NewGameObject(GameObject.Name.Crab, Rect.Name.Crab, i + aliensPerRow, i + aliensPerRow);
                
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Crab, i + aliensPerRow, false), 0.0f);
                
                crab.setX(columnX);
                crab.setY(columnY);
                columnY -= rowSpacing;

                RectManager.Add(Rect.Name.Octopus, i, columnX, columnY, 40.0f, 22.0f);
                GameObject octopus = GameObjectFactory.NewGameObject(GameObject.Name.Octopus, Rect.Name.Octopus, i, i);                
                
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Octopus, i, false), 0.0f);
                octopus.setX(columnX);
                octopus.setY(columnY);
                columnY -= rowSpacing;

                RectManager.Add(Rect.Name.Octopus, i + aliensPerRow, columnX, columnY, 40.0f, 22.0f);
                octopus = GameObjectFactory.NewGameObject(GameObject.Name.Octopus, Rect.Name.Octopus, i + aliensPerRow, i + aliensPerRow);
                
                TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Octopus, i + aliensPerRow, false), 0.0f);

                octopus.setX(columnX);
                octopus.setY(columnY);
                columnY -= rowSpacing;
                
                GameObjectFactory.SetParent(grid);
            }        
        }

        public static void ActivateAlienGrid()
        {
            Grid grid = (Grid)GameObjectFactory.NewGameObject(GameObject.Name.Grid);
            
            grid.alienBeamIn();
            
            AlienMoveCommand moveHor = new AlienMoveCommand(grid);
            DelayedCommand dCommand = new DelayedCommand(moveHor, 0, 0.5f);
            TimerManager.Add(dCommand, 1.0f);
            
            ImageManager.Add(Image.Name.Squid1, Texture.Name.Aliens, 60.0f, 540.0f, 120.0f, 120.0f);
            ImageManager.Add(Image.Name.Squid2, Texture.Name.Aliens, 300.0f, 540.0f, 120.0f, 120.0f);
            ImageChangeCommand sCommand = new ImageChangeCommand(Sprite.Name.Squid);
            TimerManager.Add(sCommand, 0.50f);
            sCommand.attachImage(Image.Name.Squid1);
            sCommand.attachImage(Image.Name.Squid2);

            ImageManager.Add(Image.Name.Crab1, Texture.Name.Aliens, 36.0f, 300.0f, 168.0f, 120.0f);
            ImageManager.Add(Image.Name.Crab2, Texture.Name.Aliens, 276.0f, 300.0f, 168.0f, 120.0f);
            ImageChangeCommand cCommand = new ImageChangeCommand(Sprite.Name.Crab);
            TimerManager.Add(cCommand, 0.5f);
            cCommand.attachImage(Image.Name.Crab1);
            cCommand.attachImage(Image.Name.Crab2);

            ImageManager.Add(Image.Name.Octopus1, Texture.Name.Aliens, 32.0f, 60.0f, 184.0f, 120.0f);
            ImageManager.Add(Image.Name.Octopus2, Texture.Name.Aliens, 272.0f, 60.0f, 184.0f, 120.0f);
            ImageChangeCommand oCommand = new ImageChangeCommand(Sprite.Name.Octopus);
            TimerManager.Add(oCommand, 0.50f);
            oCommand.attachImage(Image.Name.Octopus1);
            oCommand.attachImage(Image.Name.Octopus2);
            
            SpriteLayer squidLayer = SpriteLayerManager.Add(SpriteLayer.Name.SquidLayer);
            SpriteLayer crabLayer = SpriteLayerManager.Add(SpriteLayer.Name.CrabLayer);
            SpriteLayer octopusLayer = SpriteLayerManager.Add(SpriteLayer.Name.OctopusLayer);
            squidLayer.setVisible(true);
            crabLayer.setVisible(true);
            octopusLayer.setVisible(true);
             
        }

        public static void DeactivateAlienGrid()
        {
            //Hide grid layers
            SpriteLayer squidLayer = SpriteLayerManager.Add(SpriteLayer.Name.SquidLayer);
            SpriteLayer crabLayer = SpriteLayerManager.Add(SpriteLayer.Name.CrabLayer);
            SpriteLayer octopusLayer = SpriteLayerManager.Add(SpriteLayer.Name.OctopusLayer);

            squidLayer.setVisible(false);
            crabLayer.setVisible(false);
            octopusLayer.setVisible(false);
        }

        public static void ActivateBombs()
        {
            SpriteLayer bombLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.BombLayer);
            bombLayer.setVisible(true);

            //Add Commands
            BombImageChangeCommand command = new BombImageChangeCommand(Sprite.Name.BombDagger);
            TimerManager.Add(command, 0.15f);
            command.attachImage(Image.Name.BombDagger2);
            command.attachImage(Image.Name.BombDagger3);
            command.attachImage(Image.Name.BombDagger4);
            command.attachImage(Image.Name.BombDagger1);

            command = new BombImageChangeCommand(Sprite.Name.BombLightning);
            command.attachImage(Image.Name.BombLightning2);
            command.attachImage(Image.Name.BombLightning3);
            command.attachImage(Image.Name.BombLightning4);
            command.attachImage(Image.Name.BombLightning1);
            TimerManager.Add(command, 0.15f);

            command = new BombImageChangeCommand(Sprite.Name.BombTwister);
            TimerManager.Add(command, 0.15f);
            command.attachImage(Image.Name.BombTwister2);
            command.attachImage(Image.Name.BombTwister3);
            command.attachImage(Image.Name.BombTwister1);





            TimerManager.Add(new DelayedBombDropCommand(null), 0f);
        }

        public static void DeactivateBombs()
        {
            SpriteLayer bombLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.BombLayer);
            bombLayer.setVisible(false);

            GameObject bombRoot = (GameObject)GameObjectFactory.NewGameObject(GameObject.Name.BombRoot);

            Bomb bomb = (Bomb)bombRoot.getChild();

            while (bomb != null)
            {
                ObserverRemoveBomb pObserver = new ObserverRemoveBomb(bomb);
                DelayedObjectMan.Attach(pObserver);
                bomb = (Bomb)bomb.getSibling();
            }
        }

        public static void CreateGameOverlay()
        {
              Overlay.Overlay.CreateOverlayBar();
              Overlay.Overlay.CreateOverlayScores();
              Overlay.Overlay.CreateOverlayCredits();
              Overlay.Overlay.CreateOverlayLives();
        }

        public static void ActivateGameOverlay()
        {
            SpriteLayer overlayScoreLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayScores);
            overlayScoreLayer.setVisible(true);
            SpriteLayer overlayLivesLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayLives);
            overlayLivesLayer.setVisible(true);
            SpriteLayer overlayCreditsLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayCredits);
            overlayCreditsLayer.setVisible(true);
            SpriteLayer overlayBarLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayBar);
            overlayBarLayer.setVisible(true);
        }

        public static void DeactivateGameOverlay()
        {
            SpriteLayer overlayScoreLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayScores);
            overlayScoreLayer.setVisible(false);
            SpriteLayer overlayLivesLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayLives);
            overlayLivesLayer.setVisible(false);
            SpriteLayer overlayCreditsLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayCredits);
            overlayCreditsLayer.setVisible(false);
            SpriteLayer overlayBarLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayBar);
            overlayBarLayer.setVisible(false);
        }      


        public static void ActivateShip()
        {
            //Make New Main Ship
            GameObjectFactory.SetTree(TreeNode.Name.ShipTree);
            ShipRoot shipRoot = (ShipRoot)GameObjectFactory.NewGameObject(GameObject.Name.ShipRoot);
            GameObjectFactory.SetParent(shipRoot);

            RectManager.Add(Rect.Name.Ship, 0, 75.0f, 138.0f, 40.0f, 20.0f);
            Ship ship = (Ship)GameObjectFactory.NewGameObject(GameObject.Name.Ship, Rect.Name.Ship, 0, 0);
            Sprite shipSprite = (Sprite)ship.getSprite().getSprite();
            shipSprite.setColor(Color.Name.Green);

            TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Ship, 0, false), 0.0f);
      
            ship.setX(75.0f);
            ship.setY(138.0f);

            //Disallow Ship Movement and hide ship
            ship.getSprite().setVisible(false);
            ship.setState(new ShipDisabledState());
            
            //Show Ship 0 in 2 seconds
            TimerManager.Add(new GameObjectVisibilityCommand(GameObject.Name.Ship, 0, true), 2.0f);

            //Allow Ship Movement in 2 seconds
            TimerManager.Add(new ShipStateCommand(GameObject.Name.Ship, new MissileReadyState()), 2.0f);

            //Show Ship Layer
            SpriteLayer spriteLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShipLayer);
            spriteLayer.setVisible(true);

        }

        public static void DeactivateShip()
        {
            SpriteLayer spriteLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShipLayer);
            spriteLayer.setVisible(false);
        }

        public static void CreateMissile()
        {
            MissileRoot missileRoot = (MissileRoot)GameObjectFactory.NewGameObject(GameObject.Name.MissileRoot);
        }

        public static void ActivateMissile()
        {
            SpriteLayer missileLayer = SpriteLayerManager.Add(SpriteLayer.Name.MissileLayer);
            missileLayer.setVisible(true);

        }

        public static void DeactivateMissile()
        {
            //hide the missile layer
            SpriteLayer missileLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.MissileLayer);
            missileLayer.setVisible(false);

            //If missile is active, remove it
            GameObject missile = GameObjectManager.FindGameObject(GameObject.Name.Missile);
            if(missile != null)
                GameObjectManager.RemoveGameObject(missile);
        }

        public static void CreateWalls()
        {
            WallRoot wallRoot = (WallRoot)GameObjectFactory.NewGameObject(GameObject.Name.WallRoot);
            wallRoot.loadWalls();
            WallShipRoot wallShipRoot = (WallShipRoot)GameObjectFactory.NewGameObject(GameObject.Name.WallShipRoot);
            wallShipRoot.loadWalls();
        }

        public static void CreateShields()
        {
            SpriteLayerManager.Add(SpriteLayer.Name.ShieldExplosions);
            drawShields();
            
            ShieldRoot shieldRoot = (ShieldRoot)GameObjectFactory.NewGameObject(GameObject.Name.ShieldRoot);
            shieldRoot.loadShields();
        }

        public static void ActivateShields()
        {
            SpriteLayer explayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShieldExplosions);
            explayer.setVisible(true);

            SpriteLayer shieldLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Shields);
            shieldLayer.setVisible(true);
        }

        public static void DeactivateShields()
        {
            SpriteLayer explayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShieldExplosions);
            explayer.setVisible(false);

            SpriteLayer shieldLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Shields);
            shieldLayer.setVisible(false);
        }

        public static void CreateInputs()
        {

            InputSubject inputSubject;
            inputSubject = InputManager.GetSubjectArrowRight();
            inputSubject.Attach(new MoveRightObserver());

            inputSubject = InputManager.GetSubjectArrowLeft();
            inputSubject.Attach(new MoveLeftObserver());

            inputSubject = InputManager.GetSubjectSpace();
            inputSubject.Attach(new ShootObserver());

            inputSubject = InputManager.GetSubjectVisCollision();
            inputSubject.Attach(new CollisionRectVisObserver());

            inputSubject = InputManager.GetSubjectOne();
            inputSubject.Attach(new OneKeyObserver());

            inputSubject = InputManager.GetSubjectTwo();
            inputSubject.Attach(new TwoKeyObserver());

        }

        public static void CreateTemplate()
        {
            TextureManager.Add(Texture.Name.Template, "templateav3.tga");
            ImageManager.Add(Image.Name.Template, Texture.Name.Template, 0.0f, 0.0f, 672.0f, 758.0f);
            Sprite template = SpriteManager.Add(Sprite.Name.Template, Image.Name.Template, 336, 379, 672, 768);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.TemplateLayer, template);
        }

        public static void ActivateTemplate()
        {
            SpriteLayer templateLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.TemplateLayer);
            templateLayer.setVisible(true);
        }

        public static void DeactivateTemplate()
        {
            SpriteLayer templateLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.TemplateLayer);
            templateLayer.setVisible(false);
        }

        public static void drawShields()
        {

            ImageManager.Add(Image.Name.Shield, Texture.Name.Aliens, 0.0f, 2654.0f, 330.0f, 210.0f);
            Sprite sprite = SpriteManager.Add(Sprite.Name.Shield, Image.Name.Shield, 0, 0, 67.0f, 43.0f);
            sprite.setColor(Color.Name.Green);

            for (int i = 0; i < 4; i++)
            {
                SpriteProxy proxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy);
                proxy.setSprite(sprite);
                proxy.setRect(130.0f + (i * 135.0f), 190.0f, 67.0f, 43.0f);
                SpriteLayerManager.AddSprite(SpriteLayer.Name.Shields, proxy);
            }
        }

        public static void ActivateCollisionRects()
        {
            SpriteLayer layer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CollisionLayer);
            layer.setVisible(true);
        }

        public static void DeactivateCollisionRects()
        {
            SpriteLayer layer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CollisionLayer);
            layer.setVisible(false);
        }

        public static void CreateUFO()
        {
            // Create UFO Root
            GameObjectFactory.SetTree(TreeNode.Name.UFOTree);
            UFORoot ufoRoot = (UFORoot)GameObjectFactory.NewGameObject(GameObject.Name.UFORoot);
            GameObjectFactory.SetParent(ufoRoot);

            SpriteLayer spriteLayer = SpriteLayerManager.Add(SpriteLayer.Name.UFOLayer);
            spriteLayer.setVisible(false);
        }

        public static void ActivateUFO()
        {
            SpriteLayer spriteLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.UFOLayer);
            spriteLayer.setVisible(true);

            //Set tree and root for UFO
            GameObjectFactory.SetTree(TreeNode.Name.UFOTree);
            GameObject UFORoot = GameObjectManager.FindGameObject(GameObject.Name.UFORoot);
            GameObjectFactory.SetParent(UFORoot);



            RectManager.Add(Rect.Name.UFO, 0, 0, 595, 44.0f, 20.0f);
            UFO ufo = (UFO)GameObjectFactory.NewGameObject(GameObject.Name.UFO, Rect.Name.UFO, 0, 0);
            ufo.getSprite().setVisible(false);
     
            Random random = new Random();
            float time = random.Next(30, 60) / 5.0f;
            UFOStartCommand command = new UFOStartCommand();
            TimerManager.Add(command, time);            
        }

        public static void DeactivateUFO()
        {
            SpriteLayer spriteLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.UFOLayer);
            spriteLayer.setVisible(false);
           
            GameObject ufo = GameObjectManager.FindGameObject(GameObject.Name.UFO);
            if (ufo != null)
            {
                GameObjectManager.RemoveGameObject(ufo);
            }
        }

        public static void CreateExplosions()
        {

        }

        public static void ActivateExplosions()
        {
            SpriteLayer explosionLayer = SpriteLayerManager.Add(SpriteLayer.Name.Explosions);
            explosionLayer.setVisible(true);

        }

        public static void DeactivateExplosions()
        {
            SpriteLayer explosionLayer = SpriteLayerManager.Add(SpriteLayer.Name.Explosions);
            explosionLayer.setVisible(false);
            explosionLayer.removeAll();
        }

       public static void DeactivateShieldExplosions()
        {
            SpriteLayer shieldExplosionsLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShieldExplosions);
            shieldExplosionsLayer.removeAll();
        }
    }
}
