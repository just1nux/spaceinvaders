﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class GameObjectManager : ContainerManager
    {
       // private static volatile GameObjectManager instance;
        private PCSTree root;

        private GameObjectManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
            // used in remove
            this.root = new PCSTree();
            Debug.Assert(this.root != null);
        }

        public static GameObjectManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            GameObjectManager instance = GameManager.GetCurrentPlayer().gameObjectManagerInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().gameObjectManagerInstance = new GameObjectManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().gameObjectManagerInstance;
            }
            
            return instance;
        }

        public static GameObjectNode Add(GameObject gameObject)
        {
            Debug.Assert(gameObject != null);

            GameObjectManager manager = GameObjectManager.Instance();
            GameObjectNode node = (GameObjectNode)manager.addLink();

            Debug.Assert(node != null);

            node.setGameObject(gameObject);
            return node;
        }

        protected override Container create()
        {
            Container link = new GameObjectNode();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Boolean status = false;

            return status;
        }

        public static void Insert(GameObject gameObject, GameObject parent)
        {
            GameObjectManager manager = GameObjectManager.Instance();
            Debug.Assert(gameObject != null);

            if (parent == null)
            {
                GameObjectManager.AttachTree(gameObject, null);
            }
            else
            {
                Debug.Assert(parent != null);

                manager.root.setRoot(parent);
                manager.root.Insert(gameObject, parent);
            }
        }

        public static GameObjectNode AttachTree(GameObject gameObject, PCSTree tree)
        {
            GameObjectManager manager = GameObjectManager.Instance();

            Debug.Assert(gameObject != null);
            Debug.Assert(tree != null);

            GameObjectNode node = (GameObjectNode)manager.addLink();
            Debug.Assert(node != null);

            Debug.Assert(tree != null);
            gameObject.setTree(tree);

            Debug.Assert(gameObject != null);
            node.setGameObject(gameObject);

            return node;
        }


        public static void Print()
        {
            GameObjectManager manager = GameObjectManager.Instance();
            Debug.WriteLine("----GameObject Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            GameObjectManager manager = GameObjectManager.Instance();
            manager.baseDestroy();
        }

        public static void RemoveGameObject(GameObject gameObject)
        {
            
            //Sprite Removal
            SpriteBase sprite = gameObject.getSprite();
            Debug.Assert(sprite != null);

            SpriteLayer spriteLayer = sprite.getSpriteLayer();
            Debug.Assert(spriteLayer != null);
            spriteLayer.removeSprite(sprite);

            //Collision Sprite Removal
            SpriteBox collSprite = gameObject.getCollisionObject().collisionSprite;
            Debug.Assert(collSprite != null);
            
            SpriteLayer collLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CollisionLayer);
            collLayer.removeSprite(collSprite);

            GhostNode node = GhostManager.Add(gameObject);

            PCSTree tree = gameObject.getTree();
            if (tree != null)
            {
                tree.Remove(gameObject);
            }
        }


        public static GameObject RestoreGameObject(GameObject.Name gameObjectName, int index)
        {
            GameObject gameObject = null;

            gameObject = GameObjectManager.FindGameObject(gameObjectName, index);

            if (gameObject != null){
                SpriteBase sprite = gameObject.getSprite();
                Debug.Assert(sprite != null);

                SpriteLayer spriteLayer = sprite.getSpriteLayer();
                Debug.Assert(spriteLayer != null);

                SpriteLayer.Name layerName = (SpriteLayer.Name)spriteLayer.getName();
                SpriteLayerManager.AddSprite(layerName, sprite);

                SpriteBox collSprite = gameObject.getCollisionObject().collisionSprite;
                Debug.Assert(collSprite != null);
                SpriteLayer collLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CollisionLayer);
                collLayer.add(collSprite);

            
            }

            return gameObject;
        }


        public static GameObject FindGameObject(GameObject.Name name, int index = 0)
        {
            GameObjectManager manager = GameObjectManager.Instance();
            GameObjectNode gameObjectNode = (GameObjectNode)manager.getpActive();
            GameObject gameObjectFound = null;

            // Loop through game object nodes until found
            while (gameObjectNode != null && gameObjectFound == null)
            {
                //Loop though GameObjectTree
                GameObject gameObjectRoot = gameObjectNode.getGameObject();

                PCSTreeIterator treeIterator = new PCSTreeIterator(gameObjectRoot);
                GameObject gameObject = (GameObject)treeIterator.getFirst();
                if (gameObject != null)
                {
                    //loop through gameObjects until found
                    while (treeIterator.hasNext() && gameObjectFound == null)
                    {
                        if ((GameObject.Name)gameObject.getName() == name && gameObject.getIndex() == index)
                            gameObjectFound = gameObject;

                        gameObject = (GameObject)treeIterator.getNext();
                    }
                }
                gameObjectNode = (GameObjectNode)gameObjectNode.getNext();
            }

            return gameObjectFound;
        }

        public static void Update()
        {
            GameObjectManager manager = GameObjectManager.Instance();
            GameObjectNode gameObjectNode = (GameObjectNode)manager.getpActive();

            // Loop through game object nodes until found
            while (gameObjectNode != null)
            {
                //Loop though GameObjectTree
                gameObjectNode.update();
                gameObjectNode = (GameObjectNode)gameObjectNode.getNext();
            }    
        }

        public static void MoveGameObject(GameObject.Name name, float x=0.0f, float y=0.0f, int index=0){
            GameObject gameObject = GameObjectManager.FindGameObject(name, index);
            MoveGameObject(gameObject, x, y);
        }

        public static void MoveGameObject(GameObject gameObject, float x = 0.0f, float y = 0.0f, int index = 0)
        {
             gameObject.MovePosition(x, y);
        }
    }
}
