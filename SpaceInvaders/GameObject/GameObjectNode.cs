﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class GameObjectNode : Container
    {
        private GameObject gameObject;

        public GameObjectNode()
            : base()
        {
            initialize();
        }
        ~GameObjectNode()
        {
            setGameObject(null);
        }

        public override void initialize()
        {
            setGameObject(null);
        }

        public void setGameObject(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public GameObject getGameObject()
        {
            return this.gameObject;
        }

        public Enum getName()
        {
            return getGameObject().getName();
        }

        public override void print()
        {
            Debug.WriteLine("\tGameObject:{0}", this.GetHashCode());

            if (getGameObject() != null)
            {
                getGameObject().print();
            }
        }

        public void update()
        {

            GameObject gameObjectRoot = this.getGameObject();

                PCSTreeIterator pIterator = new PCSTreeIterator(gameObjectRoot);
                GameObject gameObject = (GameObject)pIterator.getFirst();

                while (gameObject != null)
                {
                    gameObject.update();
                    gameObject = (GameObject)pIterator.getNext();
                }
        }
    }
}