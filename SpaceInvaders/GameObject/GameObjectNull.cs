﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class GameObjectNull : GameObject
    {
        public GameObjectNull()
            : base()
        {
            setName(GameObject.Name.NullObject);
        }

        ~GameObjectNull()
        {
        }

        protected override void loadImages()
        {
            Texture nullTexture = TextureManager.Add(Texture.Name.NullObject, "null.tga");
            Image nullImage = ImageManager.Add(Image.Name.NullObject, Texture.Name.NullObject, 0, 0, 0, 0);
        }

    }
}