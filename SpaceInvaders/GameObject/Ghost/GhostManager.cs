﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class GhostManager : ContainerManager
    {
        //private static volatile GhostManager instance;

        private GhostManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {

        }

        public static GhostManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            GhostManager instance = GameManager.GetCurrentPlayer().ghostManagerInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().ghostManagerInstance = new GhostManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().ghostManagerInstance;
            }
            return instance;
        }

        /*
        public static GhostManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new GhostManager(reserveNum, reserveGrow);
            }
            return instance;
        }
        */
        public static GhostNode Add(GameObject gameObject)
        {
            Debug.Assert(gameObject != null);

            GhostManager manager = GhostManager.Instance();
            GhostNode node = (GhostNode)manager.addLink();
            Debug.Assert(node != null);

            node.parent = gameObject.getParent();
            node.setGameObject(gameObject);
            node.tree = gameObject.getTree();

            return node;
        }


        protected override Container create()
        {
            Container link = new GhostNode();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            GhostNode nodeA = (GhostNode)linkA;
            GhostNode nodeB = (GhostNode)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }

        public static void Print()
        {
            GhostManager manager = GhostManager.Instance();
            Debug.WriteLine("----Ghost Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            GhostManager manager = GhostManager.Instance();
            manager.baseDestroy();
        }

        public static GameObject Find(Enum name, int index=0)
        {
            GhostManager manager = GhostManager.Instance();
            GhostNode foundNode = null;
            GameObject foundOb = null;

            // Compare functions only compares two Nodes

            GhostNode node = new GhostNode();
            node.setName(name);
            node.setIndex(index);

            foundNode = (GhostNode) manager.searchActiveForLink(node);
            if (foundNode != null){
                foundOb = foundNode.getGameObject();
            }

            return foundOb;
        }


        public static GhostNode FindGhostNode(Enum name, int index)
        {
            GhostManager manager = GhostManager.Instance();
            GhostNode foundNode = null;

            // Compare functions only compares two Nodes

            GhostNode node = new GhostNode();
            node.setName(name);
            node.setIndex(index);

            foundNode = (GhostNode)manager.searchActiveForLink(node);

            return foundNode;
        }


        public static void Remove(GameObject gameObject)
        {
            GhostManager manager = GhostManager.Instance();
            GhostNode ghostNode = FindGhostNode(gameObject.getName(), gameObject.getIndex());
            manager.removeFromActive(ghostNode);
            
        }
    
    }
}
