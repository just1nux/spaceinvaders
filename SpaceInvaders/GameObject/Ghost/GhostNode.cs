﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class GhostNode : Container
    {
        private GameObject gameObject;
        GameObject.Name name;
        public PCSNode parent;
        public PCSTree tree;
        int index;

        public GhostNode()
            : base()
        {
            initialize();
        }

        ~GhostNode()
        {
            setGameObject(null);
        }

        public override void initialize()
        {
            setGameObject(null);
        }

        public void setGameObject(GameObject gameObject)
        {
            this.gameObject = gameObject;
            if (gameObject != null){
                this.name = (GameObject.Name) gameObject.getName();
                this.index = gameObject.getIndex();
                this.parent = gameObject.getParent();
            }
        }

        public GameObject getGameObject()
        {
            return this.gameObject;
        }

        public Enum getName()
        {
            return this.name;
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setName(Enum name)
        {
            this.name = (GameObject.Name) name;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public override void print()
        {
            Debug.WriteLine("\tGhostNode:{0}", this.GetHashCode());

            if (getGameObject() != null)
            {
                getGameObject().print();
            }
        }
    }
}