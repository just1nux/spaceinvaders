﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Shield : GameObjectNull
    {
        public Shield(GameObject.Name name, int index)
            : base()
        {
            GameObjectFactory.SetParent(GameObjectManager.FindGameObject(GameObject.Name.ShieldRoot));
            setName(name);
            setIndex(index);
        }

        protected override void loadImages()
        {
        }


        public void loadShieldColumns(int cols, int rows, float startX = 112.0f, float startY = 178.0f)
        {
            float x = startX + (this.getIndex() * 135.0f);
            float y = 0.0f;
            float w = 67.0f / cols;
            float h = 43.0f / rows;

            ShieldColumn shieldColumn;
            ShieldCube shieldCube;

            GameObjectFactory.SetParent(this);
            
            for (int col = 0; col <= cols - 1; col++)
            {
                int colNum = this.getIndex() * cols + col;
                shieldColumn = (ShieldColumn) GameObjectFactory.NewGameObject(GameObject.Name.ShieldColumn, colNum);
                shieldColumn.setH(rows * h);
                int numJump = (this.getIndex() * cols * rows);
                GameObjectFactory.SetParent(shieldColumn);
                
                for (int row = 0; row <= rows -1; row++)
                {
                    int cubeNum = (this.getIndex() * cols * rows) + (col * rows) + row;
                    y = startY + (row * h);

                    if (cubeNum == numJump + 5 || cubeNum == numJump + 12 || cubeNum == numJump + 13 || cubeNum == numJump + 18 || cubeNum == numJump + 19 || cubeNum == numJump + 35 || cubeNum == numJump + 24)
                    {
    
                    } else {
                        Rect rect = RectManager.Add(Rect.Name.ShieldCube, cubeNum, x, y, w, h);
                        shieldCube = (ShieldCube)GameObjectFactory.NewGameObject(GameObject.Name.ShieldCube, Rect.Name.ShieldCube, cubeNum, cubeNum);      
                    }
                
                }
                GameObjectFactory.SetParent(this);
                x = x + w;
            }

            GameObjectFactory.SetParent(this.getParent());

        }


        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitShield(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair.Collide(m, (GameObject) this.getChild());
        }

        public override void VisitBomb(Bomb b)
        {
            CollisionPair.Collide(b, (GameObject)this.getChild());
        }

        public override void VisitColumn(Column a)
        {
            CollisionPair.Collide((GameObject)a.getChild(), (GameObject)this.getChild());
        }
    }
}
