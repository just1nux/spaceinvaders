﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ShieldColumn : GameObjectNull
    {

        public ShieldColumn(GameObject.Name name, int index)
            : base()
        {
            setName(name);
            setIndex(index);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }
     
        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitShieldColumn(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair.Collide(m, (GameObject)this.getChild());
        }

        public override void VisitBomb(Bomb b)
        {
            CollisionPair.Collide(b, (GameObject)this.getChild());
        }

        public override void VisitCrab(Crab a)
        {
            CollisionPair.Collide(a, (GameObject)this.getChild()); 
        }

        public override void VisitSquid(Squid a)
        {
            CollisionPair.Collide(a, (GameObject)this.getChild()); 
        }

        public override void VisitOctopus(Octopus a)
        {
            CollisionPair.Collide(a, (GameObject)this.getChild()); 
        }
    }
}

