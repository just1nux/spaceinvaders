﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ShieldCube : GameObject
    {
        private bool markForDeath;

          public ShieldCube(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.ShieldCube, Image.Name.NullObject, SpriteLayer.Name.NullObject, true)
        {
           setName(GameObject.Name.ShieldCube);

        }

        public void setMarkForDeath(bool markForDeath)
        {
           this.markForDeath = markForDeath;
        }

        public bool isMarkForDeath(){
            return markForDeath;
        }

        protected override void loadImages()
        {
 
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitShieldCube(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(m, this);
            pair.notifyListeners();
        }

        public override void VisitBomb(Bomb b)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(b, this);
            pair.notifyListeners();
        }

        public override void VisitCrab(Crab a)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(a, this);
            pair.notifyListeners();    
        }

        public override void VisitSquid(Squid a)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(a, this);
            pair.notifyListeners();
        }

        public override void VisitOctopus(Octopus a)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(a, this);
            pair.notifyListeners();
        }


    }
}

