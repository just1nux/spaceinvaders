﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ShieldRoot : GameObjectNull
    {
        public ShieldRoot()
        {
            GameObjectFactory.SetTree(TreeNode.Name.ShieldTree);
            setName(GameObject.Name.ShieldRoot);
            GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public void loadShields()
        {
            //Setup
            PCSTree tree = TreeManager.Add(TreeNode.Name.ShieldTree).getTree();
            GameObjectFactory.SetTree(tree);
            GameObjectFactory.SetParent(tree.getRoot());

            for (int i = 0; i <= 3; i++)
            {
                Shield shield = (Shield)GameObjectFactory.NewGameObject(GameObject.Name.Shield, i);
                shield.loadShieldColumns(6, 6, 100.0f, 170.0f);
            }
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitShieldRoot(this);
        }

        public override void VisitGrid(Grid a)
        {
            CollisionPair.Collide((GameObject)a.getChild(), (GameObject)this.getChild());
        }

        public override void VisitMissileRoot(MissileRoot r)
        {
            CollisionPair.Collide(this, (GameObject)r.getChild());
        }

        public override void VisitBombRoot(BombRoot b)
        {
            CollisionPair.Collide(this, (GameObject)b.getChild());
        }


    }
}

