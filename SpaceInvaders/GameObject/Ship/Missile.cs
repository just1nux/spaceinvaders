﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Missile: GameObject
    {
        private bool markForDeath;

        public Missile(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.Missile, Image.Name.Missile, SpriteLayer.Name.MissileLayer)
        {
        }

        public void setMarkForDeath(bool markForDeath)
        {
            this.markForDeath = markForDeath;
        }

        public bool isMarkForDeath()
        {
            return markForDeath;
        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.Missile, Texture.Name.Aliens, 112.0f, 1290.0f, 15.0f, 60.0f);
            ImageManager.Add(Image.Name.MissileExplosion, Texture.Name.Aliens, 240.0f, 1200.0f, 240.0f, 240.0f);
            ImageManager.Add(Image.Name.MissileWallHit, Texture.Name.Aliens, 480.0f, 1200.0f, 240.0f, 240.0f);
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
             otherGameObject.VisitMissile(this);
        }

        public override void VisitShieldRoot(ShieldRoot s)
        {
            CollisionPair.Collide(this,(GameObject) s.getChild());
        }

        public override void VisitWall(Wall w)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(w, this);
            pair.notifyListeners();
        }

    }
}
