﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class MissileRoot : GameObjectNull
    {

        public MissileRoot()
            : base()
        {
            GameObjectFactory.SetTree(TreeNode.Name.MissileTree);
            setName(GameObject.Name.MissileRoot);
            GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor other)
        {           
            other.VisitMissileRoot(this);
        }

        public void loadMissiles()
        {
            //Set Up Missile Tree
            GameObjectFactory.SetTree(TreeManager.Add(TreeNode.Name.MissileTree).getTree());
            GameObjectFactory.SetParent(this);

            //Add Missle
            RectManager.Add(Rect.Name.Missile, 0, 1000.0f, 150.0f, 3.0f, 10.0f);
            GameObject missile = GameObjectFactory.NewGameObject(GameObject.Name.Missile, Rect.Name.Missile, 0);
        }

        public override void VisitBombRoot(BombRoot b)
        {
            CollisionPair.Collide((GameObject)this.getChild(), (GameObject)b.getChild());
        }

    }
}
