﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Ship: GameObject
    {
        private ShipState state;
        private bool markForDeath;

        public Ship(GameObject.Name name, Rect.Name rect, int index, int rectIndex)
            : base(name, rect, index, rectIndex, Sprite.Name.Ship, Image.Name.Ship1, SpriteLayer.Name.ShipLayer, true)
        {
            this.state = new ShipDisabledState();
        }

        protected override void loadImages(){
            ImageManager.Add(Image.Name.Ship1, Texture.Name.Aliens, 8.0f, 1016.0f, 228.0f, 120.0f);
            ImageManager.Add(Image.Name.Ship2, Texture.Name.Aliens, 248.0f, 1016.0f, 228.0f, 120.0f);
            ImageManager.Add(Image.Name.Ship3, Texture.Name.Aliens, 488.0f, 1016.0f, 228.0f, 120.0f);
        }

        public ShipState getState()
        {
            return this.state;
        }

        public void setState(ShipState state)
        {
            this.state = state;
        }

        public void moveLeft()
        {
            state.moveLeft();
        }

        public void moveRight()
        {
            state.moveRight();
        }

        public void shoot()
        {
            state.shoot(this);
        }

        public void setMarkForDeath(bool markForDeath)
        {
            this.markForDeath = markForDeath;
        }

        public bool isMarkForDeath()
        {
            return markForDeath;
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitShip(this);
        }

        public override void VisitWallShipRoot(WallShipRoot w)
        {
            CollisionPair.Collide((GameObject)w.getChild(), this);
        }

        public override void VisitWall(Wall w)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, w);
            pair.notifyListeners();
        }

        public override void VisitBombRoot(BombRoot b)
        {
            CollisionPair.Collide(this, (GameObject) b.getChild());
        }

        public override void VisitBomb(Bomb b)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, b);
            pair.notifyListeners();
        }

    }
}
