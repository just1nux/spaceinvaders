﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ShipRoot : GameObjectNull
    {

        public ShipRoot()
            : base()
        {
           GameObjectFactory.SetTree(TreeNode.Name.ShipTree);
           setName(GameObject.Name.ShipRoot);
           GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
              otherGameObject.VisitShipRoot(this);
        }

        public override void VisitWallRoot(WallRoot w)
        {
            CollisionPair.Collide(w, (GameObject)this.getChild());
        }

        public override void VisitBombRoot(BombRoot b)
        {
            CollisionPair.Collide((GameObject)b.getChild(), (GameObject)this.getChild());
        }

        public override void VisitWallShipRoot(WallShipRoot w)
        {
            CollisionPair.Collide((GameObject)w.getChild(), (GameObject)this.getChild());            
        }
    }
}

