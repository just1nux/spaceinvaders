﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class GameState
    {
        public enum Name
        {
            DISABLED,
            ATTRACT,
            PLAYERCHOOSE,
            PLAYERCHOSEN,
            PLAYING,
            GAMEOVER,
        }

        public abstract void Start();
        public abstract void Stop();

        public abstract void SKey();
        public abstract void SpaceKey();
        public abstract void OneKey();
        public abstract void TwoKey();

        private Name name;
        
        public Name getStateName(){
            return this.name;
        }

        public void setStateName(Name name)
        {
            this.name = name;
        }

    }

    public class AttractState : GameState
    {
        public static AttractState Instance()
        {
            AttractState instance = GameManager.GetCurrentPlayer().attractStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().attractStateInstance = new AttractState();
                instance = GameManager.GetCurrentPlayer().attractStateInstance;
            }
            return instance;
        }

        private AttractState()
        {
            this.setStateName(GameState.Name.ATTRACT);
            AttractMode.CreateScoreKey();
        }

        public override void Start()
        {
            GameObjectFactory.DeactivateShields();
            GameObjectFactory.DeactivateAlienGrid();
            GameObjectFactory.DeactivateShip();
            GameObjectFactory.DeactivateMissile();
            GameObjectFactory.DeactivateBombs();
            GameObjectFactory.DeactivateUFO();
            GameObjectFactory.DeactivateExplosions();
            GameObjectFactory.DeactivateGameOverlay();
            SoundManager.Stop();

            AttractMode.ActivateAttractOverlay();
            AttractMode.ActivateScoreKey();
        }

        public override void Stop()
        {
           GameManager.GetCurrentPlayer().resetScore();
           GameManager.GetCurrentPlayer().resetLives();
           GameManager.SetLevel(0);
           AttractMode.DeactivateAttractOverlay();
           AttractMode.DeactivateScoreKey();
           TimerManager.Destroy();
        }

        public override void SpaceKey(){}
        public override void SKey(){}

        public override void OneKey()
        {
            TimerManager.Add(new StateChangeCommand(LevelStartState.Instance(1), GameManager.GetState()));
        }

        public override void TwoKey()
        {
            TimerManager.Add(new StateChangeCommand(LevelStartState.Instance(2), GameManager.GetState()));
        }
    }

    public class PlayerChooseState : GameState
    {
        public static PlayerChooseState Instance()
        {
            PlayerChooseState instance = GameManager.GetCurrentPlayer().playerChooseStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().playerChooseStateInstance = new PlayerChooseState();
                instance = GameManager.GetCurrentPlayer().playerChooseStateInstance;
            }
            return instance;
        }

        private PlayerChooseState()
        {
            this.setStateName(GameState.Name.PLAYERCHOOSE);
            AttractMode.CreatePlayerChoose();
        }

        public override void Start()
        {
            GameObjectFactory.DeactivateShields();
            GameObjectFactory.DeactivateAlienGrid();
            GameObjectFactory.DeactivateShip();
            GameObjectFactory.DeactivateMissile();
            GameObjectFactory.DeactivateBombs();
            GameObjectFactory.DeactivateUFO();
            GameObjectFactory.DeactivateExplosions();
            GameObjectFactory.DeactivateGameOverlay();
            SoundManager.Stop();

            AttractMode.ActivateAttractOverlay();
            AttractMode.ActivatePlayerChoose();
        }

        public override void Stop()
        {
            GameManager.GetCurrentPlayer().resetScore();
            GameManager.GetCurrentPlayer().resetLives();
            GameManager.SetLevel(0);
            AttractMode.DeactivateAttractOverlay();
            AttractMode.DeactivatePlayerChoose();
            TimerManager.Destroy();
        }

        public override void SKey(){}
        public override void SpaceKey(){}

        public override void OneKey()
        {
            TimerManager.Add(new StateChangeCommand(LevelStartState.Instance(1), GameManager.GetState()));
        }

        public override void TwoKey()
        {
            TimerManager.Add(new StateChangeCommand(LevelStartState.Instance(2), GameManager.GetState()));
        }
    }

    public class LevelStartState : GameState
    {
        public static LevelStartState Instance(int num)
        {
            LevelStartState instance = GameManager.GetCurrentPlayer().levelStartStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().levelStartStateInstance = new LevelStartState();
                instance = GameManager.GetCurrentPlayer().levelStartStateInstance;
            }
            GameManager.SetNumPlayers(num);
            return instance;
        }
        
        private LevelStartState()
        {
            
            this.setStateName(GameState.Name.PLAYERCHOSEN);
             AttractMode.CreatePlayerChosen();
        }

        public override void Start()
        {
            AttractMode.ActivateAttractOverlay();
            AttractMode.ActivatePlayerChosen();
        }

        public override void Stop()
        {
            AttractMode.DeactivateAttractOverlay();
            AttractMode.DeactivatePlayerChosen();
            GameObjectFactory.DeactivateShieldExplosions();
            GameObjectFactory.CreateAlienGrid();
        }

        public override void SKey() {}
        public override void SpaceKey() {}
        public override void OneKey() {}
        public override void TwoKey() {}
    }

    public class PlayerStartState : GameState
    {
        public static PlayerStartState Instance(int num)
        {
            PlayerStartState instance = GameManager.GetCurrentPlayer().playerStartStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().playerStartStateInstance = new PlayerStartState(num);
                instance = GameManager.GetCurrentPlayer().playerStartStateInstance;
            }
            return instance;
        }

        private PlayerStartState(int numPlayers)
        {
            GameManager.SetNumPlayers(numPlayers);
            this.setStateName(GameState.Name.PLAYERCHOSEN);
            AttractMode.CreatePlayerChosen();
        }

        public override void Start()
        {
            AttractMode.ActivateAttractOverlay();
            AttractMode.ActivatePlayerChosen();
            GameObjectFactory.DeactivateExplosions();
        }

        public override void Stop()
        {
            AttractMode.DeactivateAttractOverlay();
            AttractMode.DeactivatePlayerChosen();
        }

        public override void SKey() { }
        public override void SpaceKey() { }
        public override void OneKey() { }
        public override void TwoKey() { }
    }

    public class GamePlayingState : GameState
    {
        public static GamePlayingState Instance()
        {
            GamePlayingState instance = GameManager.GetCurrentPlayer().gamePlayingStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().gamePlayingStateInstance = new GamePlayingState();
                instance = GameManager.GetCurrentPlayer().gamePlayingStateInstance;
            }

            return instance;
        }


        private GamePlayingState()
        {
              this.setStateName(GameState.Name.PLAYING);
            
              GameObjectFactory.CreateAlienGrid();
              GameObjectFactory.CreateShields();
              GameObjectFactory.CreateWalls();
              GameObjectFactory.CreateMissile();
              
              //Bombs needed preloading, needs to be fixed or moved.
              RectManager.Add(Rect.Name.Bomb, 0, -1000, -1000, 9.0f, 21.0f);
              GameObject bt = GameObjectFactory.NewGameObject(GameObject.Name.BombTwister, Rect.Name.Bomb, 0, 0);
              GameObject bl = GameObjectFactory.NewGameObject(GameObject.Name.BombLightning, Rect.Name.Bomb, 0, 0);
              GameObject bd = GameObjectFactory.NewGameObject(GameObject.Name.BombDagger, Rect.Name.Bomb, 0, 0);
              GameObjectManager.RemoveGameObject(bt);
              GameObjectManager.RemoveGameObject(bl);
              GameObjectManager.RemoveGameObject(bd);
            
              GameObjectFactory.CreateUFO();
              CollisionPairManager.CreateCollisionPairs();
        }

        public override void Start()
        {
            //******* START GAME ******
            GameObjectFactory.ActivateGameOverlay();
            GameObjectFactory.ActivateShields();
            GameObjectFactory.ActivateAlienGrid();
            GameObjectFactory.ActivateShip();
            GameObjectFactory.ActivateMissile();
            GameObjectFactory.ActivateBombs();
            GameObjectFactory.ActivateUFO();
            GameObjectFactory.ActivateExplosions();
           }

        public override void Stop()
        {
            GameObjectFactory.DeactivateGameOverlay();
            GameObjectFactory.DeactivateShields();
            GameObjectFactory.DeactivateAlienGrid();
            GameObjectFactory.DeactivateShip();
            GameObjectFactory.DeactivateMissile();
            GameObjectFactory.DeactivateBombs();
            GameObjectFactory.DeactivateUFO();
            GameObjectFactory.DeactivateExplosions();
            TimerManager.Destroy();
            SoundManager.Stop();
        }
        public void Pause() {
            GameObjectFactory.DeactivateMissile();
            GameObjectFactory.DeactivateBombs();
            GameObjectFactory.DeactivateUFO();
            SoundManager.Stop();
        }

        public override void SpaceKey()
        {            
            Ship ship = (Ship)GameObjectManager.FindGameObject(GameObject.Name.Ship);
            if (ship != null)
                ship.shoot();
        }

        public override void SKey()
        {
            if (SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CollisionLayer).isVisible())
                GameObjectFactory.DeactivateCollisionRects();
            else
                GameObjectFactory.ActivateCollisionRects();
        }

        public override void OneKey()
        {

        }

        public override void TwoKey()
        {

        }

    }

    public class GameOverState : GameState
    {
        public static GameOverState Instance(){
            GameOverState instance = GameManager.GetCurrentPlayer().gameOverStateInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().gameOverStateInstance = new GameOverState();
                instance = GameManager.GetCurrentPlayer().gameOverStateInstance;
            }
            return instance;
        }


        private GameOverState()
        {
            this.setStateName(GameState.Name.GAMEOVER);
            AttractMode.CreateGameOver();
        }

        public override void Start()
        {
            GameObjectFactory.DeactivateShieldExplosions();
            SoundManager.Stop();
            TimerManager.Destroy();
            AttractMode.ActivateAttractOverlay();
            AttractMode.ActivateGameOver();
            PlayerManager.SetHighScore();
            GameManager.SetCurrentPlayer(GameManager.GetPlayerOne());   
        }

        public override void Stop()
        {
            AttractMode.DeactivateAttractOverlay();
            AttractMode.DeactivateGameOver();

            
        }

       /* public override void Pause()
        {

        }
        */
        public override void SKey() { }
        public override void SpaceKey() { }
        public override void OneKey() { }
        public override void TwoKey() { }
    }
}
