﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class ShipState
    {
        public abstract void moveRight();
        public abstract void moveLeft();
        public abstract void shoot(Ship context);
    }

    public class ShipDisabledState : ShipState
    {
        public override void moveLeft()
        {
            //Dont move ship left
        }

        public override void moveRight()
        {
            //Don't move ship right
        }

        public override void shoot(Ship context)
        {
            //Do nothing, ship disabled
        }
    }



    public class MissileReadyState : ShipState
    {
        public override void moveLeft()
        {
            //Move Ship
            CommandNode move = CommandManager.Add(CommandNode.Name.ShipMoveLeft, GameObject.Name.Ship, 0, 0, GameObjectMoveCommand.Direction.WEST, 100.0f, 4.0f, false);
            TimerManager.Add(move.getCommand(), 0);
        }

        public override void moveRight()
        {
            //move ship
            CommandNode move = CommandManager.Add(CommandNode.Name.ShipMoveRight, GameObject.Name.Ship, 0, 0, GameObjectMoveCommand.Direction.EAST, 100.0f, 4.0f, false);
            TimerManager.Add(move.getCommand(), 0);
        }

        public override void shoot(Ship context)
        {
            //Play shoot sound
            SoundManager.Play(Soundk.Name.ShootSound);
            
            GameObject ship = GameObjectManager.FindGameObject(GameObject.Name.Ship);

            //Set Up Missile Tree
            GameObjectFactory.SetTree(TreeManager.Add(TreeNode.Name.MissileTree).getTree());
            GameObjectFactory.SetParent(GameObjectManager.FindGameObject(GameObject.Name.MissileRoot));

            RectManager.Add(Rect.Name.Missile, 0, ship.getX() - 1.0f, 154.0f, 3.0f, 10.0f);
            GameObject missile = GameObjectFactory.NewGameObject(GameObject.Name.Missile, Rect.Name.Missile, 0, 0);

            //move missile to start position
            GameObjectManager.MoveGameObject(GameObject.Name.Missile, ship.getX() - 1, 154.0f); //336, 154

            //Start missile moving
            CommandNode move = CommandManager.Add(CommandNode.Name.MissileMove, GameObject.Name.Missile, 0, 0, GameObjectMoveCommand.Direction.NORTH, 500.0f, 10.0f, true);
            TimerManager.Add(move.getCommand(), 0.01f);

            context.setState(new MissileFlyingState());
        }
    }

    public class MissileFlyingState : ShipState
    {
        public override void moveLeft()
        {
            //Move Ship
            CommandNode move = CommandManager.Add(CommandNode.Name.ShipMoveLeft, GameObject.Name.Ship, 0, 0, GameObjectMoveCommand.Direction.WEST, 100.0f, 4.0f, false);
            TimerManager.Add(move.getCommand(), 0);
        }

        public override void moveRight()
        {
            //move ship
            CommandNode move = CommandManager.Add(CommandNode.Name.ShipMoveRight, GameObject.Name.Ship, 0, 0, GameObjectMoveCommand.Direction.EAST, 100.0f, 4.0f, false);
            TimerManager.Add(move.getCommand(), 0);
        }

        public override void shoot(Ship context)
        {
            //Do nothing, missile already flying
        }
    }
}
