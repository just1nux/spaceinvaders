﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class UFO : GameObject
    {
        UFOStrategy strategy;
        private bool markForDeath;

        public UFO(GameObject.Name name, Rect.Name rect, int index, int rectIndex, UFOStrategy strategy)
            : base(name, rect, index, rectIndex, Sprite.Name.UFO, Image.Name.UFO1, SpriteLayer.Name.UFOLayer)
        {
            this.strategy = strategy;
        }

        public void setStrategy(UFOStrategy strategy)
        {
            this.strategy = strategy;
        }

        public void setMarkForDeath(bool markForDeath)
        {
            this.markForDeath = markForDeath;
        }

        public bool isMarkForDeath()
        {
            return markForDeath;
        }


        public void Start()
        {
            this.strategy.Start(this);
        }

        protected override void loadImages()
        {
            ImageManager.Add(Image.Name.UFO1, Texture.Name.Aliens, 120.0f, 788.0f, 240.0f, 105.0f);

        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitUFO(this);
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, m);
            pair.notifyListeners();
        }

        public override void VisitWall(Wall w)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(this, w);
            pair.notifyListeners();
        }
    }
}
