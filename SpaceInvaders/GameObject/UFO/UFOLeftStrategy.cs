﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class UFOLeftStrategy:UFOStrategy
    {
        public UFOLeftStrategy()
        {

        }

        public override void Start(UFO ufo)
        {
            ufo.setX(70.0f);
            Command command = new GameObjectMoveCommand(ufo, GameObjectMoveCommand.Direction.EAST, 560.0f, 5.0f);
            TimerManager.Add(command, 0.02f);

            Random random = new Random();
            float randomTimeForBombDrop = random.Next(50, 200) / 100;
            TimerManager.Add(new DelayedBombDropCommand(true), randomTimeForBombDrop);
        }
    }
}
