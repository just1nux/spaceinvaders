﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class UFORightStrategy : UFOStrategy
    {
        public UFORightStrategy()
        {

        }

        public override void Start(UFO ufo)
        {
            ufo.setX(600.0f);
            Command command = new GameObjectMoveCommand(ufo, GameObjectMoveCommand.Direction.WEST, 560.0f, 5.0f);
            TimerManager.Add(command, 0.02f);

            Random random = new Random();
            float randomTimeForBombDrop = random.Next(50, 200) / 100;
            TimerManager.Add(new DelayedBombDropCommand(true), randomTimeForBombDrop);
        }
    }
}