﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class UFORoot : GameObjectNull
    {
        public UFORoot()
            : base()
        {
            setName(GameObject.Name.UFORoot);
            setIndex(0);
            GameObjectFactory.SetTree(TreeNode.Name.UFOTree);
            GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitUFORoot(this);
        }

        public override void VisitMissileRoot(MissileRoot r)
        {
            CollisionPair.Collide((GameObject)r.getChild(), (GameObject)this.getChild());
        }

    }
}
