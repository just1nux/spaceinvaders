﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class UFOStrategy
    {
        abstract public void Start(UFO ufo);
    }
}
