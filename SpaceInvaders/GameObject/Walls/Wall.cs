﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Wall: GameObject
    {
        public Wall(GameObject.Name name, Rect.Name rect, int index, int rectIndex, Sprite.Name sprite)
            : base(name, rect, index, rectIndex, sprite, Image.Name.NullObject, SpriteLayer.Name.WallLayer, true)
        {

        }

        protected override void loadImages(){
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
            otherGameObject.VisitWall(this);
        }

        public override void VisitGrid(Grid a)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(a, this);
            pair.notifyListeners();
        }

        public override void VisitColumn(Column c)
        {
        }

        public override void VisitMissileRoot(MissileRoot r)
        {
            CollisionPair.Collide(this, (GameObject)r.getChild());
        }

        public override void VisitMissile(Missile m)
        {
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(m, this);
            pair.notifyListeners();
        }

        public override void VisitShip(Ship s)
        {
            //Debug.WriteLine("Collision ObserverShip!");
            CollisionPair pair = CollisionPairManager.getActiveCollisionPair();
            Debug.Assert(pair != null);
            pair.setCollision(s, this);
            pair.notifyListeners();
        }

        public override void VisitBombRoot(BombRoot b)
        {
            CollisionPair.Collide(this, (GameObject)b.getChild());
        }
    }
}
