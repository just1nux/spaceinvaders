﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class WallRoot : GameObjectNull
    {

        public WallRoot()
            : base()
        {
          GameObjectFactory.SetTree(TreeNode.Name.WallTree);
          setName(GameObject.Name.WallRoot);
          GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
              otherGameObject.VisitWallRoot(this);
        }

        public override void VisitGrid(Grid grid)
        {
            CollisionPair.Collide(grid, (GameObject) this.getChild());
        }

        public void loadWalls()
        {
            //Set Up Wall Tree
            GameObjectFactory.SetTree(TreeManager.Add(TreeNode.Name.WallTree).getTree());
            GameObjectFactory.SetParent(this);

            //Left Wall
            RectManager.Add(Rect.Name.WallLeft, 0, 13.0f, 391.5f, 25.0f, 446.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallLeft, Rect.Name.WallLeft, 0);

            //Right Wall
            RectManager.Add(Rect.Name.WallRight, 0, 659.0f, 391.5f, 25.0f, 446.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallRight, Rect.Name.WallRight, 0);

            //Top Wall
            RectManager.Add(Rect.Name.WallTop, 0, 336.0f, 691.0f, 671.0f, 152.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallTop, Rect.Name.WallTop, 0);

            //Bottom Wall
            RectManager.Add(Rect.Name.WallBottom, 0, 336.0f, 48.0f, 671.0f, 95.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallBottom, Rect.Name.WallBottom, 0);
        }

        public override void VisitUFORoot(UFORoot u)
        {
            CollisionPair.Collide((GameObject)this.getChild(), (GameObject)u.getChild());
        }
    }
}

