﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class WallShipRoot : GameObjectNull
    {

        public WallShipRoot()
            : base()
        {
          GameObjectFactory.SetTree(TreeNode.Name.WallShipTree);
          setName(GameObject.Name.WallShipRoot);
          GameObjectManager.Add(this);
        }

        public override void update()
        {
            base.baseUpdateBoundingBox();
            base.update();
        }

        public override void Accept(CollisionVisitor otherGameObject)
        {
              otherGameObject.VisitWallShipRoot(this);
        }

        public override void VisitGrid(Grid grid)
        {
            CollisionPair.Collide(grid, (GameObject) this.getChild());
        }

        public void loadWalls()
        {
            //Set Up Wall Tree
            GameObjectFactory.SetTree(TreeManager.Add(TreeNode.Name.WallShipTree).getTree());
            GameObjectFactory.SetParent(this);

            //Left Ship Wall
            RectManager.Add(Rect.Name.WallShipLeft, 0, 27.0f, 132.0f, 55.0f, 73.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallShipLeft, Rect.Name.WallShipLeft, 0);

            //Right Ship Wall
            RectManager.Add(Rect.Name.WallShipRight, 0, 644.0f, 132.0f, 55.0f, 73.0f);
            GameObjectFactory.NewGameObject(GameObject.Name.WallShipRight, Rect.Name.WallShipRight, 0);

        }
    }
}

