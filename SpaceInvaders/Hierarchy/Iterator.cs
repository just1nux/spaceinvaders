﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    abstract class Iterator
    {
        public abstract PCSNode getFirst();
        public abstract PCSNode getNext();
        public abstract bool hasNext();
        public abstract PCSNode getCurrent();
    }
}
