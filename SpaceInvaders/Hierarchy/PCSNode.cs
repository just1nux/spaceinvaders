﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class PCSNode
    {
        private PCSTree tree;
        private PCSNode parent;
        private PCSNode child;
        private PCSNode sibling;
        private Enum name;

        // Constructors: --------------------------------
        public PCSNode()
        {
            this.parent = null;
            this.child = null;
            this.sibling = null;
        }

        public PCSNode(PCSNode pNode)
        {
            this.parent = pNode.parent;
            this.child = pNode.child;
            this.sibling = pNode.sibling;
        }

        public PCSNode(PCSNode pParent, PCSNode pChild, PCSNode pSibling)
        {
            this.parent = pParent;
            this.child = pChild;
            this.sibling = pSibling;
        }
   
        // Methods: Dump ------------------
        public void setTree(PCSTree tree)
        {
            this.tree = tree;
        }

        public PCSTree getTree()
        {
            return tree;
        }


        public Enum getName()
        {
            return this.name;
        }

        public void setName(Enum name)
        {
            this.name = name;
        }

        public void dumpNode()
        {
            Debug.WriteLine("");
            Debug.WriteLine("    name: {0} {1}", this.getName(),this.GetHashCode() );
            if (this.parent != null)
            {
                Debug.WriteLine("  parent: {0} {1}", this.parent.getName(), this.parent.GetHashCode() );
            }
            else
            {
                Debug.WriteLine("  parent: ------");
            }
            if (this.child != null)
            {
                Debug.WriteLine("   child: {0} {1}", this.child.getName(), this.child.GetHashCode() );
            }
            else
            {
                Debug.WriteLine("   child: ------");
            }
            if (this.sibling != null)
            {
                Debug.WriteLine(" sibling: {0} {1}", this.sibling.getName(), this.sibling.GetHashCode() );
            }
            else
            {
                Debug.WriteLine(" sibling: ------");
            }
        
        }

        public bool hasNext()
        {
            bool done = false;

            if (getChild() != null || getSibling() != null)
                done = true;

            return done;
        }

        public PCSNode getParent()
        {
            return this.parent;
        }

        public void setParent(PCSNode parent)
        {
            this.parent = parent;
        }

        public PCSNode getChild()
        {
            return this.child;
        }

        public void setChild(PCSNode child)
        {
            this.child = child;
        }

        public PCSNode getSibling()
        {
            return this.sibling;
        }

        public void setSibling(PCSNode sibling)
        {
            this.sibling = sibling;
        }
        
    }
}
