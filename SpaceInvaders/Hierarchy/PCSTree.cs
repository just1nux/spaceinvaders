﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class PCSTree
    {
        
        public enum Name
        {
            Root,
            Not_Initialized
        }

        private PCSNode root;
        public int numNodes;
        public int maxNodeCount;


        // constructor
        public PCSTree()
        {
            this.root = null;
            this.maxNodeCount = 0;
            this.numNodes = 0;

            // create the root
            PCSNode pcsRoot = new PCSRootNode(PCSTree.Name.Root);
            this.Insert(pcsRoot, null);
        }

        public PCSNode getRoot()
        {
            return this.root;
        }

        public void setRoot(PCSNode root)
        {
            this.root = root;
        }

        // insert
        public void Insert(PCSNode inNode, PCSNode parent)
        {
            Debug.Assert(inNode != null);

            // insert to root
            if (parent == null)
            {
                this.root = inNode;
                inNode.setChild(null);
                inNode.setParent(null);
                inNode.setSibling(null);

                this.privInfoAddNode();
            }
            else  // insert inside the tree
            {
                if (parent.getChild() == null)
                { 
                    parent.setChild(inNode);

                    inNode.setParent(parent);
                    inNode.setChild(null);
                    inNode.setSibling(null);

                    this.privInfoAddNode();
                }
                else
                {   
                    // Get first child
                    PCSNode first = parent.getChild();

                    inNode.setParent(parent);
                    inNode.setChild(null);
                    inNode.setSibling(first);

                    parent.setChild(inNode);

                    this.privInfoAddNode();
                }
            }

            inNode.setTree(this);
        }

        // remove
        public void Remove(PCSNode inNode)
        {
            Debug.Assert(inNode != null);
            inNode.setTree(null);

            if (inNode.getChild() == null)
            {
                // last node
                if (inNode.getSibling() == null)
                {
                    // find the previous child
                    PCSNode pParent;
                    pParent = inNode.getParent();

                    // special case root
                    if (pParent == null)
                    {
                        this.root = null;
                    }
                    else
                    {   // no children, no younger siblings
                        privRemoveNodeNoYoungerSiblings(inNode, pParent);
                    }
                }
                else
                {   // No children, but has other younger siblings
                    privRemoveNodeHasYoungerSiblings(inNode);
                }

                inNode.setChild(null);
                inNode.setParent(null);
                inNode.setSibling(null);
                this.privInfoRemoveNode();
                return;
            }
            else
            {
                // If we are here, recursively call
                PCSNode pTmp = inNode.getChild();
                Debug.Assert(pTmp != null);

                this.Remove(pTmp);
                this.Remove(inNode);
            }

        }

        private void privRemoveNodeNoYoungerSiblings(PCSNode inNode, PCSNode pParent)
        {
            Debug.Assert(pParent != null);

            PCSNode pTmp;
            // goto eldest child
            pTmp = pParent.getChild();
            Debug.Assert(pTmp != null);

            if (pTmp.getSibling() == null)
            {   // delete inNode so it's parent is 0
                // in child has no siblings
                pParent.setChild(null);
            }
            else
            {   // now iterate until child
                while (pTmp.getSibling() != inNode)
                {
                    pTmp = pTmp.getSibling();
                }
                // this point we found the sibling
                PCSNode pPrev = pTmp;
                pPrev.setSibling(null);
            }
        }

        private void privRemoveNodeHasYoungerSiblings(PCSNode inNode)
        {
            // I have a sibling to the right of me
            // find the previous child
            PCSNode pParent;
            pParent = inNode.getParent();
            Debug.Assert(pParent != null);

            PCSNode pTmp;

            // goto eldest child
            pTmp = pParent.getChild();
            Debug.Assert(pTmp != null);

            if (pTmp == inNode)
            {   // we are deleting a eldest child with siblings
                pParent.setChild(pTmp.getSibling());
            }
            else
            {   // now iterate until child
                while (pTmp.getSibling() != inNode)
                {
                    pTmp = pTmp.getSibling();
                }

                // this point we found the sibling
                PCSNode pPrev = pTmp;
                pPrev.setSibling(inNode.getSibling());
            }
        }

        public void printObjects()
        {
            Debug.WriteLine("");
            Debug.WriteLine("Tree---------------------------");

            PCSTreeIterator iterator = new PCSTreeIterator(this.root);

            PCSNode node = iterator.getFirst();

            while (iterator.hasNext())
            {
                node.dumpNode();
                node = iterator.getNext();
            }
        }

        private void privInfoAddNode()
        {
            this.numNodes += 1;

            if (this.numNodes > this.maxNodeCount)
            {
                this.maxNodeCount += 1;
            }
        }

        private void privInfoRemoveNode()
        {
            numNodes -= 1;
        }


        class PCSRootNode : PCSNode
        {
            public PCSRootNode(PCSTree.Name treeName)
                : base()
            {
                this.setName(treeName);
            }
        }
    }
}

