﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class PCSTreeIterator : Iterator
    {
        private PCSNode root;
        private PCSNode current;

        public PCSTreeIterator(PCSNode rootNode)
        {
            Debug.Assert(rootNode != null);
            this.root = rootNode;
            this.current = this.root;
        }

        public override PCSNode getFirst()
        {
            this.current = this.root;
            return this.current;
        }

        public override PCSNode getNext()
        {
            this.current = privGetNext(this.current);

            return this.current;
        }

        private PCSNode privGetNext(PCSNode node, bool UseChild = true)
        {
            PCSNode tmp = null;
            if (node != null)
            {
                if ((node.getChild() != null) && UseChild)
                {
                    tmp = node.getChild();
                }
                else if (node.getSibling() != null)
                {
                    tmp = node.getSibling();
                }
                else if (node.getParent() != this.root)
                {
                    tmp = this.privGetNext(node.getParent(), false);
                }
                else
                {
                    tmp = null;
                }
            }

            return tmp;
        }

        public override bool hasNext()
        {
            return (this.current != null);
        }

        public override PCSNode getCurrent()
        {
            return this.current;
        }
    }
}
