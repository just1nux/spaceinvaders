﻿using System;
using System.Diagnostics;


namespace SpaceInvaders
{


    public class Image : Container
    {
        public enum Name
        {
            Uninitialized,
            NullObject,
            Ship1,
            Ship2,
            Ship3,
            UFO1,
            UFO2,
            Missile,
            MissileExplosion,
            MissileWallHit,
            Squid1,
            Squid2,
            Crab1,
            Crab2,
            Octopus1,
            Octopus2,
            Template,
            Solid,
            Shield,
            ShieldCube,
            BombLightning1,
            BombLightning2,
            BombLightning3,
            BombLightning4, 
            BombDagger1,
            BombDagger2,
            BombDagger3,
            BombDagger4,
            BombTwister1,
            BombTwister2,
            BombTwister3,
            AlienExplosion1,
            AlienExplosion2,
            BombMissileExplosion1,
            BombMissileExplosion2,
            BombMissileExplosion3,
            WallExplosion,
            WallExplosion2,
            UFOPoints,
            BombUFO1,
            BombUFO2,
            BombUFO3,
            BombUFO4,

        }

        private Name name;
        private Texture texture;
        private Azul.Rect rect;

        public Image()
        {
            initialize();
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public void setTexture(Texture texture)
        {
            this.texture = texture;
        }

        public void setTexture(Texture.Name name)
        {
            this.texture = TextureManager.Instance().findActiveByName(name);
        }

        public Texture getTexture()
        {
            return this.texture;
        }

        public void setRect(float x, float y, float w, float h)
        {
            if (this.rect == null){
                this.rect = new Azul.Rect(x, y, w, h);
            }
        }

        public void setRect(Azul.Rect rect)
        {
            this.rect = rect;
        }

        public Azul.Rect getRect()
        {
            return this.rect;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public void set(int x)
        {

        }


    }

}

