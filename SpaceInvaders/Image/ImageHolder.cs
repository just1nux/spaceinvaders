﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class ImageHolder: SLink
    {
        private Image image;

        public ImageHolder(Image image)
            : base()
        {
            setImage(image);
        }

        public void setImage(Image image){
            this.image = image;
        }

        public Image getImage()
        {
            return this.image;
        }


    }
}