﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class ImageManager : ContainerManager
    {
        private static volatile ImageManager instance;

        private ImageManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
            TextureManager.Add(Texture.Name.Aliens, "aliensv4.tga");
            TextureManager.Add(Texture.Name.Solid, "fill.tga");  
        }

        public static ImageManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new ImageManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static ImageManager Instance()
        {
            if (instance == null)
            {
                instance = new ImageManager();
            }
            return instance;
        }

        public static Image Add(Image.Name name)
        {
            ImageManager manager = ImageManager.Instance();
            Image image = manager.findActiveByName(name);
            if (image == null)
            {
                image = (Image)manager.addLink();
                image.setName(name);
            }
            return image;


        }

        public static Image Add(Image.Name name, Texture.Name textureName, float x, float y, float w, float h)
        {
            Image image = ImageManager.Add(name);
            image.setTexture(textureName);
            image.setRect(x, y, w, h);
            return image;
        }

        protected override Container create()
        {
            Container link = new Image();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Image nodeA = (Image)linkA;
            Image nodeB = (Image)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public Image findActiveByName(Image.Name name)
        {
            Image foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Image node = (Image)pullFromReserve();
                node.setName(name);
                foundNode = (Image)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }
        public static void Print()
        {
            ImageManager manager = ImageManager.Instance();
            Debug.WriteLine("----Image Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            ImageManager manager = ImageManager.Instance();
            manager.baseDestroy();
        }
    }
}