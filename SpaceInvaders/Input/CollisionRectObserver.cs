﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class CollisionRectVisObserver:InputObserver
    {
        public override void Notify()
        {
            GameManager.GetState().SKey();
        }
    }
}
