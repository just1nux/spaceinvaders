﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    
    class OneKeyObserver : InputObserver
    {
        public override void Notify()
        {
            GameManager.GetState().OneKey();
        }
    }

    class TwoKeyObserver : InputObserver
    {
        public override void Notify()
        {
            GameManager.GetState().TwoKey();
        }
    }
}
