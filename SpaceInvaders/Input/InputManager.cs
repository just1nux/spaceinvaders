﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class InputManager
    {
        private static InputManager instance = null;
        private bool prevIsSpaceKey;
        private bool prevIsSKey;

        private InputSubject subjectArrowRight;
        private InputSubject subjectArrowLeft;
        private InputSubject subjectSpace;
        private InputSubject subjectEnter;
        private InputSubject subjectVisCollision;
        private InputSubject subjectOne;
        private InputSubject subjectTwo;



        private InputManager()
        {
            this.subjectArrowRight = new InputSubject();
            this.subjectArrowLeft = new InputSubject();
            this.subjectSpace = new InputSubject();
            this.subjectEnter = new InputSubject();
            this.subjectVisCollision = new InputSubject();
            this.subjectOne = new InputSubject();  
            this.subjectTwo = new InputSubject();  
            this.prevIsSpaceKey = false;
            this.prevIsSKey = false;
        }

        private static InputManager Instance()
        {
            if (instance == null)
            {
                instance = new InputManager();
            }

            return instance;
        }

        public static void SetSubjectArrowRight(InputSubject subjectArrowRight)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectArrowRight = subjectArrowRight;
        }

        public static void SetSubjectArrowLeft(InputSubject subjectArrowLeft)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectArrowLeft = subjectArrowLeft;
        }

        public static void SetSubjectSpace(InputSubject subjectSpace)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectSpace = subjectSpace;
        }

        public static void SetSubjectOne(InputSubject subjectOne)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectOne = subjectOne;
        }

        public static void SetSubjectTwo(InputSubject subjectTwo)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectTwo = subjectTwo;
        }

        public static void SetSubjectVisCollision(InputSubject subjectVisCollision)
        {
            InputManager manager = InputManager.Instance();
            manager.subjectVisCollision = subjectVisCollision;
        }


        public static InputSubject GetSubjectArrowRight()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectArrowRight;
        }

        public static InputSubject GetSubjectArrowLeft()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectArrowLeft;
        }

        public static InputSubject GetSubjectSpace()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectSpace;
        }

        public static InputSubject GetSubjectVisCollision()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectVisCollision;
        }

        public static InputSubject GetSubjectOne()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectOne;
        }

        public static InputSubject GetSubjectTwo()
        {
            InputManager manager = InputManager.Instance();
            return manager.subjectTwo;
        }


        public static void Update()
        {
            InputManager manager = InputManager.Instance();

            bool isSpaceKey = Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_SPACE);
            bool isSKey = Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_S);

            if (isSpaceKey && !manager.prevIsSpaceKey)
            {
                GetSubjectSpace().Notify();
            }
            manager.prevIsSpaceKey = isSpaceKey;

            if (Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_ARROW_LEFT))
            {
                GetSubjectArrowLeft().Notify();
            }

            if (Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_ARROW_RIGHT))
            {
                GetSubjectArrowRight().Notify();
            }

            if (Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_S) && !manager.prevIsSKey)
            {
                GetSubjectVisCollision().Notify();
            }

            if (Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_1))
            {
                GetSubjectOne().Notify();
            }

            if (Azul.Input.GetKeyState(Azul.AZUL_KEY.KEY_2))
            {
                GetSubjectTwo().Notify();
            }

            manager.prevIsSpaceKey = isSpaceKey;
            manager.prevIsSKey = isSKey;

        }
    }
}
