﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    abstract class InputObserver : CollisionLink
    {
        abstract public void Notify();
        public InputSubject subject;

        public void setSubject(InputSubject subject)
        {
            this.subject = subject;
        }

        public InputSubject getSubject()
        {
            return this.subject;
        }
    }
}

