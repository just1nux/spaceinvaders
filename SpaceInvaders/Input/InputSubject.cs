﻿using System;
using System.Diagnostics;


namespace SpaceInvaders
{
    class InputSubject
    {
        private InputObserver observers;

        public void setObservers(InputObserver observers)
        {
            this.observers = observers;
        }

        public InputObserver getObservers()
        {
            return this.observers;
        }

        public void Attach(InputObserver observer)
        {
            observer.setSubject(this);

            if (getObservers() == null)
            {
                setObservers(observer);
                observer.setNext(null);
                observer.setPrev(null);
            }
            else
            {
                observer.setNext(observers);
                observer.setPrev(null);
                getObservers().setPrev(observer);
                setObservers(observer);
            }
         }
        
        public void Notify()
        {
            InputObserver node = getObservers();

            while (node != null)
            {
                node.Notify();
                node = (InputObserver)node.getNext();
            }
           
        }
    }
}

