﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class MoveLeftObserver : InputObserver
    {
        public override void Notify()
        {
            //Get Ship
            Ship ship = (Ship) GameObjectManager.FindGameObject(GameObject.Name.Ship);
            if (ship!= null)
            ship.moveLeft();
        }
    }
}
