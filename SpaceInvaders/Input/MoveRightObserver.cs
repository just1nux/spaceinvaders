﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class MoveRightObserver : InputObserver
    {
        public override void Notify()
        {
            Ship ship = (Ship)GameObjectManager.FindGameObject(GameObject.Name.Ship);
            if (ship!=null)
            ship.moveRight();
        }
    }
}
