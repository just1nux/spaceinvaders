﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class GameManager : Azul.Game
    {
        private static volatile GameManager instance;
        private GameState gameState;
        private Player currentPlayer;
        private Player playerOne;
        private Player playerTwo;
        private int highScore;
        private int level;
        private int numPlayers;
       
        private GameManager()
            : base()
        {
           
        }

        public static GameManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new GameManager();
                
            }
            return instance;
        }

        public override void Initialize()
        {
            // Game Window Device setup
            this.SetWindowName("Space Invaders");
            this.SetWidthHeight(672, 768);
            this.SetClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            this.highScore = 0;
            this.level = 0;
        }

        public override void LoadContent()
        {
            //Load sounds
            SoundManager.Add(Soundk.Name.fastinvader1, "fastinvader1.wav");
            SoundManager.Add(Soundk.Name.fastinvader2, "fastinvader2.wav");
            SoundManager.Add(Soundk.Name.fastinvader3, "fastinvader3.wav");
            SoundManager.Add(Soundk.Name.fastinvader4, "fastinvader4.wav");
            SoundManager.Add(Soundk.Name.ShootSound, "shoot.wav");
            SoundManager.Add(Soundk.Name.invaderkilled, "invaderkilled.wav");
            SoundManager.Add(Soundk.Name.ufohighpitch, "ufo_highpitch.wav", true);
            SoundManager.Add(Soundk.Name.ufolowpitch, "ufo_lowpitch.wav", false); 
            

            // Resident loads
            //Make new players
            this.playerTwo = new Player(2);
            this.currentPlayer = playerTwo;
            GameObjectFactory.CreateGameOverlay();
            
            this.playerOne = new Player(1);
            this.currentPlayer = playerOne;
            GameObjectFactory.CreateGameOverlay();
            

            //******* CREATE GAME INPUTS *******
            GameObjectFactory.CreateInputs();

            //Set the initial State toi Attract Mode
            this.gameState = AttractState.Instance();
            GetState().Start();

            Print();            
        }

        public override void Update()
        {
            InputManager.Update();
            TimerManager.Update();
            GameObjectManager.Update();
            CollisionPairManager.Process();
            DelayedObjectMan.Process();
            PlayerManager.Update();
        }

        //-----------------------------------------------------------------------------
        // Game::Draw()
        //		This function is called once per frame
        //	    Use this for draw graphics to the screen.
        //      Only do rendering here
        //-----------------------------------------------------------------------------
        public override void Draw()
        {
            SpriteLayerManager.Draw();
        }

        //-----------------------------------------------------------------------------
        // Game::UnLoadContent()
        //       unload content (resources loaded above)
        //       unload all content that was loaded before the Engine Loop started
        //-----------------------------------------------------------------------------
        public override void UnLoadContent()
        {
            TimerManager.Destroy(); //N
            SpriteProxyManager.Destroy(); //Y
            SpriteLayerManager.Destroy(); //Y
            SpriteManager.Destroy(); //N
            SpriteBoxManager.Destroy(); //?
            ColorManager.Destroy(); //N
            ImageManager.Destroy(); //N
            TextureManager.Destroy(); //N
            GameObjectManager.Destroy(); //Y
            TreeManager.Destroy(); //Y
            DeathManager.Destroy(); //?
            CommandManager.Destroy(); //N?
            GhostManager.Destroy(); //N?
            GlyphManager.Destroy(); //N
            FontManager.Destroy(); //N
        }

        public static void Print(){
             
            // SpriteProxyManager.Print();
            // SpriteManager.Print();
            // SpriteLayerManager.Print();
            // SpriteBoxManager.Print();
            // ColorManager.Print();
            // ImageManager.Print();
            // TextureManager.Print();
            // TimerManager.Print();
            // TreeManager.Print();
            // RectManager.Print();
            // DeathManager.Print();
            // GameObjectManager.Print();
            // CommandManager.Print();
            // GhostManager.Print();
            // GlyphManager.Print();
            // FontManager.Print();
            // TreeManager.Add(TreeNode.Name.BombTree).getTree().printObjects();
            // CollisionPairManager.Print();

        }

        public static void SetState(GameState gameState)
        {
           GameManager manager = GameManager.Instance();
           manager.gameState = gameState;
        }

        public static GameState GetState()
        {
            GameManager manager = GameManager.Instance();
            return manager.gameState;
        }

        public static Player GetCurrentPlayer()
        {
            return GameManager.Instance().currentPlayer;
        }

        public static void SetCurrentPlayer(Player player)
        {
            GameManager.Instance().currentPlayer = player;
        }

        public static Player GetPlayerOne()
        {
            return GameManager.Instance().playerOne;
        }

        public static Player GetPlayerTwo()
        {
            return GameManager.Instance().playerTwo;
        }

        public static int GetHighScore()
        {
            return GameManager.Instance().highScore;
        }

        public static void SetHighScore(int highScore)
        {
            if (highScore > GameManager.Instance().highScore)
                GameManager.Instance().highScore = highScore;
        }

        public static int GetLevel()
        {
            return GameManager.Instance().level;
        }

        public static void SetLevel(int level)
        {
            GameManager.Instance().level = level;
        }

        public static void IncrementLevel()
        {
            GameManager.Instance().level++;
        }

        public static int GetNumPlayers()
        {
            return GameManager.Instance().numPlayers;
        }

        public static void SetNumPlayers(int numPlayers)
        {
            GameManager.Instance().numPlayers = numPlayers;
        }



    }
}