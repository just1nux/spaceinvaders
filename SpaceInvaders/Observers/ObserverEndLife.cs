﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverEndLife: CollisionObserver
    {
        
        public ObserverEndLife()
        {

        }
        
        public override void notify()
        {
            TimerManager.Destroy();
            GamePlayingState.Instance().Pause();
            TimerManager.Add(new EndLifeCommand(), 2.0f);  
        }
    }
}
