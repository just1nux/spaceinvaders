﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverExplodeAlienSound: CollisionObserver
    {
        
        public ObserverExplodeAlienSound()
        {

        }
        
        public override void notify()
        {
            SoundManager.Play(Soundk.Name.invaderkilled);
        }
    }
}
