﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverExplodeAlienVisual: CollisionObserver
    {
        
        public ObserverExplodeAlienVisual()
        {

        }
        
        public override void notify()
        {
            GameObject alien = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.Squid || name == GameObject.Name.Crab || name == GameObject.Name.Octopus)
            {
                alien = this.getSubject().getGameObjectA();
            }
            else
            {
                alien = this.getSubject().getGameObjectB();
            }

            ImageManager.Add(Image.Name.AlienExplosion1, Texture.Name.Aliens, 262.0f, 1252.0f, 195.0f, 135.0f);
            Sprite alienExplosionSprite = SpriteManager.Add(Sprite.Name.AlienExplosion, Image.Name.AlienExplosion1, 215.0f, 345.0f, 38.0f, 24.0f);
            SpriteProxy alienExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, alienExplosionSprite, alien.getX(), alien.getY());
            SpriteLayer explosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, alienExplosionSpriteProxy);

            Command command = new SpriteRemoveFromLayerCommand(alienExplosionSpriteProxy, explosionLayer);
            TimerManager.Add(command, 0.35f);

        }
    }
}
