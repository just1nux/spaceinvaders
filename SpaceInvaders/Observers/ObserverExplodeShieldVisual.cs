﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverExplodeShieldBombVisual: CollisionObserver
    {
        
        public ObserverExplodeShieldBombVisual()
        {

        }
        
        public override void notify()
        {
            GameObject bombmissile = null;
            GameObject shieldCube = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            
            if (name == GameObject.Name.ShieldCube)
            {
                shieldCube = this.getSubject().getGameObjectA();
                bombmissile = this.getSubject().getGameObjectB();
            }
            else
            {
                shieldCube = this.getSubject().getGameObjectB();
                bombmissile = this.getSubject().getGameObjectA();
            }

            //Randomize location of explosion
            //Pick a time (.25 to 1 sec
            Random random = new Random();
            float xoffset = random.Next(-10, 10) / 1.0f;
            float yoffset = random.Next(-10, 10) / 1.0f;
            // Debug.WriteLine("TIME {0}", randomTime);

            //Set up explosion sprite
            ImageManager.Add(Image.Name.WallExplosion, Texture.Name.Aliens, 1035.0f, 1500.0f, 90.0f, 120.0f);
            Sprite shieldExplosionSprite = SpriteManager.Add(Sprite.Name.ShieldExplosion, Image.Name.WallExplosion, 215.0f, 345.0f, 17.1f, 22.8f);
            SpriteLayer layer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShieldExplosions);

            //Green Explosion
            SpriteProxy greenExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, shieldExplosionSprite, shieldCube.getX() + xoffset, shieldCube.getY() + yoffset);
            greenExplosionSpriteProxy.setColor(Color.Name.Green);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ShieldExplosions, greenExplosionSpriteProxy);

            Command removeGreen = new SpriteRemoveFromLayerCommand(greenExplosionSpriteProxy, layer);
            TimerManager.Add(removeGreen, 0.25f);

            //Black Explosion
            SpriteProxy shieldExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, shieldExplosionSprite, shieldCube.getX()+xoffset, shieldCube.getY()+yoffset);
            shieldExplosionSpriteProxy.setColor(Color.Name.Black);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ShieldExplosions, shieldExplosionSpriteProxy);
        }
    }


    class ObserverExplodeShieldMissileVisual : CollisionObserver
    {

        public ObserverExplodeShieldMissileVisual()
        {

        }

        public override void notify()
        {
            GameObject bombmissile = null;
            GameObject shieldCube = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();

            if (name == GameObject.Name.ShieldCube)
            {
                shieldCube = this.getSubject().getGameObjectA();
                bombmissile = this.getSubject().getGameObjectB();
            }
            else
            {
                shieldCube = this.getSubject().getGameObjectB();
                bombmissile = this.getSubject().getGameObjectA();
            }

            //Randomize location of explosion
            //Pick a time (.25 to 1 sec
            Random random = new Random();
            float xoffset = random.Next(-10, 10) / 1.0f;
            float yoffset = random.Next(-10, 10) / 1.0f;
            // Debug.WriteLine("TIME {0}", randomTime);

            //Set up explosion sprite
            ImageManager.Add(Image.Name.WallExplosion2, Texture.Name.Aliens, 517.0f, 1252.0f, 165.0f, 135.0f);
            Sprite shieldExplosionSprite = SpriteManager.Add(Sprite.Name.ShieldExplosion2, Image.Name.WallExplosion2, 0.0f, 0.0f, 28.05f, 22.95f);
            SpriteLayer layer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShieldExplosions);

            //Green Explosion
            SpriteProxy greenExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, shieldExplosionSprite, shieldCube.getX() + xoffset, shieldCube.getY() + yoffset);
            greenExplosionSpriteProxy.setColor(Color.Name.Green);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ShieldExplosions, greenExplosionSpriteProxy);

            Command removeGreen = new SpriteRemoveFromLayerCommand(greenExplosionSpriteProxy, layer);
            TimerManager.Add(removeGreen, 0.25f);

            //Black Explosion
            SpriteProxy shieldExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, shieldExplosionSprite, shieldCube.getX() + xoffset, shieldCube.getY() + yoffset);
            shieldExplosionSpriteProxy.setColor(Color.Name.Black);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ShieldExplosions, shieldExplosionSpriteProxy);
        }
    }
}
