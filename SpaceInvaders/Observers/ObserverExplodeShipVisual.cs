﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverExplodeShipVisual: CollisionObserver
    {
        
        public ObserverExplodeShipVisual()
        {

        }
        
        public override void notify()
        {
            GameObject bomb = null;
            GameObject ship = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.BombDagger || name == GameObject.Name.BombLightning || name == GameObject.Name.BombTwister)
            {
                bomb = this.getSubject().getGameObjectA();
                ship = this.getSubject().getGameObjectB();
            }
            else
            {
                bomb = this.getSubject().getGameObjectB();
                ship = this.getSubject().getGameObjectA();
            }

            //ship = GameObjectManager.FindGameObject(GameObject.Name.Ship);
            ship = GameObjectFactory.NewGameObject(GameObject.Name.Ship);

            //NORMAL EXPLOSION IMAGE
            ImageManager.Add(Image.Name.WallExplosion, Texture.Name.Aliens, 1035.0f, 1500.0f, 90.0f, 120.0f);
            Sprite wallExplosionSprite = SpriteManager.Add(Sprite.Name.WallExplosion, Image.Name.WallExplosion, 215.0f, 345.0f, 17.1f, 22.8f);
            SpriteProxy wallExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, wallExplosionSprite, bomb.getX(), bomb.getY()-10.0f);
            SpriteLayer explosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, wallExplosionSpriteProxy);
            wallExplosionSprite.setColor(Color.Name.Green);

            Command command = new SpriteRemoveFromLayerCommand(wallExplosionSpriteProxy, explosionLayer);
            TimerManager.Add(command, 0.30f);


            //SHIP EXPLODES

            ImageManager.Add(Image.Name.Ship1, Texture.Name.Aliens, 8.0f, 1016.0f, 228.0f, 120.0f);
            ImageManager.Add(Image.Name.Ship2, Texture.Name.Aliens, 248.0f, 1016.0f, 228.0f, 120.0f);
            ImageManager.Add(Image.Name.Ship3, Texture.Name.Aliens, 488.0f, 1016.0f, 228.0f, 120.0f);

            Sprite shipExplosionSprite = SpriteManager.Add(Sprite.Name.ShipExplosion, Image.Name.Ship2, 0f, 0f, 43.32f, 22.8f);
            SpriteProxy shipExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, shipExplosionSprite, ship.getX(), ship.getY());
            
            SpriteLayerManager.AddSprite(SpriteLayer.Name.ShipLayer, shipExplosionSpriteProxy);
            shipExplosionSprite.setColor(Color.Name.Green);

            SimpleImageChangeCommand explosionAnimation = new SimpleImageChangeCommand(Sprite.Name.ShipExplosion);
            explosionAnimation.attachImage(Image.Name.Ship2);
            explosionAnimation.attachImage(Image.Name.Ship3);
            TimerManager.Add(explosionAnimation, 0.10f);

            SpriteLayer shipLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.ShipLayer);
            Command removeCommand = new SpriteRemoveFromLayerCommand(shipExplosionSpriteProxy, shipLayer);
            TimerManager.Add(removeCommand, 2.00f);          
            
        }
    }
}
