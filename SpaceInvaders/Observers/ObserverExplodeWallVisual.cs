﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverExplodeWallBombVisual: CollisionObserver
    {
        
        public ObserverExplodeWallBombVisual()
        {

        }
        
        public override void notify()
        {
            GameObject bombmissile = null;
            GameObject wall = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.BombUFO || name == GameObject.Name.BombDagger || name == GameObject.Name.BombLightning || name == GameObject.Name.BombTwister)
            {
                bombmissile = this.getSubject().getGameObjectA();
                wall = this.getSubject().getGameObjectB();
            }
            else
            {
                bombmissile = this.getSubject().getGameObjectB();
                wall = this.getSubject().getGameObjectA();
            }

           
                ImageManager.Add(Image.Name.WallExplosion, Texture.Name.Aliens, 1035.0f, 1500.0f, 90.0f, 120.0f);
                Sprite wallExplosionSprite = SpriteManager.Add(Sprite.Name.WallExplosion, Image.Name.WallExplosion, 215.0f, 345.0f, 17.1f, 22.8f);
                SpriteProxy wallExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, wallExplosionSprite, bombmissile.getX(), bombmissile.getY());
                SpriteLayer explosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);
                SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, wallExplosionSpriteProxy);

                wallExplosionSprite.setColor(Color.Name.Green);
        
                Command command = new SpriteRemoveFromLayerCommand(wallExplosionSpriteProxy, explosionLayer);
                TimerManager.Add(command, 0.30f);
           
        }
    }


    class ObserverExplodeWallMissileVisual : CollisionObserver
    {

        public ObserverExplodeWallMissileVisual()
        {

        }

        public override void notify()
        {
            GameObject bombmissile = null;
            GameObject wall = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.Missile)
            {
                bombmissile = this.getSubject().getGameObjectA();
                wall = this.getSubject().getGameObjectB();
            }
            else
            {
                bombmissile = this.getSubject().getGameObjectB();
                wall = this.getSubject().getGameObjectA();
            }

            ImageManager.Add(Image.Name.WallExplosion2, Texture.Name.Aliens, 517.0f, 1252.0f, 165.0f, 135.0f);
            Sprite wallExplosionSprite = SpriteManager.Add(Sprite.Name.WallExplosion, Image.Name.WallExplosion2, 0.0f, 0.0f, 28.05f, 22.95f);
            SpriteProxy wallExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, wallExplosionSprite, bombmissile.getX(), bombmissile.getY());
            SpriteLayer explosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, wallExplosionSpriteProxy);

           
            wallExplosionSprite.setColor(Color.Name.Red);
           
            Command command = new SpriteRemoveFromLayerCommand(wallExplosionSpriteProxy, explosionLayer);
            TimerManager.Add(command, 0.30f);

        }
    }
}
