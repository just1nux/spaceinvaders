﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverGrid: CollisionObserver
    {        
        public ObserverGrid()
        {

        }

        public override void notify()
        {
            Grid grid = (Grid)this.getSubject().getGameObjectA();
            Wall wall = (Wall)this.getSubject().getGameObjectB();

            GameObject.Name name = (GameObject.Name)wall.getName();

            switch(name){
                case GameObject.Name.WallRight:
                    if(grid.getDeltaX () > 0){
                        grid.setDeltaX(-6.0f);
                        grid.setDeltaY(-25.0f);
                    }
                    break;
                case GameObject.Name.WallLeft:
                    if (grid.getDeltaX() < 0)
                    {
                        grid.setDeltaX(+6.0f);
                        grid.setDeltaY(-25.0f);
                    }
                    break;
                default:
                    break;
            }

        }
    }
}
