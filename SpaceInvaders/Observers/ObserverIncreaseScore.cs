﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverIncreaseScore: CollisionObserver
    {
        
        public ObserverIncreaseScore()
        {

        }
        
        public override void notify()
        {
            GameObject alien = null;
            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.Missile)
            {
                alien = (GameObject)this.getSubject().getGameObjectB();
            }
            else
            {
                alien = (GameObject)this.getSubject().getGameObjectA();
            }

            GameObject.Name alienName = (GameObject.Name) alien.getName();

            switch(alienName){
                case GameObject.Name.Octopus:
                    GameManager.GetCurrentPlayer().increaseScore(10);
                    break;
                case GameObject.Name.Crab:
                    GameManager.GetCurrentPlayer().increaseScore(20);
                    break;
                case GameObject.Name.Squid:
                    GameManager.GetCurrentPlayer().increaseScore(30);
                    break;
                default:
                    break;
            }
        }
    }
}
