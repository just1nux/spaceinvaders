﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverMissileBombExplosion: CollisionObserver
    {
        
        public ObserverMissileBombExplosion()
        {

        }
        
        public override void notify()
        {
            GameObject missile = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.Missile)
            {
                missile = this.getSubject().getGameObjectA();
            }
            else
            {
                missile = this.getSubject().getGameObjectB();
            }

            ImageManager.Add(Image.Name.BombMissileExplosion1, Texture.Name.Aliens, 60.0f, 2453.0f, 120.0f, 135.0f);
            ImageManager.Add(Image.Name.BombMissileExplosion2, Texture.Name.Aliens, 300.0f, 2453.0f, 120.0f, 135.0f);
            ImageManager.Add(Image.Name.BombMissileExplosion3, Texture.Name.Aliens, 450.0f, 2453.0f, 120.0f, 135.0f);
       
            Sprite explosionSprite = SpriteManager.Add(Sprite.Name.BombMissileExplosion, Image.Name.BombMissileExplosion1, 215.0f, 345.0f, 30.0f, 20.0f);
            SpriteProxy explosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, explosionSprite, missile.getX(), missile.getY());
            SpriteLayer explosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, explosionSpriteProxy);

            ImageChangeCommand command1 = new ImageChangeCommand(Sprite.Name.BombMissileExplosion);
            command1.attachImage(Image.Name.BombMissileExplosion2);
            TimerManager.Add(command1, 0.15f);

            ImageChangeCommand command2 = new ImageChangeCommand(Sprite.Name.BombMissileExplosion);
            command2.attachImage(Image.Name.BombMissileExplosion3);
            TimerManager.Add(command2, 0.30f);

            ImageChangeCommand command3 = new ImageChangeCommand(Sprite.Name.BombMissileExplosion);
            command2.attachImage(Image.Name.BombMissileExplosion1);
            TimerManager.Add(command2, 0.45f);

            Command command4 = new SpriteRemoveFromLayerCommand(explosionSpriteProxy, explosionLayer);
            TimerManager.Add(command4, 0.45f);

            //No Sound 0.15 per fram animation of 3 frames.
        }
    }
}
