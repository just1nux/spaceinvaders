﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverPlaySounds: CollisionObserver
    {
        Soundk.Name soundName;
        public ObserverPlaySounds(Soundk.Name soundName)
        {
            this.soundName = soundName;
        }
        
        public override void notify()
        {
            SoundManager.Play(this.soundName);                
        }
    }
}