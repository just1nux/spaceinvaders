﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverPlayerLoseLife: CollisionObserver
    {        
        public ObserverPlayerLoseLife()
        {

        }
        
        public override void notify()
        {
            GameManager.GetCurrentPlayer().decrementLives();
        }
    }
}
