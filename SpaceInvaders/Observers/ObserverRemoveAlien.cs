﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveAlien: CollisionObserver
    {
        public ObserverRemoveAlien()
        {

        }

        public override void notify()
        {
           GameObject alien = this.getSubject().getGameObjectB();

           //Remove Alien
           GameObject column = (GameObject) alien.getParent();
           GameObject grid = (GameObject) column.getParent();

           GameObjectManager.RemoveGameObject(alien);
           column.update();


            //Remove Column

            if (column.getChild() == null){
                 GameObjectManager.RemoveGameObject(column);
            }

            //Grid empty? 
           if (grid.getChild() == null)
            {
         
                GameManager.IncrementLevel();
              //  GameManager.GetState().Pause();
                Command command = new StateChangeCommand(LevelStartState.Instance(GameManager.GetNumPlayers()), GameManager.GetState());
                TimerManager.Add(command, 0.0f);
            }
        }
    }
}
