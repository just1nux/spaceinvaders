﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveBomb : CollisionObserver
    {
        private Bomb bomb;

        public ObserverRemoveBomb()
        {
            this.bomb = null;
        }

        public ObserverRemoveBomb(ObserverRemoveBomb b)
        {
            Debug.Assert(b != null);
            this.bomb = b.bomb;
        }

        public ObserverRemoveBomb(Bomb b)
        {
            Debug.Assert(b != null);
            this.bomb = b;

            if (!bomb.isMarkForDeath())
            {
                bomb.setMarkForDeath(true);
                ObserverRemoveBomb pObserver = new ObserverRemoveBomb(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void notify()
        {

            
            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.BombUFO || name == GameObject.Name.BombLightning || name == GameObject.Name.BombTwister || name == GameObject.Name.BombDagger)
            {
                this.bomb = (Bomb)this.getSubject().getGameObjectA();
            }
            else
            {
                this.bomb = (Bomb)this.getSubject().getGameObjectB();
            }

            Debug.Assert(this.bomb != null);

            if (!bomb.isMarkForDeath())
            {
                bomb.setMarkForDeath(true);
                ObserverRemoveBomb pObserver = new ObserverRemoveBomb(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void Execute()
        {
            //Remove
            GameObjectManager.RemoveGameObject(bomb);
        }
    }
}
