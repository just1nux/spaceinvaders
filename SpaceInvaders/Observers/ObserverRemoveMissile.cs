﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveMissile : CollisionObserver
    {
        private Missile missile;

        public ObserverRemoveMissile()
        {
            this.missile = null;
        }

        public ObserverRemoveMissile(ObserverRemoveMissile m)
        {
            Debug.Assert(m != null);
            this.missile = m.missile;
        }

        public ObserverRemoveMissile(Missile m)
        {
            Debug.Assert(m != null);
            this.missile = m;

            if (!missile.isMarkForDeath())
            {
                missile.setMarkForDeath(true);
                ObserverRemoveMissile pObserver = new ObserverRemoveMissile(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void notify()
        {
            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.Missile)
            {
                this.missile = (Missile)this.getSubject().getGameObjectA();
            }
            else
            {
                this.missile = (Missile)this.getSubject().getGameObjectB();
            }

            Debug.Assert(this.missile != null);

            if (!missile.isMarkForDeath())
            {
                missile.setMarkForDeath(true);
                ObserverRemoveMissile pObserver = new ObserverRemoveMissile(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void Execute()
        {
            //Command Removal
            GameObjectMoveCommand missileMove = (GameObjectMoveCommand)CommandManager.FindCommandNode(CommandNode.Name.MissileMove).getCommand();
            missileMove.setRepeating(false);

            //Reset ship state
            Ship ship = (Ship)GameObjectManager.FindGameObject(GameObject.Name.Ship);
            if (ship != null)
                ship.setState(new MissileReadyState());

            //Remove Missile
            GameObjectManager.RemoveGameObject(missile);
        }
    }
}
