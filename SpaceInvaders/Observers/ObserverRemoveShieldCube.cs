﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveShieldCube: CollisionObserver
    { 
        private ShieldCube cube;
        
        public ObserverRemoveShieldCube()
        {
            this.cube = null;
        }

        public ObserverRemoveShieldCube (ObserverRemoveShieldCube b)
        {
            Debug.Assert(b != null);
            this.cube = b.cube;
        }

        public override void notify()
        {

            //Debug.WriteLine("REMOVE MISSILE OBSERVER");
            this.cube = (ShieldCube) this.getSubject().getGameObjectB();

            Debug.Assert(this.cube != null);

            if (!cube.isMarkForDeath())
            {
                cube.setMarkForDeath(true);
                ObserverRemoveShieldCube pObserver = new ObserverRemoveShieldCube(this);
                DelayedObjectMan.Attach(pObserver);
            }
       
        }

        public override void Execute()
        {
            //Debug.WriteLine("REMOVE SHIELD CUBE {0}", cube.getIndex());
            
            //Remove
            GameObject scolumn = (GameObject)cube.getParent();
            GameObject shield = (GameObject)scolumn.getParent();
            GameObjectManager.RemoveGameObject(cube);

            //Remove Column
            if (scolumn.getChild() == null)
            {
                //Debug.WriteLine("REMOVE COLUMN {0}", scolumn.getIndex());
                GameObjectManager.RemoveGameObject(scolumn);
            }

            //Remove Shield
            if (shield.getChild() == null)
            {
                //Debug.WriteLine("REMOVE SHIELD {0}", shield.getIndex());
                GameObjectManager.RemoveGameObject(shield);
            }
             
        }

    }
}
