﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveShip : CollisionObserver
    {
        private Ship ship = null;


        public ObserverRemoveShip()
        {
            this.ship = null;
        }

        public ObserverRemoveShip(ObserverRemoveShip s)
        {
            Debug.Assert(s != null);
            this.ship = s.ship;
        }

        public ObserverRemoveShip(Ship s)
        {
            Debug.Assert(s != null);
            this.ship = s;

            if (!ship.isMarkForDeath())
            {
                ship.setMarkForDeath(true);
                ObserverRemoveShip pObserver = new ObserverRemoveShip(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void notify()
        {

            //this.ship = (Ship)GameObjectManager.FindGameObject(GameObject.Name.Ship, 0);
            this.ship = (Ship)GameObjectFactory.NewGameObject(GameObject.Name.Ship, 0);

            Debug.Assert(this.ship != null);

            if (!ship.isMarkForDeath())
            {
                ship.setMarkForDeath(true);
                ObserverRemoveShip pObserver = new ObserverRemoveShip(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void Execute()
        {
            //Remove
           
            GameObject shipRoot = (GameObject)ship.getParent();

            GameObjectManager.RemoveGameObject(ship);

            if(shipRoot != null){
                GameObjectManager.RemoveGameObject(shipRoot);
            }
            
        }
    }
}















        

