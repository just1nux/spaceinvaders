﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverRemoveUFO : CollisionObserver
    {
        private UFO ufo;

        public ObserverRemoveUFO()
        {
            this.ufo = null;
        }

        public ObserverRemoveUFO(ObserverRemoveUFO u)
        {
            Debug.Assert(u != null);
            this.ufo = u.ufo;
        }

        public ObserverRemoveUFO(UFO u)
        {
            Debug.Assert(u != null);
            this.ufo = u;

            if (!ufo.isMarkForDeath())
            {
                ufo.setMarkForDeath(true);
                ObserverRemoveUFO pObserver = new ObserverRemoveUFO(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void notify()
        {

            
            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.UFO)
            {
                this.ufo = (UFO)this.getSubject().getGameObjectA();
            }
            else
            {
                this.ufo = (UFO)this.getSubject().getGameObjectB();
            }

            Debug.Assert(this.ufo != null);

            if (!ufo.isMarkForDeath())
            {
                ufo.setMarkForDeath(true);
                ObserverRemoveUFO pObserver = new ObserverRemoveUFO(this);
                DelayedObjectMan.Attach(pObserver);
            }

        }

        public override void Execute()
        {
            //Remove
            GameObjectManager.RemoveGameObject(ufo);
        }
    }
}
