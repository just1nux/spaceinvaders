﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverResetGrid: CollisionObserver
    {        
        public ObserverResetGrid()
        {

        }

        public override void notify()
        {
            GameObject alien = null;
            GameObject shieldCube = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            
            if (name == GameObject.Name.Squid || name == GameObject.Name.Crab || name == GameObject.Name.Octopus)
            {
                alien = this.getSubject().getGameObjectA();
                shieldCube = this.getSubject().getGameObjectB();
            }
            else
            {
                alien = this.getSubject().getGameObjectA();
                shieldCube = this.getSubject().getGameObjectB();
            }

            GameObjectManager.RemoveGameObject(shieldCube);
            
            GameObject grid = (GameObject)alien.getParent().getParent();
            
            while (grid.getChild() != null)
            {
                GameObject column = (GameObject)grid.getChild();
                while (column.getChild() != null)
                {
                    GameObject falien = (GameObject)grid.getChild();
                    GameObjectManager.RemoveGameObject(falien);
                }
                GameObjectManager.RemoveGameObject(column);
            }
            GameObjectManager.RemoveGameObject(grid);

            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.SquidLayer).removeAll();
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.CrabLayer).removeAll();
            SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OctopusLayer).removeAll();

            GameObjectFactory.CreateAlienGrid();
        }
    }
}
