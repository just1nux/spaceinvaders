﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverShip: CollisionObserver
    {        
        public ObserverShip()
        {

        }

        public override void notify()
        {
            GameObject ship = this.getSubject().getGameObjectA();
            GameObject wall = this.getSubject().getGameObjectB();
            GameObject.Name name = (GameObject.Name)wall.getName();
           
            switch(name){
                case GameObject.Name.WallShipRight:
                    GameObjectManager.MoveGameObject(ship, wall.getX() - wall.getW()/2.0f - ship.getW()/2.0f, ship.getY());
                    break;
                case GameObject.Name.WallShipLeft:
                    GameObjectManager.MoveGameObject(ship, wall.getX() + wall.getW()/2.0f + ship.getW()/2.0f, ship.getY());
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }
        }
    }
}
