﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverStopSounds: CollisionObserver
    {
        
        public ObserverStopSounds()
        {
        }
        
        public override void notify()
        {
            SoundManager.Stop();                
        }
    }
}
