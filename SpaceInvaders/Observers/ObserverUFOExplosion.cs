﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    class ObserverUFOExplosion: CollisionObserver
    {
        
        public ObserverUFOExplosion()
        {

        }
        
        public override void notify()
        {
            GameObject ufo = null;

            GameObject.Name name = (GameObject.Name)this.getSubject().getGameObjectA().getName();
            if (name == GameObject.Name.UFO)
            {
                ufo = this.getSubject().getGameObjectA();
            }
            else
            {
                ufo = this.getSubject().getGameObjectB();
            }

            ImageManager.Add(Image.Name.UFO1, Texture.Name.Aliens, 120.0f, 787.0f, 240.0f, 105.0f);
            ImageManager.Add(Image.Name.UFO2, Texture.Name.Aliens, 563.0f, 787.0f, 315.0f, 105.0f);

            SpriteLayer ufoExplosionLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.Explosions);

            //UFO Points
            Random random = new Random();
            int UFOPoints = random.Next(1, 4);
            UFOPoints = UFOPoints * 100;

            //Update Score
            GameManager.GetCurrentPlayer().increaseScore(UFOPoints);

            //UFO Point Display
            Font ufofont = FontManager.Add(Font.Name.UFOPOINTS, SpriteLayer.Name.Explosions, UFOPoints.ToString(), ufo.getX()-20.0f, ufo.getY());
            SpriteFont ufoSpriteFont = ufofont.getSpriteFont();
            ufoExplosionLayer.removeSprite(ufoSpriteFont);
            ufoSpriteFont.setColor(Color.Name.Red);

            Command commandAddFont = new SpriteAddToLayerCommand(ufoSpriteFont, ufoExplosionLayer);
            TimerManager.Add(commandAddFont, 0.40f);
            
            //Explosion
            Sprite ufoExplosionSprite = SpriteManager.Add(Sprite.Name.UFOExplosion, Image.Name.UFO2, 215.0f, 345.0f, 60.0f, 20.0f);
            ufoExplosionSprite.setColor(Color.Name.Red);

            SpriteProxy ufoExplosionSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.Proxy, ufoExplosionSprite, ufo.getX(), ufo.getY());
            //remove ufo after getting location for explosion

            GameObject ufoRoot = (GameObject)ufo.getParent();

            if (ufoRoot != null)
            {
                GameObjectManager.RemoveGameObject(ufoRoot);
            }
            

            
            //Add sprite to layer
            SpriteLayerManager.AddSprite(SpriteLayer.Name.Explosions, ufoExplosionSpriteProxy);

            Command commandRemoveExplosion = new SpriteRemoveFromLayerCommand(ufoExplosionSpriteProxy, ufoExplosionLayer);
            TimerManager.Add(commandRemoveExplosion, 0.40f);

            Command commandRemoveUFOFont = new SpriteRemoveFromLayerCommand(ufoSpriteFont, ufoExplosionLayer);
            TimerManager.Add(commandRemoveUFOFont, 1.55f);
            
        }
    }
}
