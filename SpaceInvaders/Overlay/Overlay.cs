﻿using System;
using System.Diagnostics;

namespace SpaceInvaders.Overlay
{
    public static class Overlay
    {

        public static void CreateOverlayScores()
        {
            FontManager.Add(Font.Name.SCORE, SpriteLayer.Name.OverlayScores, "SCORE<1>  HI-SCORE SCORE<2>", 25, 680);
            FontManager.Add(Font.Name.NUMSCORE1, SpriteLayer.Name.OverlayScores, "0000", 75, 640);
            FontManager.Add(Font.Name.NUMSCORE2, SpriteLayer.Name.OverlayScores, "0000", 505, 640);
            FontManager.Add(Font.Name.NUMHIGHSCORE, SpriteLayer.Name.OverlayScores, "0000", 265, 640);        
        }

        public static void CreateOverlayCredits()
        {
            FontManager.Add(Font.Name.CREDITS, SpriteLayer.Name.OverlayCredits, "CREDIT", 412, 74);
            FontManager.Add(Font.Name.NUMCREDITS, SpriteLayer.Name.OverlayCredits, "00", 580, 74);
        }

        public static void CreateOverlayLives()
        {
            FontManager.Add(Font.Name.NUMLIVES, SpriteLayer.Name.OverlayLives, "", 30, 74);

            //Make ship root
            GameObjectFactory.SetTree(TreeNode.Name.ShipTree);
            ShipRoot shipRoot = (ShipRoot)GameObjectFactory.NewGameObject(GameObject.Name.ShipRoot);
            GameObjectFactory.SetParent(shipRoot);

            //loop create maximum of 8 lives
            float x = 100.0f;
            Sprite shipSpritea;

            for (int i = 1; i < 8; i++)
            {
                shipSpritea = SpriteManager.Add(Sprite.Name.Ship, Image.Name.Ship1, 215.0f, 345.0f, 38.0f, 24.0f);
                SpriteProxy shipSpriteProxy = SpriteProxyManager.Add(SpriteProxy.Name.LifeProxy, shipSpritea, x, 75.0f, i);
                SpriteLayer overlayLivesLayer = SpriteLayerManager.FindLayerByName(SpriteLayer.Name.OverlayLives);
                SpriteLayerManager.AddSprite(SpriteLayer.Name.OverlayLives, shipSpriteProxy);
                shipSpriteProxy.setVisible(false);
                x = x + 47.0f;
            }
        }

        public static void CreateOverlayBar()
        {
            ImageManager.Add(Image.Name.Solid, Texture.Name.Solid, 0f, 0f, 10.0f, 10.0f);
            Sprite sprite = SpriteManager.Add(Sprite.Name.BottomBar, Image.Name.Solid, 336f, 89.0f, 672.0f, 3.0f);
            sprite.setColor(Color.Name.Green);
            SpriteLayerManager.AddSprite(SpriteLayer.Name.OverlayBar, sprite);
        }
    }
}
