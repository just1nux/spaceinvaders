﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Player
    {
        protected int score;
        protected int lives;
        protected int num;

        public SpriteBoxManager spriteBoxManagerInstance = null;
        public SpriteLayerManager spriteLayerManagerInstance = null;
        public GameObjectManager gameObjectManagerInstance = null;
        public TreeManager treeManagerInstance = null;
        public GamePlayingState gamePlayingStateInstance = null;
        public GameOverState gameOverStateInstance = null;
        public PlayerChooseState playerChooseStateInstance = null;
        public LevelStartState levelStartStateInstance = null;
        public AttractState attractStateInstance = null;
        public GhostManager ghostManagerInstance = null;
        public FontManager fontManagerInstance = null;
        public PlayerStartState playerStartStateInstance = null;

        public Player(int num)
        {
            this.num = num;
            this.score = 0;
            this.lives = 3;
        }

        public void setNum(int num)
        {
            this.num = num;
        }

        public int getNum()
        {
            return num;
        }

        public void increaseScore(int score = 10)
        {
            if (this.score < 1500 && this.score + score >= 1500)
                this.lives++;
            if (this.score < 3000 && this.score + score >= 3000)
                this.lives++;

            this.score = this.score + score;
        }

        public void decrementLives()
        {
            this.lives--;
        }

        public int getScore()
        {
            return score;
        }

        public int getLives()
        {
            return lives;
        }

        public void resetScore()
        {
            score = 0;
        }

        public void resetLives()
        {
            lives = 3;
        }

        public void clearLives()
        {
            lives = 0;
        }

    }
}
