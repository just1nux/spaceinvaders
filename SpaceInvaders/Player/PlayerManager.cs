﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class PlayerManager
    {
        private static volatile PlayerManager instance;

        public static PlayerManager Instance()
        {
            if (instance == null)
            {
                instance = new PlayerManager();

            }
            return instance;
        }

        public static void Update(){

            updatePlayerOneScore();
            updatePlayerTwoScore();
            updateCurrentPlayerLives();
            updateHighScore();
        }

        private static void updateHighScore()
        {
            Font highText = FontManager.FindByName(Font.Name.NUMHIGHSCORE);
            int high = GameManager.GetHighScore();

            String fmt = new String('0', 4 - high.ToString().Length);
            String formattedHigh = fmt + high.ToString();

            if (highText != null)
            highText.UpdateMessage(formattedHigh);
        }

        private static void updateCurrentPlayerLives()
        {
            Font livesText = FontManager.FindByName(Font.Name.NUMLIVES);
            int lives = GameManager.GetCurrentPlayer().getLives();

            if (livesText != null)
            livesText.UpdateMessage(lives.ToString());

            for (int i = 1; i < 8; i++)
            {
                SpriteProxy proxy = SpriteProxyManager.Find(SpriteProxy.Name.LifeProxy, i);
                if (proxy != null)
                {
                    if (lives > i)
                        proxy.setVisible(true);
                    else
                        proxy.setVisible(false);
                }
            }
        }


        private static void updatePlayerOneScore()
        {
            //UPDATE PLAYER 1 SCORE
            Font playerOneScoreText = FontManager.FindByName(Font.Name.NUMSCORE1);

            int playerOneScore = GameManager.GetPlayerOne().getScore();

            if (playerOneScore > 9999)
                playerOneScore = 9999;

            String fmt = new String('0', 4 - playerOneScore.ToString().Length);
            String formattedOneScore = fmt + playerOneScore.ToString();

            if (playerOneScoreText!= null)
            playerOneScoreText.UpdateMessage(formattedOneScore);
        }

        private static void updatePlayerTwoScore()
        {
            //UPDATE PLAYER 2 SCORE
            Font playerTwoScoreText = FontManager.FindByName(Font.Name.NUMSCORE2);
            int playerTwoScore = GameManager.GetPlayerTwo().getScore();

            if (playerTwoScore > 9999)
                playerTwoScore = 9999;

            String fmt = new String('0', 4 - playerTwoScore.ToString().Length);
            String formattedTwoScore = fmt + playerTwoScore.ToString();

            if (playerTwoScoreText != null)
            playerTwoScoreText.UpdateMessage(formattedTwoScore);
        }

        public static void SetHighScore()
        {
            int score1 = GameManager.GetPlayerOne().getScore();
            int score2 = GameManager.GetPlayerTwo().getScore();

            int highScore = score1;
  
            if (score2 > score1){
                highScore = score2;
            }

            GameManager.SetHighScore(highScore);
        }
    }
}
