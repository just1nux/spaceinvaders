﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Rect: Container
    {
        private float x;
        private float y;
        private float w;
        private float h;
        private float a;
        private int index;
        private Name name;

        public Rect(float x=300, float y=300, float w=1, float h=1, float a=0)
        {
            setX(x);
            setY(y);
            setW(w);
            setH(h);
            setA(a);
        }

        public Rect(Azul.Rect rect)
        {
            Set(rect);
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }


        public void Set(CollisionRect rect)
        {
            this.setX(rect.getX());
            this.setY(rect.getY());
            this.setW(rect.getW());
            this.setH(rect.getH());
        }

        public void Set(Azul.Rect rect)
        {
            this.setX(rect.x);
            this.setY(rect.y);
            this.setW(rect.width);
            this.setH(rect.height);
        }

        public Azul.Rect getAzulRect()
        {
            return new Azul.Rect(this.x, this.y, this.w, this.h);
        }

        public static bool Intersect(CollisionRect rectA, CollisionRect rectB)
        {
            bool status = true;

            float minXA = rectA.x - rectA.w / 2;
            float maxXA = rectA.x + rectA.w / 2;
            float minYA = rectA.y - rectA.h / 2;
            float maxYA = rectA.y + rectA.h / 2;

            float minXB = rectB.x - rectB.w / 2;
            float maxXB = rectB.x + rectB.w / 2;
            float minYB = rectB.y - rectB.h / 2;
            float maxYB = rectB.y + rectB.h / 2;

            if ((maxXB < minXA) || (minXB > maxXA) || (maxYB < minYA) || (minYB > maxYA))
            {
                status = false;
            }

            return status;
        }

        public void union(CollisionRect ColRect)
        {
            float minX = Math.Min(this.x - this.w / 2, ColRect.x - ColRect.w / 2);
            float maxX = Math.Max(this.x + this.w / 2, ColRect.x + ColRect.w / 2);
            float minY = Math.Min(this.y - this.h / 2, ColRect.y - ColRect.h / 2);
            float maxY = Math.Max(this.y + this.h / 2, ColRect.y + ColRect.h / 2);

            this.w = (maxX - minX);
            this.h = (maxY - minY);
            this.x = minX + this.w / 2;
            this.y = minY + this.h / 2;                   
        }

        public float getX()
        {
            return this.x;
        }

        public float getY()
        {
            return this.y;
        }

        public float getW()
        {
            return this.w;
        }

        public float getH()
        {
            return this.h;
        }

        public float getA()
        {
            return this.a;
        }

        public void setX(float x)
        {
            this.x = x;
        }

        public void setY(float y)
        {
            this.y = y;
        }

        public void setW(float w)
        {
            this.w = w;
        }

        public void setH(float h)
        {
            this.h = h;
        }

        public void setA(float a)
        {
            this.a = a;
        }

        public override void print()
        {
            Debug.WriteLine("RECT:{0} x:{1} y:{2} w:{3} h:{4}", getName(), getX(), getY(), getW(), getH());
        }

        public Name getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public int getIndex()
        {
            return this.index;
        }

        public enum Name
        {
            Uninitialized,
            NullRect,
            Template,
            Squid,
            Crab,
            Octopus,
            Ship,
            Missile,
            WallRight,
            WallLeft,
            WallTop,
            WallBottom,
            WallShipLeft,
            WallShipRight,
            SpaceInvaders24pt,
            Shield,
            ShieldColumn,
            ShieldCube,
            Bomb,
            UFO,
        }
    }
}

