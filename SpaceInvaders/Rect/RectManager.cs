﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class RectManager : ContainerManager
    {
        private static volatile RectManager instance;

        private RectManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static RectManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new RectManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static Rect Add(Rect.Name name, int index=0, float x=0, float y=0, float w=1, float h=1, float a=0)
        {
            RectManager manager = RectManager.Instance();
            Rect node = manager.findActiveByName(name, index);
            if (node == null)
            {
                node = (Rect)manager.addLink();
                node.setName(name);
                node.setX(x);
                node.setY(y);
                node.setW(w);
                node.setH(h);
                node.setA(a);
                node.setIndex(index);
            }

            return node;
        }

        public static Rect FindByName(Rect.Name name, int index = 0)
        {
            RectManager manager = RectManager.Instance();
            Rect rect = manager.findActiveByName(name, index);
            return rect;
        }

        protected override Container create()
        {
            Container link = new Rect();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Rect nodeA = (Rect)linkA;
            Rect nodeB = (Rect)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }

        public Rect findActiveByName(Rect.Name name, int index)
        {
            Rect foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Rect node = (Rect)pullFromReserve();
                node.setName(name);
                node.setIndex(index);
                foundNode = (Rect)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            RectManager manager = RectManager.Instance();
            Debug.WriteLine("----Rect Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            RectManager manager = RectManager.Instance();
            manager.baseDestroy();
        }
    }
}
