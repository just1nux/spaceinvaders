﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Soundk : Container
    {
        private Name name;
        private bool looped;
        private IrrKlang.ISoundSource snd = null;

        public Soundk()
        {
        }

        public void set(IrrKlang.ISoundSource soundSource, bool looped = false)
        {
            snd = soundSource;
            this.looped = looped;
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public bool getLooped()
        {
            return this.looped;
        }

        public Enum getName()
        {
            return this.name;
        }

        public IrrKlang.ISoundSource getSoundSource()
        {
            return this.snd;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public enum Name
        {
            Uninitialized,
            NullSound,
            ShootSound,
            fastinvader1,
            fastinvader2,
            fastinvader3,
            fastinvader4,
            invaderkilled,
            ufohighpitch,
            ufolowpitch,
        }

    }
}
