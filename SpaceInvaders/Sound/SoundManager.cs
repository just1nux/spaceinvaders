﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class SoundManager : ContainerManager
    {
        private static volatile SoundManager instance;

        public IrrKlang.ISoundEngine sndEngine = null;
        
        private SoundManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
            // start up the engine
            this.sndEngine = new IrrKlang.ISoundEngine();
        }

        public static SoundManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new SoundManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static Soundk Add(Soundk.Name name)
        {
            SoundManager manager = SoundManager.Instance();
            Soundk node = (Soundk)manager.addLink();
            node.setName(name);
            return node;
        }

        public static void Play(Soundk.Name name)
        {
            SoundManager manager = SoundManager.Instance();
            Soundk snd = (Soundk)manager.findActiveByName(name);
            manager.sndEngine.Play2D(snd.getSoundSource(), snd.getLooped(), false, false);
        }

        public static void Stop()
        {
            SoundManager manager = SoundManager.Instance();
            manager.sndEngine.StopAllSounds();
        }

        public static Soundk Add(Soundk.Name name, String soundFile, bool looped = false)
        {
            SoundManager manager = SoundManager.Instance();
            Soundk sound = manager.findActiveByName(name);
            if (sound == null){
                sound = Add(name);
                sound.set(manager.sndEngine.AddSoundSourceFromFile(soundFile), looped);
            }

            return sound;
        }

        protected override Container create()
        {
            Container link = new Soundk();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Soundk nodeA = (Soundk)linkA;
            Soundk nodeB = (Soundk)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public Soundk findActiveByName(Soundk.Name name)
        {
            Soundk foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Soundk node = (Soundk)pullFromReserve();
                node.setName(name);
                foundNode = (Soundk)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            SoundManager manager = SoundManager.Instance();
            Debug.WriteLine("----Sound Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            SoundManager manager = SoundManager.Instance();
            manager.baseDestroy();
        }
    }
}
