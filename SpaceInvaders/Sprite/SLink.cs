﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class SLink
    {
        private SLink next;

        protected SLink()
        {
            setNext(null);
        }

        public void setNext(SLink next){
            this.next = next;
        }

        public SLink getNext()
        {
            return this.next;
        } 
    }

}