﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class Sprite : SpriteBase
    {
        public enum Name
        {
            Uninitialized,
            NullObject,
            Template,
            BottomBar,
            Squid,
            Crab,
            Octopus,
            Ship,
            Missile,
            UFO,
            WallLeft,
            WallRight,
            WallShipLeft,
            WallShipRight,
            WallTop,
            WallBottom,
            Shield,
            ShieldCube,
            BombLightning,
            BombTwister,
            BombDagger,
            AlienExplosion,
            BombMissileExplosion,
            WallExplosion,
            UFOExplosion,
            ShipExplosion,
            ShieldExplosion,
            ShieldExplosion2,
            BombUFO,
            BombUFOExplosion,
        }

        private Azul.Sprite azulSprite;
        private Image image;
        private Name name;
        private float x;
        private float y;

        public Sprite()
        {
            initialize();
        }

        public override void initialize()
        {
            setName(Sprite.Name.Uninitialized);
            setSpriteLayer(null);
            setColor(ColorManager.Add(Color.Name.NullColor, 1.0f, 1.0f, 1.0f, 1.0f));
        }

        public override void setX(float x)
        {
            this.x = x;
        }

        public override float getX()
        {
            return this.x;
        }

        public override void setY(float y)
        {
            this.y = y;
        }

        public override float getY()
        {
            return this.y;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public void setImage(Image.Name imageName)
        {
            ImageManager imageManager = ImageManager.Instance();
            this.image = imageManager.findActiveByName(imageName);
        }

        public void setImage(Image image)
        {
            this.image = image;
        }

        public Image getImage()
        {
            return image;
        }

        override public void update()
        {
            if (azulSprite == null){

                azulSprite = new Azul.Sprite(getImage().getTexture().getAzulTexture(), getImage().getRect(), getAzulRect());
            }
            else
            {
                if(getColor() != null)
                    azulSprite.SwapColor(getColor().getAzulColor());
                
                azulSprite.angle = this.getAngle();
                azulSprite.sx = this.getSx();
                azulSprite.sy = this.getSy();
                azulSprite.x = this.getX();
                azulSprite.y = this.getY();
              
                azulSprite.SwapScreenRect(getAzulRect());
                azulSprite.SwapTexture(getImage().getTexture().getAzulTexture());
                azulSprite.SwapTextureRect(getImage().getRect());
                
                  
            }
            
            azulSprite.Update();
        }

        override public void render()
        { 
            if(azulSprite != null)
                azulSprite.Render();
        }


    
    }
}
