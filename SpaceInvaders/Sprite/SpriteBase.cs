﻿using System;
using System.Diagnostics;


namespace SpaceInvaders
{
    abstract public class SpriteBase : Container
    {
        private float angle;
        private float sx;
        private float sy;
        private float w;
        private float h;
        private bool visible = true;
        private Color color;

        private SpriteLayer spriteLayer; 

        private SpriteBase layerNext;
        private SpriteBase layerPrev;

        abstract public void render();
        abstract public void update();
        abstract public float getX();
        abstract public float getY();
        abstract public void setX(float x);
        abstract public void setY(float y);

        public void setColor(Color color)
        {
            this.color = color;
        }

        public Color getColor()
        {
            return this.color;
        }

        public void setColor(Color.Name colorName)
        {
            ColorManager colorManager = ColorManager.Instance();
            this.color = colorManager.findActiveByName(colorName);
        }

        public void setSpriteLayer(SpriteLayer spriteLayer)
        {
            this.spriteLayer = spriteLayer;
        }

        public SpriteLayer getSpriteLayer()
        {
            return this.spriteLayer;
        }

        public void setLayerNext(SpriteBase layerNext)
        {
            this.layerNext = layerNext;
        }

        public SpriteBase getLayerNext()
        {
            return this.layerNext;
        }

        public void setLayerPrev(SpriteBase layerPrev)
        {
            this.layerPrev = layerPrev;
        }

        public SpriteBase getLayerPrev()
        {
            return this.layerPrev;
        }

        public void setSx(float sx)
        {
            this.sx = sx;
        }

        public float getSx()
        {
            return this.sx;
        }

        public void setSy(float sy)
        {
            this.sy = sy;
        }

        public float getSy()
        {
            return this.sy;
        }

        public void setH(float h)
        {
            this.h = h;
        }

        public float getH()
        {
            return this.h;
        }

        public void setW(float w)
        {
            this.w = w;
        }

        public float getW()
        {
            return this.w;
        }

        public void setAngle(float angle)
        {
            this.angle = angle;
        }

        public float getAngle()
        {
            return this.angle;
        }
        
        public Azul.Rect getAzulRect()
        {
            return new Azul.Rect(getX(), getY(), w, h);
        }
        
        public void setRect(float x, float y, float w, float h)
        {
            setX(x);
            setY(y);
            setW(w);
            setH(h);
        }

        public void setVisible(bool visible)
        {
            this.visible = visible;
        }

        public bool isVisible()
        {
            return this.visible;
        }
    }

}

