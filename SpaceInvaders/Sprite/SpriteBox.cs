﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteBox : SpriteBase
    {
       private Azul.SpriteBox azulSpriteBox;
       private Color lineColor;
       private Name name;
       private float x;
       private float y;

        public SpriteBox()
        {
            initialize();
        }

        public override void initialize()
        {
            setName(Name.Uninitialized);
            setLineColor(null);
            azulSpriteBox = null;
            setSx(1.0f);
            setSy(1.0f);
            setSpriteLayer(null);
        }

        public override void setX(float x)
        {
            this.x = x;
        }

        public override float getX()
        {
            return this.x;
        }

        public override void setY(float y)
        {
            this.y = y;
        }

        public override float getY()
        {
            return this.y;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0} x:{1} y:{2} h:{3} w:{4} angle:{5}", getName(), getX(), getY(), getH(), getW(), getAngle());
        }

       public void setAzulSpriteBox(Azul.SpriteBox spriteBox){
           this.azulSpriteBox = spriteBox;
       }

       public Azul.SpriteBox getAzulSpriteBox()
       {
           return this.azulSpriteBox;
       }

       public void setLineColor(Color.Name colorName)
       {
           ColorManager colorManager = ColorManager.Instance();
           this.lineColor = colorManager.findActiveByName(colorName);
       }

       public void setLineColor(Color color)
       {
           this.lineColor = color;
       }

       public Color getLineColor()
       {
           return lineColor;
       }

       override public void update()
       {
              if (azulSpriteBox == null )
              {
                  azulSpriteBox = new Azul.SpriteBox(new Azul.Rect(getX(), getY(), getW(), getH()), lineColor.getAzulColor());
              }
              else
              {
                  azulSpriteBox.SwapColor(lineColor.getAzulColor());
                  azulSpriteBox.angle = this.getAngle();
                  azulSpriteBox.sx = this.getSx();
                  azulSpriteBox.sy = this.getSy();
                  azulSpriteBox.SwapScreenRect(new Azul.Rect(this.getX(), this.getY(), this.getW(), this.getH()));
                  //Debug.WriteLine("{0}{1}", azulSpriteBox.sx, azulSpriteBox.sy);
              }

           azulSpriteBox.Update();
       }

       override public void render(){
           azulSpriteBox.Render();
       }

       public enum Name
       {
           Uninitialized,
           Borderbox,
           Redbox,
           Bluebox,
           Greenbox,
           Collision,
       }
    }
}
