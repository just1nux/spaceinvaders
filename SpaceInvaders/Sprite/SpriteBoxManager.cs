﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteBoxManager : ContainerManager
    {
       // private static volatile SpriteBoxManager instance;

        private SpriteBoxManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static SpriteBoxManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            SpriteBoxManager instance = GameManager.GetCurrentPlayer().spriteBoxManagerInstance;
            if (instance == null)
            {
                GameManager.GetCurrentPlayer().spriteBoxManagerInstance = new SpriteBoxManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().spriteBoxManagerInstance;
            }
            return instance;
        }

        public static SpriteBox Add(SpriteBox.Name spriteBoxName, Color.Name colorName, float x, float y, float w, float h)
        {
            SpriteBoxManager manager = SpriteBoxManager.Instance();
            SpriteBox node = (SpriteBox)manager.addLink();
            node.setName(spriteBoxName);

            ColorManager colorManager = ColorManager.Instance();
            Color color = colorManager.findActiveByName(colorName);
            node.setLineColor(color);

            node.setX(x);
            node.setY(y);
            node.setW(w);
            node.setH(h);
            return node;
        }

        protected override Container create()
        {
            Container link = new SpriteBox();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            SpriteBox nodeA = (SpriteBox)linkA;
            SpriteBox nodeB = (SpriteBox)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public SpriteBox findActiveByName(SpriteBox.Name name)
        {
            SpriteBox foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                SpriteBox node = (SpriteBox)pullFromReserve();
                node.setName(name);
                foundNode = (SpriteBox)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            SpriteBoxManager manager = SpriteBoxManager.Instance();
            Debug.WriteLine("----SpriteBox Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            SpriteBoxManager manager = SpriteBoxManager.Instance();
            manager.baseDestroy();
        }
    }
}

