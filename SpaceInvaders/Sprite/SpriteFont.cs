﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteFont : SpriteBase
    {
        private Font.Name name;
        private Azul.Sprite azulSprite;
        private Azul.Rect screenRect;
        
        private String message;
        private Glyph.Name glyphName;

        public float x;
        public float y;
        private float kerning;

        public SpriteFont()
            : base()
        {
        }

        public override void initialize()
        {
            this.azulSprite = new Azul.Sprite();
            this.screenRect = new Azul.Rect();
            this.message = null;
            this.glyphName = Glyph.Name.Uninitialized;
            this.x = 0.0f;
            this.y = 0.0f;
            this.kerning = 10.0f;
            this.setColor(ColorManager.Add(Color.Name.NullColor, 1.0f, 1.0f, 1.0f, 1.0f));
        }

        ~SpriteFont()
        {
            this.azulSprite = null;
            this.screenRect = null;
            this.message = null;
        }

        public void Set(Font.Name name, String message, Glyph.Name glyphName, float xStart, float yStart)
        {
            Debug.Assert(message != null);
            this.message = message;

            this.x = xStart;
            this.y = yStart;

            this.name = name;
            this.glyphName = glyphName;

        }

        public void UpdateMessage(String message)
        {
            Debug.Assert(message != null);
            this.message = message;
        }

        override public void update()
        {
            Debug.Assert(this.azulSprite != null);
        }

        override public void render()
        {
            Debug.Assert(this.azulSprite != null);
            Debug.Assert(this.screenRect != null);
            Debug.Assert(this.message != null);
           // Debug.Assert(this.message.Length > 0);

            float xTmp = this.x;
            float yTmp = this.y;

            float xEnd = this.x;

            for (int i = 0; i < this.message.Length; i++)
            {
                int key = Convert.ToByte(message[i]);

                Glyph glyph = GlyphManager.FindByName(this.glyphName, key);
                Debug.Assert(glyph != null);

                xTmp = xEnd + glyph.getRect().getAzulRect().width / 2;
                this.screenRect.Set(xTmp, yTmp, glyph.getRect().getAzulRect().width, glyph.getRect().getAzulRect().height);

                azulSprite.Swap(glyph.getTexture().getAzulTexture(), glyph.getRect().getAzulRect(), this.screenRect, this.getColor().getAzulColor());

                azulSprite.Update();
                azulSprite.Render();

                // move the starting to the next character
                xEnd = glyph.getRect().getAzulRect().width / 2 + xTmp + kerning;

            }
        }

        public override void setX(float x)
        {
            this.x = x;
        }

        public override void setY(float y)
        {
            this.y = y;
        }

        public override float getX()
        {
            return this.x;
        }

        public override float getY()
        {
            return this.y;
        }
        public override void print()
        {
            Debug.WriteLine("SpriteFont");
        }
    }
}
