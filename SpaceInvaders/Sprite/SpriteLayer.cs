﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteLayer : Container
    {
        public enum Name
        {
            Uninitialized,
            NullObject,
            TemplateLayer,
            GunLayer,
            ShipLayer,
            MissileLayer,
            SquidLayer,
            CrabLayer,
            OctopusLayer,
            CollisionLayer,
            WallLayer,
            OverlayScores,
            OverlayCredits,
            OverlayLives,
            OverlayBar,
            Shields,
            BombLayer,
            ScoreKey,
            UFOLayer,
            PlayerChoose,
            PlayerChosen,
            Explosions,
            ShieldExplosions,
            GameOver,
        }

        private Name name;
        private SpriteBase sprite;
        private bool visible;

        public SpriteLayer()
        {
            initialize();
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
            visible = false;
        }

        public void add(Sprite.Name name)
        {
            SpriteBase spriteBase = SpriteManager.Instance().findActiveByName(name);
            add(spriteBase);
        }

        public void add(SpriteBase spriteBase)
        {
            addToFront(ref spriteBase, ref sprite);
            spriteBase.setSpriteLayer(this);
        }

        public void add(SpriteBox.Name name)
        {
            SpriteBase spriteBase = SpriteBoxManager.Instance().findActiveByName(name);
            addToFront(ref spriteBase, ref sprite);
            spriteBase.setSpriteLayer(this);
        }



        private void addToFront(ref SpriteBase link, ref SpriteBase front)
        {
            if (front != null)
            {
                link.setLayerNext(front);
                front.setLayerPrev(link);
            }
            front = link;
        }

        public void removeSprite(SpriteBase sprite)
        {
            if (sprite != null)
            {
                if (sprite.getLayerPrev() != null)
                {	// middle or last
                    sprite.getLayerPrev().setLayerNext(sprite.getLayerNext());
                }
                else
                {  // first
                    setSprite((SpriteBase)sprite.getLayerNext());
                }

                if (sprite.getLayerNext() != null)
                {	//middle
                    sprite.getLayerNext().setLayerPrev(sprite.getLayerPrev());
                }

                sprite.setLayerNext(null);
                sprite.setLayerPrev(null);
            }
        }



        public void setSprite(SpriteBase sprite)
        {
            this.sprite = sprite;
        }

        public SpriteBase getSprite()
        {
            return this.sprite;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());

            SpriteBase spriteBase = getSprite();

            while (spriteBase != null)
            {
                spriteBase.print();
                spriteBase = (SpriteBase)spriteBase.getLayerNext();
            }
        }

        public void removeAll()
        {
            SpriteBase spriteBase = getSprite();
            SpriteBase tspriteBase;

            while (spriteBase != null)
            {
                tspriteBase = (SpriteBase)spriteBase.getLayerNext();
                removeSprite(spriteBase);
                spriteBase = tspriteBase;
            }
        }


        public void update()
        {
            SpriteBase link = sprite;
            while (link != null)
            {
                if (isVisible())
                {
                    link.update();
                }
                link = (SpriteBase)link.getLayerNext();
            }
        }

        public void render()
        {
            SpriteBase link = sprite;
            while (link != null)
            {
                if (isVisible() && link.isVisible())
                {
                    link.render();
                }
                link = (SpriteBase)link.getLayerNext();
            }
        }

        public void setVisible(bool visible){
            this.visible = visible;
        }

        public bool isVisible(){
            return this.visible;
        }
    }
}
