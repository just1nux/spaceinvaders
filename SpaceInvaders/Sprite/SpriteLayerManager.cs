﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteLayerManager : ContainerManager
    {
        //private static SpriteLayerManager instance = null;
        private SpriteLayer pRefNode;

        private SpriteLayerManager(int reserveNum = 3, int reserveGrow = 1)
            : base(reserveNum, reserveGrow)
        {
            this.pRefNode = (SpriteLayer)this.create();
        }

        public static SpriteLayerManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            SpriteLayerManager instance = GameManager.GetCurrentPlayer().spriteLayerManagerInstance;

            if (instance == null)
            {
                //instance = new SpriteLayerManager(reserveNum, reserveGrow);
                GameManager.GetCurrentPlayer().spriteLayerManagerInstance = new SpriteLayerManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().spriteLayerManagerInstance;
            }
            return instance;
        }

        ~SpriteLayerManager()
        {
            this.pRefNode = null;
            //SpriteLayerManager.instance = null;
        }

        public static void Destroy()
        {
            SpriteLayerManager manager = SpriteLayerManager.Instance();
            manager.baseDestroy();
        }


        protected override Container create()
        {
            Container link = new SpriteLayer();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            SpriteLayer nodeA = (SpriteLayer)linkA;
            SpriteLayer nodeB = (SpriteLayer)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public static SpriteLayer Add(SpriteLayer.Name name)
        {
            SpriteLayerManager manager = SpriteLayerManager.Instance();
            SpriteLayer spriteLayer = manager.findActiveByName(name);
            if (spriteLayer == null)
            {
                spriteLayer = (SpriteLayer)manager.addLink();
                spriteLayer.setName(name);
            }
            return spriteLayer;

        }
        
        public static void AddSprite(SpriteLayer.Name layerName, SpriteBase spriteBase)
        {
            SpriteLayer layer = SpriteLayerManager.Add(layerName);
            layer.add(spriteBase);
        }

        public static void AddSprite(SpriteLayer.Name layerName, Sprite.Name spriteName)
        {
            Sprite sprite = SpriteManager.Instance().findActiveByName(spriteName);

            AddSprite(layerName, sprite);
        }

        public static SpriteLayer FindLayerByName(SpriteLayer.Name name)
        {
            SpriteLayerManager manager = SpriteLayerManager.Instance();
            SpriteLayer spriteLayer = manager.findActiveByName(name);

            if (spriteLayer == null)
                spriteLayer = SpriteLayerManager.Add(name);
            return spriteLayer;
        }


        public SpriteLayer findActiveByName(SpriteLayer.Name name)
        {
            SpriteLayer foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                SpriteLayer node = (SpriteLayer)pullFromReserve();
                node.setName(name);
                foundNode = (SpriteLayer)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Draw()
        {
            SpriteLayerManager manager = SpriteLayerManager.Instance();
            SpriteLayer link = (SpriteLayer)manager.pActive;
            while (link != null)
            {
               
                link.update();
                link.render();
                
                link = (SpriteLayer)link.getNext();
            }
        }

        public void print()
        {
            SpriteLayer link = (SpriteLayer)pActive;
            while (link != null)
            {
                link.print();
                link = (SpriteLayer)link.getNext();
            }
        }

        public static void Print()
        {
            SpriteLayerManager manager = SpriteLayerManager.Instance();
            Debug.WriteLine("----SpriteLayer Manager----");
            manager.printTotals();
        }

    }
}
 
