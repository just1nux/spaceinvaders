﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteManager : ContainerManager
    {
        private static volatile SpriteManager instance;

       private SpriteManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static SpriteManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new SpriteManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static Sprite Add(Sprite.Name name)
        {
            //if sprite doesnt exist, make it otherwise return it.
            SpriteManager manager = SpriteManager.Instance();
            Sprite sprite = manager.findActiveByName(name);
            if (sprite == null)
            {
                sprite = (Sprite)manager.addLink();
                sprite.setName(name);
                sprite.setRect(0, 0, 0, 0);
            }
            return sprite;
        }

        public static Sprite Add(Sprite.Name name, Image.Name imageName, float x, float y, float w, float h)
        {
            Sprite sprite = SpriteManager.Add(name);
            sprite.setImage(imageName);
            sprite.setRect(x, y, w, h);
            return sprite;
        }


        protected override Container create()
        {
            Container link = new Sprite();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Sprite nodeA = (Sprite)linkA;
            Sprite nodeB = (Sprite)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }


        public Sprite findActiveByName(Sprite.Name name)
        {
            Sprite foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Sprite node = (Sprite)pullFromReserve();
                node.setName(name);
                foundNode = (Sprite)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }
        
        public static void Print()
        {
            SpriteManager manager = SpriteManager.Instance();
            Debug.WriteLine("----Sprite Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            SpriteManager manager = SpriteManager.Instance();
            manager.baseDestroy();
        }
    }
}
