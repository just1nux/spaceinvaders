﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteProxy: SpriteBase
    {
        private Name name;
        private float x;
        private float y;
        private SpriteBase sprite;
        private int index;
       
        public SpriteProxy()
        {
        }

        public override void initialize()
        {
            setName(Name.Uninitialized);
            setX(0.0f);
            setY(0.0f);
            setSpriteLayer(null);
            setColor(ColorManager.Add(Color.Name.NullColor, 1.0f, 1.0f, 1.0f, 1.0f));
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setSprite(SpriteBase sprite)
        {
            this.sprite = sprite;
        }

        public SpriteBase getSprite()
        {
            return this.sprite;
        }

        public override void setX(float x)
        {
            this.x = x;
        }

        public override float getX()
        {
            return this.x;
        }

        public override void setY(float y)
        {
            this.y = y;
        }

        public override float getY()
        {
            return this.y;
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0} x:{1} y:{2}", getName(), getX(), getY());
        }

        override public void update()
        {    
            pushToSprite();
            sprite.update();
        }

        public void pushToSprite()
        {
            sprite.setX(this.getX());
            sprite.setY(this.getY());
                        
            if ((Color.Name)getColor().getName() != Color.Name.NullColor)
            {
                sprite.setColor(this.getColor());
            }
        }

        override public void render()
        {
            pushToSprite();
            sprite.update();
            sprite.render();
        }

        public enum Name
        {
            Uninitialized,
            NullObject,
            Proxy,
            LifeProxy,
        }

    }
}
