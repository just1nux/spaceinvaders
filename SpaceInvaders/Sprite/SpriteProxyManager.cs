﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class SpriteProxyManager : ContainerManager
    {
        private static volatile SpriteProxyManager instance;

        private SpriteProxyManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static SpriteProxyManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new SpriteProxyManager(reserveNum, reserveGrow);
            }
            return instance;
        }


        public static SpriteProxy Add(SpriteProxy.Name proxyName, Sprite sprite, float x, float y, int index=0)
        {
            SpriteProxyManager manager = SpriteProxyManager.Instance();
            SpriteProxy spriteProxy = Add(proxyName, index);
            spriteProxy.setSprite(sprite);
            spriteProxy.setX(x);
            spriteProxy.setY(y);

            return spriteProxy;
        }

        public static SpriteProxy Add(SpriteProxy.Name proxyName, int index=0)
        {
            SpriteProxyManager manager = SpriteProxyManager.Instance();
            SpriteProxy spriteProxy = (SpriteProxy)manager.addLink();
            spriteProxy.setName(proxyName);
            spriteProxy.setIndex(index);
            return spriteProxy;
        }

        public static SpriteProxy Find(SpriteProxy.Name name, int index = 0)
        {
            SpriteProxyManager manager = SpriteProxyManager.Instance();
            SpriteProxy spriteProxy = manager.findActiveByName(name, index);
            return spriteProxy;
        }

        public SpriteProxy findActiveByName(SpriteProxy.Name name, int index=0)
        {
            SpriteProxy foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                SpriteProxy node = (SpriteProxy)pullFromReserve();
                node.setName(name);
                node.setIndex(index);
                foundNode = (SpriteProxy)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        protected override Container create()
        {
            Container link = new SpriteProxy();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            SpriteProxy nodeA = (SpriteProxy)linkA;
            SpriteProxy nodeB = (SpriteProxy)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString() && nodeA.getIndex() == nodeB.getIndex())
                status = true;

            return status;
        }
        
        public static void Print()
        {
            SpriteProxyManager manager = SpriteProxyManager.Instance();
            Debug.WriteLine("----SpriteProxy Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            SpriteProxyManager manager = SpriteProxyManager.Instance();
            manager.baseDestroy();
        }
    }
}
