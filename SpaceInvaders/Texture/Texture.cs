﻿using System;
using System.Diagnostics;


namespace SpaceInvaders
{

    public class Texture : Container
    {
        private Name name;
        private Azul.Texture azulTexture;

        public Texture()
        {
            initialize();
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public void setAzulTexture(Azul.Texture texture){
            this.azulTexture = texture;
        }

        public Azul.Texture getAzulTexture(){
            return azulTexture;
        }

        public void setAzulTexture(String texture)
        {
            if (this.azulTexture == null){
                this.azulTexture = new Azul.Texture(texture);
            }
        }

        public Enum getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("name:{0}", getName());
        }

        public enum Name
        {
            Uninitialized,
            NullObject,
            Template,
            Aliens,
            Solid,
            SpaceInvaders24pt,
        }
    }
}

