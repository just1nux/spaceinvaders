﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class TextureManager : ContainerManager
    {
        private static volatile TextureManager instance;

        private TextureManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static TextureManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new TextureManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        
        public static Texture Add(Texture.Name name)
        {
            TextureManager manager = TextureManager.Instance();
            Texture texture = manager.findActiveByName(name);
            if (texture == null)
            {
                texture = (Texture)manager.addLink();
                texture.setName(name);
            }
            return texture;
        }
         

        public static Texture Add(Texture.Name name, String textureString)
        {
            Texture texture = TextureManager.Add(name);
            texture.setAzulTexture(textureString);
            return texture;
        }

        public static Texture FindByName(Texture.Name name)
        {
            TextureManager manager = TextureManager.Instance();
            Texture texture = manager.findActiveByName(name);
            return texture;
        }


        protected override Container create()
        {
            Container link = new Texture();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            Texture nodeA = (Texture)linkA;
            Texture nodeB = (Texture)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public Texture findActiveByName(Texture.Name name)
        {
            Texture foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                Texture node = (Texture)pullFromReserve();
                node.setName(name);
                foundNode = (Texture)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            TextureManager manager = TextureManager.Instance();
            Debug.WriteLine("----Texture Manager----");
            manager.printTotals();
        }
        
        public static void Destroy()
        {
            TextureManager manager = TextureManager.Instance();
            manager.baseDestroy();
        }

    }
}
