﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public abstract class Command:Container
    {
        abstract public void execute(float deltaTime);
    }
}
