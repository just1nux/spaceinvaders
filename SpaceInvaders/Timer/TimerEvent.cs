﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class TimerEvent: Container
    {
        private Command command;
        private float eventTime;
        private float deltaTime;
        private Name name;

        public TimerEvent()
            : base()
        {
 
        }

        public override void initialize()
        {
            setCommand(null);
            setEventTime(0.0f);
            setDeltaTime(0.0f);
        }

        public Name getName()
        {
            return this.name;
        }

        public void setName(Name name)
        {
            this.name = name;
        }

        public void Process()
        {
            Debug.Assert(getCommand() != null);
            command.execute(getDeltaTime());
        }

        public void setCommand(Command command)
        {
            this.command = command;
        }

        public Command getCommand()
        {
            return command;
        }

        public float getEventTime()
        {
            return eventTime;
        }

        private void setEventTime(float eventTime)
        {
            this.eventTime = eventTime;
        }

        public void setDeltaTime(float deltaTime)
        {
            this.deltaTime = deltaTime;
            this.eventTime = GameManager.Instance().GetTime() + deltaTime;
        }

        public float getDeltaTime()
        {
            return deltaTime;
        }

        public override void print()
        {
            Debug.WriteLine("eventTime: {0} deltaTime: {1}", getEventTime(), getDeltaTime());
        }

        public enum Name {
            Uninitialized,
            TimerEvent,
        }
    }


}
