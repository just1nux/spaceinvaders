﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class TimerManager: ContainerManager
    {
        private static volatile TimerManager instance;

        private TimerManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static TimerManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            if (instance == null)
            {
                instance = new TimerManager(reserveNum, reserveGrow);
            }
            return instance;
        }

        public static TimerEvent Add(Command command, float deltaTime = 0.0f)
        {
            TimerManager manager = TimerManager.Instance();
            Container link = manager.pullFromFront(ref manager.pReserve);
            TimerEvent timer = (TimerEvent)link;
            timer.setDeltaTime(deltaTime);
            timer.setCommand(command);
            
            manager.addToActiveSorted(ref link);
            manager.decrementmNumReserve();

            return timer;
        }

        protected override Container create()
        {
            Container link = new TimerEvent();
            return link;
        }

        public static void Update()
        {
            TimerManager manager = TimerManager.Instance();
            TimerEvent pEvent = (TimerEvent) manager.getpActive();
            TimerEvent nextEvent = null;

            float currTime = GameManager.Instance().GetTime();
            while (pEvent != null && (currTime >= pEvent.getEventTime()))
            {
                nextEvent = (TimerEvent)pEvent.getNext();
         
                if (currTime >= pEvent.getEventTime())
                {
                    pEvent.Process();
                    Container container = (Container) pEvent;
                    manager.remove(ref container);
                }

                pEvent = nextEvent;
            }
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            TimerEvent nodeA = (TimerEvent)linkA;
            TimerEvent nodeB = (TimerEvent)linkB;

            Boolean status = false;

            if (nodeA.getEventTime() < nodeB.getEventTime())
                status = true;
            Boolean n = new Boolean();
            n = status;
    
            return status;
        }
        
        public static void Print()
        {
            TimerManager manager = TimerManager.Instance();
            Debug.WriteLine("----Timer Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            TimerManager manager = TimerManager.Instance();
            manager.baseDestroy();
        }
    }
}
