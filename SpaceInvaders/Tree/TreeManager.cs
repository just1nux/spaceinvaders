﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{

    public class TreeManager : ContainerManager
    {
        //private static volatile TreeManager instance;

        private TreeManager(int reserveNum = 3, int reserveGrow = 5)
            : base(reserveNum, reserveGrow)
        {
           
        }

        public static TreeManager Instance(int reserveNum = 3, int reserveGrow = 5)
        {
            TreeManager instance = GameManager.GetCurrentPlayer().treeManagerInstance;

            if (instance == null)
            {
                GameManager.GetCurrentPlayer().treeManagerInstance = new TreeManager(reserveNum, reserveGrow);
                instance = GameManager.GetCurrentPlayer().treeManagerInstance;
            }

            return instance;
        }

        public static TreeNode Add(TreeNode.Name name)
        {
            TreeManager manager = TreeManager.Instance();
            TreeNode node = manager.findActiveByName(name);
            if (node == null)
            {
                node = (TreeNode)manager.addLink();
                PCSTree tree = new PCSTree();
                node.set(tree);
                node.setName(name);
            }

            return node;
        }




        protected override Container create()
        {
            Container link = new TreeNode();
            return link;
        }

        protected override bool compare(Container linkA, Container linkB)
        {
            Debug.Assert(linkA != null);
            Debug.Assert(linkB != null);

            TreeNode nodeA = (TreeNode)linkA;
            TreeNode nodeB = (TreeNode)linkB;

            Boolean status = false;

            if (nodeA.getName().ToString() == nodeB.getName().ToString())
                status = true;

            return status;
        }

        public TreeNode findActiveByName(TreeNode.Name name)
        {
            TreeNode foundNode = null;
            // if there are active links to search
            if (pActive != null)
            {
                //pull a reserve node for use
                TreeNode node = (TreeNode)pullFromReserve();
                node.setName(name);
                foundNode = (TreeNode)searchActiveForLink(node);
                //return the reserve node when done
                node.initialize();
                Container nodeb = node;
                addToReserve(ref nodeb);
            }
            return foundNode;
        }

        public static void Print()
        {
            TreeManager manager = TreeManager.Instance();
            Debug.WriteLine("----Tree Manager----");
            manager.printTotals();
        }

        public static void Destroy()
        {
            TreeManager manager = TreeManager.Instance();
            manager.baseDestroy();
        }
    }
}
