﻿using System;
using System.Diagnostics;

namespace SpaceInvaders
{
    public class TreeNode : Container
    {
        private Name name;
        private PCSTree tree;

        public TreeNode()
        {
            initialize();
        }

        public void set(PCSTree tree)
        {
            setTree(tree);
        }

        public override void initialize()
        {
            name = Name.Uninitialized;
        }

        public Name getName()
        {
            return this.name;
        }

        public void setName(Name nodeName)
        {
            this.name = nodeName;
        }

        public override void print()
        {
            Debug.WriteLine("Tree name:{0}", getName());
        }

        public void setTree(PCSTree tree)
        {
            this.tree = tree;
        }

        public PCSTree getTree()
        {
            return this.tree;
        }

        public enum Name
        {
            Uninitialized,
            AlienTree,
            ShipTree,
            MissileTree,
            WallTree,
            ShieldTree,
            BombTree,
            UFOTree,
            WallShipTree,
        }

    }
}
